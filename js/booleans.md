# Booleanos

Otro tipo de datos es el `Boolean`. Los booleanos solo pueden tomar dos valores: `true` o `false`.

Son basicamente interruptores de encendido o apagado en el código, si es `true` lo activo si es `false` lo apago.

Son además exclusivos mutuamente.

No debemos escribir los valores booleanos como strings, ya que "true" o "false" no son palabras reservadas del lenguaje.

Ejercicio:
Escribe una función que devuelva `true` o `false`.

## `if...else`

Las declaraciones `if...else` se utilizan para tomar decisiones en código. La palabra clave `if` le dice a JavaScript que ejecute el código entre llaves bajo ciertas condiciones, definidas entre paréntesis. Estas condiciones se conocen como condiciones booleanas y solo pueden ser verdaderas o falsas.

Cuando la condición se evalúa como verdadera, el programa ejecuta la declaración dentro de las llaves. Cuando la condición booleana se evalúa como falsa, la instrucción dentro de las llaves no se ejecutará.

```
si (la condición es verdadera) {
    este código se ejecuta
}
```

Por ejemplo:

```js
function test(condition) {
    if (condition) {
        return "es cierto":
    }

    return "es falso";
}

test(true) // "es cierto"
test(false) // "es falso"
```

Cuando se llama a `test` con un valor `true`, este evalúa `condition` para ver si es `true` o `false`. Como es `true`, la función devuelve "es cierto". Cuando llamamos a `test` con un valor `false`, `condition` no se cumple y la instrucción en las llaves no se ejecuta y la función devuelve "es falso".

Ejercicio:

Declara `if...else` dentro de la función, de manera que devuelva "Has comprado", "No ha comprado".

```js
function haComprado(num) {
  const loHaHecho = Math.random() > 0.5; // esto valdrá true si efectivamente es par

  // solo cambia el codigo debajo del comentario
}
```

## comparador de igualdad '=='

Hay muchos operadores de comparación en JavaScript. Todos estos operadores devuelven un valor booleano `true` o `false`.

El operador más básico es el operador de igualdad `==`. El operador de igualdad compara dos valores y devuelve `true` si son equivalentes o `false` si no lo son. Ten en cuenta que la igualdad es diferente de la asignación (=), que asigna el valor a la derecha del operador a una variable a la izquierda.

```js
función equalTest(myVal) {
  if (myVal == 10) {
     return "Igual a 10";
  }
  return "Diferente de 10";
}
```

Si `myVal` es igual a 10, el operador de igualdad devuelve `true`, por lo que se ejecutará el código entre llaves, y la función devolverá "Igual a 10". De lo contrario, la función devolverá "Diferente de 10". Para que JavaScript pueda comparar dos tipos de datos diferentes (por ejemplo, números y cadenas), debe convertir un tipo a otro. Esto se conoce como "Type Coercion". Puede comparar los términos de la siguiente manera:

```js
1 == 1; // true
1 == 2; // false
1 == "1"; // true
"3" == 3; // true
```

Ejercicio:

```js
function equals(val, val2) {
  let equal;

  return equal;
}

// Change this value to test
testEqual(10, 10); // devuelve true
testEqual(10, "9"); // devuelve false
testEqual(10, 9); // devuelve false
testEqual(10, "10"); // devuelve true
```

## comparador de igualdad estricta `===`

A diferencia del operador de igualdad, que intenta convertir ambos valores en comparación con un tipo común, el comparador de igualdad estricta no realiza una conversión del tipo.

Si los valores que se comparan no son del mismo tipo, el operador de igualdad realizará una conversión de tipo y luego evaluará los valores. Sin embargo, el operador de igualdad estricta comparará tanto el tipo de datos como el valor tal cual, sin convertir un tipo a otro.

_En JavaScript, puedes conocer el tipo de una variable o un valor con el operador `typeof`, de la siguiente manera:_

```js
typeof 3;
typeof "3";
typeof myBar;
```

Si los valores que se comparan tienen diferentes tipos, se consideran desiguales y el operador de igualdad estricta devolverá `false`.

```js
3 === 3; // true
3 === "3"; // false
```

En el segundo ejemplo, 3 es un valor de tipo Number y '3' es uno de tipo String.

Ejercicio:

Implementa la funcion del ejercicio anterior, de tal manera que compare por igualdad estricta.

## comparador de desigualdad

El comparador de desigualdad (! =) es lo opuesto al comparador de igualdad. Significa "No es igual" y devuelve `false` donde la igualdad devolvería `true` y viceversa. Al igual que el comparador de igualdad, el comparador de desigualdad convertirá tipos de datos de los valores al comparar.

```js
1 != 2; // true
1 != "1"; // false
1 != "1"; // false
1 != true; // false
0 != false; // false
```

Ejercicio:

Escribe una funcion de un listado que imprima por consola "Es Diferente" si el numero que se le pasa es diferente a 100.

## comparador de desigualdad estricta

El comparador de desigualdad estricta (! ==) es el opuesto lógico del comparador de igualdad estricta. Significa "estrictamente no igual" y devuelve `false` donde la igualdad estricta devolvería `true` y viceversa. La desigualdad estricta no convertirá los tipos de datos.

```js
3 !== 3; // false
3 !== "3"; // true
4 !== 3; // true
```

Ejercicio

Escribe una funcion que compare si la edad recibida por parametros es diferente de 18.

## soy? estoy?

![gato :D](assets/beornottobe.jpg)

```js
// CopyRight Schrödinger en memoria de 'Snoop'
const CAT_STATUS_DEATH = "DEATH";

function onOff() {
  return Math.random > 0.5;

  // devuelve aleatoriamente `true` o `false`;
}

function caja(gato) {
  const veneno = onOff();

  if (!veneno) {
    return gato;
  }

  return null;
}

function torturar(gato) {
  alert("Torturando a tu gato: " + gato);
  const gatoTorturado = caja(gato);
  alert("Tu gato ha sido toturado al gusto: " + gato);
  if (gatoTorturado != null) {
    return CAT_STATUS_ALIVE;
  }

  return CAT_STATUS_DEATH;
}

function play() {
  const tuGato = prompt("Dame tu gato!: escribe su nombre");
  const estadoDeTuGato = torturar(tuGato);

  if (estadoDeTuGato === CAT_STATUS_ALIVE) {
    alert("Sigue vivo!! :D");
  } else {
    alert("Me he cargado al gato T_T");
  }
}

let contadorPartidas = 0;

play();
```

## `else if`

`if...else` nos vale para un solo resultado, pero ¿como lo hacemos si tenemos más de uno?

Hay una forma de encadenar condiciones adicionales extras a if...else — usando else if. Cada condición extra requiere un bloque adicional en medio de bloque if() { ... } y else { ... }

Ejercicio práctico, ejemplo con 5 options y un parrafo que se cambia con textContent.

## comparador `>` mayor que

El operador mayor que `>` compara los valores de dos números. Si el número a la izquierda es mayor que el número a la derecha, devuelve `true`. De lo contrario, devuelve `false`.

Al igual que el operador de igualdad `==`, `>` convertirá los tipos de los valores al comparar.

```js
5 > 3; // true
7 > "3"; // true
2 > 3; // false
"1" > 9; // false
```

## comparador `>=` mayor o igual que

El operador mayor igual que `>=` compara los valores de dos números. Si el número a la izquierda es mayor o igual que el número a la derecha, devuelve `true`. De lo contrario, devuelve `false`.

Al igual que el operador de igualdad `==`, `>=` convertirá los tipos de los valores al comparar.

```js
6 >= 6; // true
7 >= "3"; // true
2 >= 3; // false
"7" >= 9; // false
```

## comparador `<` menor que

El operador menor que `<` compara los valores de dos números. Si el número a la izquierda es menor que el número a la derecha, devuelve `true`. De lo contrario, devuelve `false`.

Al igual que el operador de igualdad `==`, `<` convertirá los tipos de los valores al comparar.

```js
5 < 3; // true
7 < "3"; // true
2 < 3; // false
"1" < 9; // false
```

## comparador `<=` menor o igual que

El operador menor igual que `<=` compara los valores de dos números. Si el número a la izquierda es menor o igual que el número a la derecha, devuelve `true`. De lo contrario, devuelve `false`.

Al igual que el operador de igualdad `==`, `<=` convertirá los tipos de los valores al comparar.

```js
6 <= 6; // true
7 <= "3"; // true
2 <= 3; // false
"7" <= 9; // false
```

## esto y esto `&&`

A veces necesitarás probar más de una cosa a la vez. El operador `&&` devuelve `true` si y solo si los operandos a la izquierda y derecha son verdaderos.

Se puede lograr el mismo efecto anidando una declaración if dentro de otra if:

```js
if (num > 5) {
  if (num < 10) {
    return "Sí";
  }
}

return "No";
```

La misma lógica se puede escribir como:

```js
if (num > 5 && num < 10) {
  return "Sí";
}

return "No";
```

## esto o esto `||`

El operador `||` devuelve `true` si cualquiera de los operandos es `true`. De lo contrario, devuelve `false`.
El operador se compone de dos símbolos de tubería `|`.

El siguiente patrón debería parecer familiar:

```js
if (num > 10) {
  return "No";
}
if (num < 5) {
  return "No";
}

return "Sí";
```

La misma lógica se puede escribir como:

```js
if (num > 10 || num < 5) {
  return "No";
}
return "Sí";
```

Ejercicio: combina los ifs con ||

```js
function testLogicalOr(val) {
  // Solo cambia el codigo por debajo de este comentario

  if (val) {
    return "Outside";
  }

  if (val) {
    return "Outside";
  }

  // Solo cambia el codigo por encima de este comentario
  return "Inside";
}

// Declara el valor
testLogicalOr(value);
```

Ejercicio golf

## Anti patron: retorno de booleanos en funciones

Hay veces que se utilizan estamentos if...else para retornar boleanos, como el siguiente:

```js
function esIgual(a, b) {
  if (a === b) {
    return true;
  }

  return false;
}
```

Esto es un antipatron que complica las cosas, es mejor retornar directamente la comparacion ya que esta se resuelve como un booleano. Lo anterior puede ser reescrito como:

```js
function esIgual(a, b) {
  return a === b;
}
```

Ejercicio:

Construye una funcion que retorna true si un numero es menor que otro, y false si es mayor o igual.
