# Declaración de variables

## `var`

En informática, los datos son cualquier cosa que sea significativa para la computadora. JavaScript proporciona siete tipos de datos diferentes que son `ùndefined`, `null`, `boolean`, `string`, `symbol`, `number` y `object`.

Por ejemplo, las computadoras distinguen entre números, como el número 12, y cadenas, como "12", "perro" o "123 gatos", que son colecciones de caracteres. Las computadoras pueden realizar operaciones matemáticas en un número, pero no en una cadena.

Las variables permiten a las computadoras almacenar y manipular datos de manera dinámica. Lo hacen mediante el uso de una "etiqueta" para señalar los datos en lugar de utilizar los datos en sí. Cualquiera de los siete tipos de datos puede almacenarse en una variable.

Las variables son similares a las variables x e y que usamos en matemáticas, lo que significa que son un nombre simple para representar los datos a los que queremos referirnos. Las variables de programación difieren de las variables matemáticas en que pueden almacenar diferentes valores en diferentes momentos.

Le decimos a JavaScript que cree o declare una variable colocando la palabra clave `var` delante de ella, así:

```js
var name;
```

Esto crea una variable llamada `name`. En javascript terminamos los estamentos con semicolons. Los nombres de variable pueden estar compuestos de letras, numeros o los símbolos `$` o `_`, pero no pueden contener espacios o empezar con un número.

Ejercicio:

Crea una variable llamada `myName`

## `let`

Uno de los mayores problemas al declarar variables con la palabra clave `var` es que puede sobrescribir las declaraciones de variables sin un error.

```js
var camper = "James";
var camper = "David";
console.log(camper);
// registra 'David'
```

Como puedes ver en el código anterior, la variable camper se declara originalmente como James y luego se invalida como David. En una aplicación pequeña, es posible que no te encuentres con este tipo de problema, pero cuando tu código se hace más grande, puedes sobrescribir accidentalmente una variable que no pretendías sobrescribir.

Debido a que este comportamiento no arroja un error, buscar y corregir errores se vuelve más difícil.

Se introdujo una nueva palabra clave llamada `let` en ES6 para resolver este problema potencial con la palabra clave var. Si reemplazaras `var` con `let` en las declaraciones de variables del código anterior, el resultado sería un error.

```js
let camper = "James";
let camper = "David"; // arroja un error
```

Este error se puede ver en la consola de su navegador. Entonces, a diferencia de `var`, cuando se usa `let`, una variable con el mismo nombre solo se puede declarar una vez.

Ejercicio:

Dado el siguiente código:

```js
var catName;
var quote;
function catTalk() {
  "use strict";

  catName = "Oliver";
  quote = catName + " says Meow!";
}
catTalk();
```

Haz que el código sólo use `let`;

## `const`

La palabra clave `let` no es la única forma nueva de declarar variables. En ES6, también puede declarar variables utilizando la palabra clave `const`.

`const` tiene todas las características increíbles que permite `let`, con la ventaja adicional de que las variables declaradas usando `const` son de solo lectura. Son un valor constante, lo que significa que una vez que una variable se asigna con `const`, no se puede reasignar.

```js
"use strict";
const FAV_PET = "Gatos";
FAV_PET = "Perros"; // devuelve error
```

Como puedes ver, intentar reasignar una variable declarada con const arrojará un error. Siempre debes nombrar las variables que no deseas reasignar utilizando la palabra clave `const`. Esto ayuda cuando accidentalmente intentas reasignar una variable que debe mantenerse constante. Una práctica común al nombrar constantes es usar todas las letras mayúsculas, con palabras separadas por un guión bajo.

Nota: Es común que los desarrolladores usen identificadores de variables en mayúscula para valores inmutables y minúsculas o camelCase para valores mutables (objetos y matrices).

Ejercicio:

```js
function printManyTimes(str) {
  "use strict";

  var sentence = str + " is cool!";
  for (var i = 0; i < str.length; i += 2) {
    console.log(sentence);
  }
}

printManyTimes("freeCodeCamp");
```

Cambia el código para que todas las variables se declaren usando `let` o const. Use `let` cuando quiera que cambie la variable, y `const` cuando quiera que la variable permanezca constante. Además, cambia el nombre de las variables declaradas con `const` para cumplir con las prácticas comunes, lo que significa que las constantes deben estar en mayúsculas.

## Asignación de variables

En JavaScript, puedes almacenar un valor en una variable con el operador de asignación.

```js
var myVariable = 5;
```

Esto asigna el valor de Número 5 a myVariable.

La asignación siempre va de derecha a izquierda. Todo a la derecha del operador `=` se resuelve antes de asignar el valor a la variable a la izquierda del operador.

```js
myVar = 5;
myNum = myVar;
```

Esto asigna 5 a myVar y luego resuelve myVar a 5 nuevamente y lo asigna a myNum.

Ejercicio:

```js
var a;
var b = 2;
```

Asigna el valor 7 a la variable a
Asigna el contenido de a en b

### Inicialización de variables

Cuando se declaran las variables de JavaScript, tienen un valor inicial de indefinido. Si realizas una operación matemática en una variable `undefined`, su resultado será `NaN`, que significa "Not a Number". Si concatenas una cadena con una variable `undefined`, obtendrás un string de "undefined".

Ejercicio:

```js
var a;
var b;
var c;

a = a + 1;
b = b + 5;
c = c + " String!";
```

Inicializa las tres variables a, b y c con 5, 10 y "Yo soy un" respectivamente para que no sean `undefined`.

### Los nombres de las variables importan!

En JavaScript, todas las variables y nombres de funciones distinguen entre mayúsculas y minúsculas. Esto significa que la capitalización importa.

MYVAR no es lo mismo que MyVar ni myvar. Es posible tener múltiples variables distintas con el mismo nombre pero con una carcasa diferente. Se recomienda encarecidamente que, en aras de la claridad, no utilices esta función del lenguaje.

Trata de escribir nombres de variables en JavaScript en camelCase. En camelCase, los nombres de variables de varias palabras tienen la primera palabra en minúscula y la primera letra de cada palabra subsiguiente está en mayúscula.

Ejemplo:

```js
var someVariable;
var anotherVariableName;
var thisVariableNameIsSoLong;
```

Ejercicio:

```js
var StUdLyCapVaR;
var properCamelCase;
var TitleCaseOver;

STUDLYCAPVAR = 10;
PRoperCAmelCAse = "A String";
tITLEcASEoVER = 9000;
```

Corrige el nombre de las variables de tal manera que usen camelCase.
