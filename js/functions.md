# Funciones

Podemos dividir piezas de codigo que queremos usar en funciones reutilizables llamadas funciones.

Por ejemplo:

```js
function sayHelloTo(name) {
  console.log("Hola " + name + "!");
}
```

Las funciones pueden ser llamadas o invocadas mediante el uso de su nombre seguido de parentesis, por ejemplo:

```js
sayHelloTo("Iván");
```

El código escrito en el cuerpo de la función sera ejecutado en cada llamada o invocación a la misma.

## Parametros

Las funciones pueden aceptar valores, llamados parametros que son recibidos y usados por la misma cuando esta es llamada. El parametro es declarado entre parentesis, como en el ejemplo anterior, que acepta el parametro `name`.

Ejercicio:

Crea una función que acepte un string y lo devuelva con una exclamación al final.
Crea una función que acepte un string y lo devuelva en upperCase.
Combinalas en una tercera función.

Los tipos concretos que hemos estado viendo, strings, numbers, arrays... contienen numerosas funciones que nos ayudan a manipularlos, estas funciones son llamadas metodos.

## Funciones anonimas

En JS podemos declarar funciones que no tienen nombre, son llamadas funciones anonimas, normalmente son usadas para adjuntarlas a un manejador de eventos, por ejemplo:

```js
const button = document.querySelector("button");

button.onclick = function() {
  /*  ... */
};
```

## Scope global

El scope se refiere a la visibilidad de las variables para ciertas partes del código. Cuando las variables son definidas fuera del bloque de una función estas tienen scope global. Es decir, pueden ser leidas desde cualquier punto del código.

```js
let count = 0;

function countSum() {
  count += 1;
}
```

Ejercicio:

Define una variable global y una funcion que modifique dicho valor.
Por último define una funcion que invoque a la primera función y muestre por consola el valor de la variable global.

# Scope local

Variables declaradas dentro del cuerpo de una función tienen un scope local, es decir, solo son visible dentro de la propia función.

```js
function myTest() {
  let loc = "foo";
  console.log(loc);
}
myTest(); // "foo"
console.log(loc); // loc no está definida
```

Nada impide que trabajemos con scope global y local, pero ten en cuenta que las variables locales toman precedencia sobre las globales.

```js
let someVar = "Hat";
function myFun() {
  let someVar = "Head";
  return someVar;
}
```

Ejercicio:

Declara una variable local que sobreescriba el valor de oterWear:

```js
let outerWear = "T-Shirt";

function myOutfit() {
  return outerWear;
}

myOutfit();
```

### Retornando valores

Igual que las funciones pueden recibir datos como argumentos, pueden devolver datos tras su ejecución. Esto se realiza mediante el uso de la palabra `return` al final de la función, como puedes ver en el ejericio anterior.

Una función que no retorna ningun valor, retornara `undefined` por defecto.
Como regla general, las funciones siempre deberían retornar algo. Es una cuestión de codigo limpio.

Ejercicio:

Crea una funcion llamada timesFive, que acepta un argumento y lo multiplica por 5. Despues retorna el valor.

### Funciones con asignacion en el retorno de valores

Como en JS todo lo que se encuentra a la derecha del signo igual es resuleto antes de la asignación, podemos asignar el retorno del valor de una función a otra variable.

Si tuvieramos una función llamada `suma` que suma dos numeros. Entonces:

```js
let result = sum(2, 2);
```

Asignara a `result` el valor 4;

Ejercicio:

Crea la funcion de suma y asigna el resultado dentro del valor de una funcion que hace uso de la funcion suma, asignando su valor en un scope local y luego lo retorna multiplicado por 5.

## Arrow functions

La expresión de función flecha tiene una sintaxis más corta que una expresión de función convencional y no vincula sus propios this. Las funciones flecha siempre son anónimas. Estas funciones son funciones no relacionadas con métodos y no pueden ser usadas como constructores.

```js
const suma = (x, y) => x + y;
const suma10 = x => suma(10, x);
```
