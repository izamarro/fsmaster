# Arrays

Las matrices se describen como "objetos tipo lista"; básicamente son objetos individuales que contienen múltiples valores almacenados en una lista. Los objetos de matriz pueden almacenarse en variables y tratarse de la misma manera que cualquier otro tipo de valor, la diferencia es que podemos acceder individualmente a cada valor dentro de la lista y hacer cosas útiles y eficientes con la lista, como recorrerlo con un bucle y hacer una misma cosa a cada valor. Tal vez tenemos una serie de productos y sus precios almacenados en una matriz, y queremos recorrerlos e imprimirlos en una factura, sumando todos los precios e imprimiendo el total en la parte inferior.

Si no tuvieramos matrices, tendríamos que almacenar cada elemento en una variable separada, luego llamar al código que hace la impresión y agregarlo por separado para cada artículo. Esto sería mucho más largo de escribir, menos eficiente y más propenso a errores.

## Declaración

En un array podemos almacenar cualquier tipo de objeto, incluso otros arrays, empiezan con el símbolo `[` y terminan con el símbolo `]`

```js
let compraCasa = ["leche", "pan", "queso", "cerveza"];
let compras = [compraCasa, "martillo", "clavos"]; // este array seria multidimensional
```

Ejercicio:

Crea un array que contenga numeros y strings.
Asignalo a otro array que contenga otros números y strings.

## Acceso a los datos mediante indexacion

Podemos acceder a los datos dentro de las matrices usando índices.

Los índices de matriz se escriben en la misma notación de corchete que usan las cadenas, excepto que en lugar de especificar un carácter, están especificando una entrada en la matriz. Al igual que las cadenas, las matrices usan indexación basada en cero, por lo que el primer elemento de una matriz es el elemento 0.

```js
let array = [50, 60, 70];
array[0]; // equals 50
let data = array[1]; // equals 60
```

Ejercicio:

Repite el ejercicio anterior y trata de acceder al primer dato del array contenido en el primer array.

## Modificando los datos de un array

A diferencia de los strings la entradas de un array pueden ser reasignadas.

```js
let array = [50, 60, 70];
array[0] = 30;
```

## Metodos de manejo de arrays comunes

### Longitud

Puedes averiguar la longitud de una matriz (cuántos elementos contiene) exactamente de la misma manera que determinas la longitud (en caracteres) de una cadena— utilizando la propiedad `length`.

```js
let array = [50, 60, 70];
array.length; // 3
```

También nos sirve para indicarle a un bucle, que recorra todos los elementos de un array:

```js
let array = [50, 60, 70];
for (let i = 0; i < array.length; i++) {
  console.log(array[i]);
}
```

Ejercicio:

Localiza el ultimo elemento del siguiente array:

```js
const randomArray = Array.from(new Array(Math.floor(Math.random() * 10)));
```

### Convertir string a arrays y viceversa

A menudo se te presentarán algunos datos brutos contenidos en una cadena larga y grande, y es posible que desees separar los elementos útiles de una forma más conveniente y luego hacerle cosas, como mostrarlos en una tabla de datos. Para hacer esto, podemos usar el método `split()`. En su forma más simple, esto toma un único parámetro, el caracter que quieres separar de la cadena, y devuelve las subcadenas entre el separador como elementos en una matriz.

```js
let data = "Madrid,Roma,Santiago,Berlin,Atenas";
let array = data.split(",");
```

Si quisieramos convertir a string un array, podemos ir en dirección inversa mediante el metodo `join()`.

```js
let data = "Madrid,Roma,Santiago,Berlin,Atenas";
let array = data.split(",");
let newData = array.join(",");
```

Nota: el metodo `toString()` sería equivalente a `join()` salvo que no podemos especificar el separador.

### Añadiendo elementos con push

Es facil adjuntar elementos al final de un array mediante el uso del metodo `.push()`

```js
let arr = [1, 2, 3];
arr.push(4);
// arr === [1,2,3,4]
```

Ejercicio:

```js
let myArray = [
  ["John", 23],
  ["cat", 2]
];
```

Incluye un nuevo array dentro del actual que contenga un string y un numero, luego modifica los tres array interiores e incluye un array interior con un string y un numero.

### Eliminando elementos con pop

Otra forma de cambiar los datos en una matriz es con la función .pop().

.pop () se usa para "extraer" un valor del final de una matriz. Podemos almacenar este valor "emergente" asignándolo a una variable. En otras palabras, .pop() elimina el último elemento de una matriz y devuelve ese elemento.

Cualquier tipo de entrada se puede "extraer" de una matriz: números, cadenas, incluso matrices anidadas.

```js
let threeArr = [1, 4, 6];
let oneDown = threeArr.pop();
console.log(oneDown); // Returns 6
console.log(threeArr); // Returns [1, 4]
```

Ejercicio:

Del ejercicio anterior saca el array introducido en el ultimo paso.

### Eliminando elementos con shift

pop () siempre elimina el último elemento de una matriz. ¿Qué pasa si quieres eliminar el primero?

Ahí es donde entra en juego .shift (). Funciona igual que .pop (), excepto que elimina el primer elemento en lugar del último.

```js
let ourArray = ["Stimpson", "J", ["cat"]];
let removedFromOurArray = ourArray.shift();
```

### Añadiendo elementos con unshift

No solo puede desplazar elementos fuera del comienzo de una matriz, sino que también puede desplazar elementos al comienzo de una matriz, es decir, agregar elementos delante de la matriz.

.unshift() funciona exactamente como .push(), pero en lugar de agregar el elemento al final de la matriz, unshift() agrega el elemento al comienzo de la matriz.

```js
let ourArray = ["Stimpson", "J", "cat"];
ourArray.shift(); // ourArray === ["J", "cat"]
ourArray.unshift("Happy");
// ourArray now equals ["Happy", "J", "cat"]
```

Ejercicio con arrays

## Metodos funcionales

La programación funcional es un paradigma de programación donde el valor de salida de una función depende solo de los argumentos que se pasan a la función, por lo que llamar a una función una cantidad determinada de veces siempre producirá el mismo resultado, sin importar la cantidad de veces que la llame . Esto contrasta con una gran cantidad de código común y contemporáneo, donde muchas funciones funcionan con un estado local o global, lo que puede terminar devolviendo diferentes resultados en diferentes ejecuciones. Un cambio en ese estado es un efecto secundario y, al eliminarlos, puede facilitar la comprensión y la predicción del comportamiento de su código.

Vamos a ver algunos metodos funcionales que cumplen esta definición sobre los arrays:

### map

El método map() crea un nuevo array con los resultados de la llamada a la función indicada aplicados a cada uno de sus elementos.

Ejemplo imperativo:

```js
const numbers = [1, 2, 3, 4];
let doubled = [];

for (let i = 0; i < numbers.length; i++) {
  doubled.push(numbers[i] * 2);
}

console.log(doubled);
```

Ejemplo funcional

```js
const numbers = [1, 2, 3, 4];
const doubled = numbers.map(n => n * 2);

console.log(doubled); //==> [2, 4, 6, 8];
```

En ambos ejemplos nuestro objetivo era obtener el doble de cada valor de un array. En la primera versión imperativa, usamos un contador que es nuestro estado de recorrido del primer array para saber dónde insertar el doble de cada valor en el segundo array.

Con la manera declarativa nos olvidamos del contador y nos centramos en especificar la función que queremos aplicar. Es decir, cuando usamos programación declarativa, las sentencias declaran QUÉ hacer, delegando en otra función, pero no CÓMO hacerlo, a diferencia de la programación imperativa

Ejercicio:

Dado el array:

```js
const arr = [{ foo: 1, bar: 2}, 1, {foo 3, bar: 4}, 5];
```

- Mapea los indices del array que no son objetos a objetos en los que su propiedad foo, es el numero y su propiedad bar es el numero más 2.

### forEach

El método forEach() ejecuta la función indicada una vez por cada elemento del array.

```js
const array1 = ["a", "b", "c"];

array1.forEach(element => console.log(element));
```

Ejercicio:

- Dado el array: ["Ana", "Adolfo", "Carmen", "Irene", "Juan", "Euripio", "Eduardo", "Jaime", "Jesus", "Sara", "Manuel", "Rodrigo", "Carlos"], concatenale los nombres de tus compañeros de clase.

- Alimenta un objeto cuyas propiedades son la primera letra de cada nombre y si se repite suma uno al valor de la propiedad.

Solo puedes usar forEach

```js
// Ejemplo

const letters = { a: 1, b: 2 };
```

### sort

El método sort() ordena los elementos de un array localmente y devuelve el array ordenado. El modo de ordenación por defecto responde a la posición del valor del string de acuerdo a su valor Unicode. Y puede ser diferente por navegador. Esto significa que en la medida de lo posible deberías implementar una función que garantize el orden de una manera estable.

Si no se provee la funcion de comparación, los elementos son ordenados convirtiéndolos a strings y comparando la posición del valor Unicode de dichos strings. Por ejemplo, "Cherry" viene antes que "banana" ( por ir las mayúsculas antes que las minúsculas en la codificación Unicode) . En un ordenamiento numérico, 9 está antes que 80, pero dado que los números son convertidos a strings y ordenados según el valor Unicode, el resultado será "80" antes que "9".

Ejemplo:

```js
let nombres = ["Victor", "Ana", "Carlos"];
nombres.sort();
// Ana, Carlos, Victor

let numeros = [9, 5, 80];
numeros.sort();
// 5, 80, 9
```

Si proveemos una funcion de comparación, es posible dar nuestro algoritmo de ordenación.
Los elementos del array en nuestra funcion de comparación, seran ordenados conforme al valor que retorne dicha función de comparación.

Recibira dos elementos del array, pongamos que a y b.

- Si la funcion de comparación retorna menos que 0, entonces b va antes que a.
- Si la funcion de comparación retorna 0, no habra cambios, pero estaran ordenados respecto al resto
- Si la funcion de comparación retorna mas que 0, entonces a va antes que b.

```js
function compare(a, b) {
  if (a es menor que b según criterio de ordenamiento) {
    return -1;
  }
  if (a es mayor que b según criterio de ordenamiento) {
    return 1;
  }
  // a debe ser igual b
  return 0;
}
```

Ejemplo:

```js
let numeros = [9, 5, 80];
numeros.sort((a, b) => a - b);

// [5, 9, 80]
```

Los objectos pueden ser ordenados por el valor de una de sus propiedades.

```js
let items = [
  { name: "Edward", value: 21 },
  { name: "Sharpe", value: 37 },
  { name: "And", value: 45 },
  { name: "The", value: -12 },
  { name: "Magnetic", value: 13 },
  { name: "Zeros", value: 37 }
];

function byName(a, b) {
  if (a.name > b.name) {
    return 1;
  }
  if (a.name < b.name) {
    return -1;
  }
  // a must be equal to b
  return 0;
}

items.sort(byName);
```

Ejercicio:

Ordena el siguiente array por id y autor con titulo.

```js
let library = [
  { author: "Bill Gates", title: "The Road Ahead", libraryID: 1254 },
  { author: "Steve Jobs", title: "Walter Isaacson", libraryID: 4264 },
  {
    author: "Suzanne Collins",
    title: "Mockingjay: The Final Book of The Hunger Games",
    libraryID: 3245
  }
];
```

### find

El método find() devuelve el valor del primer elemento del array que cumple la función de prueba proporcionada. En cualquier otro caso se devuelve undefined.

No debe usarse si:

- Si necesitas el índice del elemento encontrado en el array, utiliza findIndex().

- Si necesitas encontrar el índice de un elemento, Array.prototype.indexOf(). (Es similar a findIndex(), pero comprueba la igualdad de cada elemento con el valor en lugar de usar una función de prueba.)

- Si necesitas encontrar si un valor existe en un array, utiliza Array.prototype.includes().

Ejemplo:

```js
let library = [
  { author: "Bill Gates", title: "The Road Ahead", libraryID: 1254 },
  { author: "Steve Jobs", title: "Walter Isaacson", libraryID: 4264 },
  {
    author: "Suzanne Collins",
    title: "Mockingjay: The Final Book of The Hunger Games",
    libraryID: 3245
  }
];

const billGatesBook = library.find(book => book.author === "Bill Gates");
```

## reduce

El método reduce() ejecuta una función reductora sobre cada elemento de un array, devolviendo como resultado un único valor.

Recibe 2 parametros fundamentales, el accumulador y el siguiente elemento.

El valor devuelto de la función reductora se asigna al acumulador, cuyo valor se recuerda en cada iteración de la matriz y, en última instancia, se convierte en el valor final, único y resultante.

```js
const array1 = [1, 2, 3, 4];
const reducer = (accumulator, currentValue) => accumulator + currentValue;

// 5 + 1 + 2 + 3 + 4
console.log(array1.reduce(reducer, 5));
// expected output: 15
```

Ejercicio:

Dado el array siguiente:

```js
const arr = [{ foo: 1, bar: 2}, 1, {foo 3, bar: 4}, 5];
```

- Suma todos los numeros presentes en el array
