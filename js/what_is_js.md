# ¿Qué es JS?

JavaScript es un lenguaje de programación que te permite realizar actividades complejas en una página web.

Cada vez que ves contenido dinamico en 3D, o actualizaciones del mismo, puedes estar seguro de que JS esta involucrado.

Por ejemplo:

```html
<p>Me llamo:</p>
```

```js
const p = document.querySelector("p");

function updateP() {
  const name = prompt("Introduce el nuevo nombre");
  p.textContent = `${p.textContent}: ${name}`;
}

p.addEventListener("click", updateP);
```

## ¿Qué podemos hacer con JS?

JavaScript nos permite realizar tareas como:

- Almacenar valores útiles en variables.
- Operar con el valor de las variables.
- Programar respuestas a eventos que ocurren dentro de nuestra web.

Más interesante aun es la posibilidad de incluir APIs en nuestro código JS.

Las APIs (Application Programming Interfaces) no son sino bloques de código que nos permiten implementar funciones complejas en nuestras apps sin tener que construir las mismas desde cero, lo cual haría el proyecto imposible de terminar.

Así como las herramientas para construir una casa, es lo mismo para las cosas de programación – Es mucho más fácil tomar los paneles que ya estén cortados y atornillarlos para hacer un estante de libros, ya que es más trabajoso diseñarlo por ti mismo, ir y encontrar la madera correcta, cortarla del tamaño correcto y lijarla, buscar los tornillos del tamaño correcto y ensamblarla para hacer un estante de libros.

Las APIs podemos dividirlas en dos categorías mayoritarias:

- Browser APIs:
  on construidos dentro de tu buscador web, y son capaces de exponer información desde la cercanía en base al ambiente de tu ordenador, o hacer cosas complejas. Por ejemplo:

  - El DOM (Modelo de Objeto de Documento) API te permite manipular, crear, remover y cambiar códigos escritos en lenguaje HTML y CSS, incluso aplicando dinámicamente nuevos estilos a tu página web, etc. Cada vez que aparezca un aviso (popup) en una página web, o nuevo contenido a mostrarse en ella, (Así como vimos en el ejemplo anterior de nuestro sencillo demo) es tan sólo un ejemplo, de lo que se puede hacer con la acción DOM.

  - La Localización-Geo API restablece la información geográfica, Así es como Google Maps permite encontrar tu dirección e imprimirla en un mapa.

  - Las Canvas y el WebGL API te permiten crear animaciones y gráficos en 2D y 3D. La gente está haciendo cosas increíbles usando estas tecnologías web – puedes ver los Chrome Experiments y webglsamples.

  - Video y Sonido APIs, como HTMLMediaElement y WebRTCte permite crear cosas realmente interesantes, tanto como poner música y vídeo en una página web o grabar un vídeo desde tu cámara web y que otra persona pueda verla desde su computadora (Prueba nuestro ejercicio Snapshot demo para tener una idea).

- APIs de terceros: generalmente hay que incluir su codigo en nuesdtro proyecto para usarlo, ejemplos:

  - Twilio API: Nos permite montar sistemas de videollamada o mensajeria instantanea.
  - Google Maps API: permite incrustar mapas personalizados en tu sitio web y otro tipo de funcionalidades.

## ¿Qué hace JS en la web?

Cuando cargas una página Web en tu navegador, tu código (HTML, CSS y JavaScript) es leído dentro de un ambiente de ejecución (pestaña del navegador). Esto es como una fábrica que coge la materia prima (Las líneas de código) y lo presenta como el producto final (la página Web).

![ejecucion en navegador](assets/execution.png)

JavaScript es ejecutado por el motor del navegador de JavaScript, **despues** que el código HTML y CSS han sido juntados. Esto asegura que el estilo y la estructura de la página están en su lugar en el momento en que JavaScript comienza a ejecutarse.

Esto es algo muy bueno, algo muy común en el uso de JavaScript para modificar dinámicamente el código HTML y CSS, para que la interfaz de usuario sea actualizada, usando DOM. Si intentasemos cargar JavaScript y hacerlo funcionar antes de que sea leído el código HTML y CSS, se verá afectado, ocurriendo errores de programación.

## Seguridad

Cada pestaña del navegador se considera como un compartimento separado para hacer funcionar el código, significa que en la mayoría de los casos los códigos en cada pestaña funcionan completamente separados, y el código en una pestaña no puede afectar directamente el código de otra pestaña, o en otro navegador.

## Orden de ejecución

Cuando el navegador encuentra un bloque de JavaScript, generalmente lo corre en orden, de arriba hacia abajo. Esto significa que tienes que tener cuidado en qué orden pones las cosas.

En nuestro primer ejemplo si invirtiesemos el orden de las dos primeras líneas el código no funcionaría, ya que la variable `p` no estaría definida antes de su uso.

## Como incluir JS en tu web

JavaScript es aplicado a tu página en HTML de una manera similar al CSS. Donde el CSS usa el elemento `<link>` para aplicar estilos-de-hoja externos y el tag `<style>` es un elemento para aplicar estilos-de-hoja interno al HTML, JavaScript solamente necesita de un solo amigo en el mundo del HTML - que es el elemento `<script>`.

### Incluido en el HTML

Dado el siguiente HTML:

```html
<body>
  <button>Click me</button>
</body>
```

Podemos incluir código que renderize parrafos de manera dinamica mediante la introducción de la etiqueta `<script>` justo antes del cierre del body.

```html
<script>
  function createParagraph() {
    var para = document.createElement("p");
    para.textContent = "You clicked the button!";
    document.body.appendChild(para);
  }

  var buttons = document.querySelectorAll("button");

  for (var i = 0; i < buttons.length; i++) {
    buttons[i].addEventListener("click", createParagraph);
  }
</script>
```

### Externo

La etiqueta `<script>` nos vale para cargar script js de manera externa, por ejemplo:

```html
<script src="script.js"></script>
```

### Estrategias de carga

Hay una serie de problemas relacionados con conseguir que los scripts se carguen en el momento adecuado. Un problema común es que todo el HTML de una página se carga en el orden en que aparece. Si utiliza JavaScript para manipular elementos de la página (o más exactamente, el Document Object Model), su código no funcionará si el JavaScript es cargado y analizado antes que el código HTML al que está intentando manipular.

Para evitarlo, podemos seguir distintas estrategias de carga:

- Mediante detección de eventos de carga del DOM:

  ```js
  document.addEventListener("DOMContentLoaded", function() {});
  ```

  Se trata de un receptor de eventos, que escucha el evento "DOMContentLoaded" del navegador, lo que significa que el cuerpo HTML está completamente cargado y analizado. El JavaScript dentro de este bloque no se ejecutará hasta después de que se dispare ese evento, por lo tanto se evita el error.

- Desplazar la carga del script al final del body. (deprecado).

- `async` y `defer`:
  Los scripts async descargarán el script sin bloquear la renderización de la página y lo ejecutarán tan pronto como el script termine de descargarse. No se garantiza que los scripts se ejecutarán en un orden específico, sólo que no impedirán que se muestre el resto de la página. Es mejor usar async cuando los scripts de la página se ejecutan independientemente uno del otro y no dependen de ningún otro script de la página. Por ejemplo:

  ```html
  <script async src="js/vendor/jquery.js"></script>

  <script async src="js/script2.js"></script>

  <script async src="js/script3.js"></script>
  ```

  El problema de esta técnica es que no puedes garantizar el orden de la carga de los scripts.

  `defer` por su parte si nos garantiza el orden:

  ```html
  <script defer src="js/vendor/jquery.js"></script>

  <script defer src="js/script2.js"></script>

  <script defer src="js/script3.js"></script>
  ```
