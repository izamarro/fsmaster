# Trabajando con asincronía

Hasta ahora todos los algorítmos que hemos planteado trabajan de forma síncrona, es decir la ejecución de uno, bloquea la ejecución del siguiente hasta su finalización y así sucesivamente.

Pero, ¿qué ocurre en una situación en la que no sabemos cuando van terminar de ocurrir ciertas acciones? Por ejemplo, imaginemos que estamos llamando a un servidor desde nuestro código JS, para obtener los precios del bitcoin en determinadas fechas y pintarlos de manera organizada en una tabla. No sabemos cuando el servidor va a terminar de contestar, ni sabemos si lo hará.

Trabajar con la incertidumbre del tiempo en nuestro código, y de la resolución de nuestras acciones, es posible en JS gracias al uso de código asíncrono.

## Modelo de concurrencia y de paralelismo

Concurrencia y paralelismo son conceptos de manejo de las acciones asincronas, con un matiz diferente. Es por esto que muy a menudo se confunden y se utilizan erróneamente:

- Concurrencia: cuando dos o mas tareas progresan simultáneamente.

- Paralelismo: cuando dos o mas tareas se ejecutan, literalmente, a la vez, en el mismo instante de tiempo.

_Nota: que varias tareas progresen simultáneamente no tiene porque significar que sucedan al mismo tiempo. La concurrencia aborda un problema más general, el paralelismo es un sub-caso de la concurrencia donde las cosas suceden exactamente al mismo tiempo._

Mucha gente aún sigue creyendo que la concurrencia implica necesariamente más de un hilo o thread de ejecución. Esto no es cierto. El multiplexado, por ejemplo, es un mecanismo común para implementar concurrencia en escenarios donde los recursos son limitados. Piensa en cualquier sistema operativo moderno haciendo multitarea con un único nucleo. Se trocean las tareas en tareas más pequeñas y las entrelaza, de modo que cada una de ellas se ejecutará durante un breve instante. Sin embargo, a largo plazo, la impresión es que todas progresan a la vez.

![](assets/concurrency_es.png)

En el gráfico vemos varios escenarios:

- 1: no es ni concurrente ni paralelo. Es simplemente una ejecución secuencial.
- 2, 3 y 4: son escenarios donde se ilustra la concurrencia bajo distintas técnicas:

  - Escenario 3: muestra como la concurrencia puede conseguirse con un único thread. La tarea es troceada en varias partes para parecer que mantiene un ritmo de resolucion constante.

  - Escenario 2 y 4: ilustran paralelismo, utilizando multiples hilos donde las tareas o subtareas corren en paralelo exactamente al mismo tiempo. A nivel de hilo, el escenario 2 es secuencial, mientras que 4 aplica multiplexado.

## Tareas limitadas por la CPU vs Tareas limitadas por Entrada/Salida

Casi todos los ejemplos anteriores que hemos visto, son tareas que consumían recursos de CPU. Estas tareas se componen de operaciones cuyo código asociado a ellas, será ejecutado en nuestra aplicación. Se las conoce como operaciones limitadas por CPU, (CPU-Bound)

Sin embargo, es frecuente encontrar otro tipo de operaciones en nuestros programas, por ejemplo: leer un fichero en disco, acceder a una base de datos externa o consultar datos a través de la red. Todas estas operaciones de entrada/salida disparan peticiones especiales que son atendidas fuera del contexto de nuestra aplicación. Por ejemplo, desde nuestro programa se ordena la lectura de un fichero en disco, pero es el sistema operativo y el propio disco los involucrados en completar esta petición. Por lo tanto, las operaciones I/O-bound (limitadas por entrada/salida) no corren o se ejecutan en el dominio de nuestra aplicación.

![](assets/cpu_io_es.png)

Cuando decimos que una operación esta limitada por algo, hablamos de la existencia de un cuello de botella con el recurso que la limita. De este modo, si incrementamos la potencia de nuestra CPU, mejoraremos el rendimiento de las operaciones CPU-bound, mientras que una mejora en el sistema de entrada/salida favorecerá el desempeño de las tareas I/O-bound.

La naturaleza de las operaciones CPU-bound es intrínsecamente síncrona (o secuencial, si la CPU esta ocupada no puede ejecutar otra tarea hasta que se libere) a menos que se utilicen mecanismos de concurrencia como los vistos anteriormente (entrelazado o paralelismo por ejemplo). ¿Qué sucede con las operaciones I/O-bound? Un hecho interesante es que pueden ser asíncronas, y la asincronía es una forma muy útil de concurrencia.

### Tareas I/O: Bloqueante vs No bloqueantes y Sincrono vs Asincrono

Durante el contexto de ejecución de una tarea I/O, distinguimos dos fases:

- Fase de Espera a que el dispositivo este listo, a que la operación se complete o que los datos esten disponibles.

- Fase de Ejecución entendida como la propia respuesta, lo que sea que quiera hacerse como respuesta a los datos recibidos.

Distinguimos dos naturalezas de tareas durante la fase de espera:

- Bloqueante: Una llamada u operación bloqueante no devuelve el control a nuestra aplicación hasta que se ha completado. El thread queda bloqueado en estado de espera.

- No Bloqueante: Una llamada no bloqueante devuelve inmediatamente con independencia del resultado. En caso de que se haya completado, devolverá los datos solicitados. En caso contrario (si la operación no ha podido ser satisfecha) podría devolver un código de error. En este caso se sobreentiende que algún tipo de encolado se realiza para finalizar el trabajo o para lanzar una nueva petición más tarde, en un mejor momento.

![](assets/blocking_non_blocking_es.png)

Síncrono vs Asíncrono se refiere a cuando tendrá lugar la respuesta:

- Síncrono: es frecuente emplear 'bloqueante' y 'síncrono' como sinónimos, dando a entender que toda la operación de entrada/salida se ejecuta de forma secuencial y, por tanto, debemos esperar a que se complete para procesar el resultado.

- Asíncrono: la finalización de la operación I/O se señaliza más tarde, mediante un mecanismo específico como por ejemplo un callback, una promesa o un evento, lo que hace posible que la respuesta sea procesada en diferido. Como se puede adivinar, su comportamiento es no bloqueante ya que la llamda I/O devuelve inmediatamente.

![](assets/sync_async_es.png)

### Como lo maneja JS

JS fue diseñado para ser ejecutado en navegadores, trabajar con peticiones sobre la red y procesar las interacciones de usuario, al tiempo que se mantiene una interfaz fluida. Ser bloqueante o síncrono no ayudaría a conseguir estos objetivos, es por ello que Javascript ha evolucionado intencionadamente pensando en operaciones de tipo I/O.

**Javascript utiliza un modelo asíncrono y no bloqueante, con un loop de eventos implementado con un único thread para sus interfaces de entrada/salida.**

Gracias a esta solución, Javascript es áltamente concurrente a pesar de emplear un único thread.

![](assets/async_call_steps_es.png)

### Loop de eventos JS

_Recomendado leer: https://developer.mozilla.org/es/docs/Web/JavaScript/EventLoop_

![](event_loop_model_es.png)

Distinguimos:

- Call Stack

  La pila de llamadas, se encarga de albergar las instrucciones que deben ejecutarse. Nos indica en que punto del programa estamos, por donde vamos. Cada llamada a una función de nuestra aplicación, entra a la pila generando un nuevo frame (bloque de memoria reservada para los argumentos y variables locales de dicha función). Por tanto, cuando se llama a una función, su frame es insertado arriba en la pila, cuando una función se ha completado y devuelve, su frame se saca de la pila también por arriba. El funcionamiento es de tipo LIFO: last in, first out. De este modo, las llamadas a función que están dentro de otra función contenedora son apiladas encima y serán atendidas primero.

- Heap

  Región de memoria libre, normalmente de gran tamaño, dedicada al alojamiento dinámico de objetos. Es compartida por todo el programa y controlada por un recolector de basura que se encarga de liberar aquello que no se necesita.

- Cola o Queue

  Cada vez que nuestro programa recibe una notificación del exterior o de otro contexto distinto al de la aplicación (como es el caso de operaciones asíncronas), el mensaje se inserta en una cola de mensajes pendientes y se registra su callback correspondiente. Recordemos que un callback era la función que se ejecutará como respuesta.

- Loop de Eventos

  Cuando la pila de llamadas (call stack) se vacía, es decir, no hay nada más que ejecutar, se procesan los mensajes de la cola. Con cada 'tick' del bucle de eventos, se procesa un nuevo mensaje. Este procesamiento consiste en llamar al callback asociado a cada mensaje lo que dará lugar a un nuevo frame en la pila de llamadas. Este frame inicial puede derivar en muchos más, todo depende del contenido del callback. Un mensaje se termina de procesar cuando la pila vuleve a estar vacía de nuevo. A este comportamiento se le conoce como 'run-to-completion'.

![](assets/event_loop_tick_animated_es.gif)

De esta forma, podemos entender la cola como el almacén de los mensajes (notificaciones) y sus callbacks asociados mientras que el loop de eventos es el mecanismo para lanzarlos. Este mecanismo sigue un comportamiento síncrono: cada mensaje debe ser procesado de forma completa para que pueda comenzar el siguiente.

Una de las implicaciones más relevantes de este bucle de eventos es que los callbacks no serán despachados tan pronto como sean encolados, sino que deben esperar su turno. Este tiempo de espera dependerá del numero de mensajes pendientes de procesar (por delante en la cola) así como del tiempo que se tardará en cada uno de ellos. Aunque pueda parecer obvio, esto explica la razón por la cual la finalización de una operación asíncrona no puede predecirse con seguridad.

El loop de eventos no está libre de problemas, y podrían darse situaciones comprometidas en los siguientes casos:

- La pila de llamadas no se vacía ya que nuestra aplicación hace uso intensivo de ella. No habrá tick en el bucle de eventos y por tanto los mensajes no se procesan.

- El flujo de mensajes que se van encolando es mayor que el de mensajes procesados. Demasiados eventos a la vez.

- Un callback requiere procesamiento intensivo y acapara la pila. De nuevo bloqueamos los ticks del bucle de eventos y el resto de mensajes no se despachan.

Lo más probable es que un cuello de botella se produzca como consecuencia de una mezcla de factores. En cualquier caso, acabarían retrasando el flujo de ejecución. Y por tanto retrasando el renderizado, el procesado de eventos, etc. La experiencia de usuario se degradaría y la aplicación dejaría de responder de forma fluida. Para evitar esta situación, recuerda siempre mantener los callbacks lo más ligeros posible. En general, evita código que acapare la CPU y permite que el loop de eventos se ejecute a buen ritmo.

Nota: Recientemente se han incluido mecanismos de paralelismo en JS, que puedes consultar en:

- https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/SharedArrayBuffer
- https://developer.mozilla.org/en-US/docs/Web/API/Web_Workers_API

## Patrones asíncronos en JS

### Callbacks

Los callbacks son la pieza clave para que Javascript pueda funcionar de forma asíncrona. De hecho, el resto de patrones asíncronos en Javascript está basado en callbacks de un modo u otro, simplemente añaden azúcar sintáctico para trabajar con ellos más cómodamente.

Un callback no es más que una función que se pasa como argumento de otra función, y que será invocada para completar algún tipo de acción. En nuestro contexto asíncrono, un callback representa el '¿Qué quieres hacer una vez que tu operación asíncrona termine?'. Por tanto, es el trozo de código que será ejecutado una vez que una operación asíncrona notifique que ha terminado. Esta ejecución se hará en algún momento futuro, gracias al mecanismo que implementa el bucle de eventos.

Fíjate en el siguiente ejemplo sencillo utilizando un callback:

```js
const myCallback = () => console.log("Hola Mundo con callback!");
setTimeout(myCallback, 1000);
```

`setTimeout` es una función asíncrona que programa la ejecución de un callback una vez ha transcurrido, como mínimo, una determinada cantidad de tiempo (1 segundo en el ejemplo anterior). A tal fin, dispara un timer en un contexto externo y registra el callback para ser ejecutado una vez que el timer termine. En resumen, retrasa una ejecución, como mínimo, la cantidad especificada de tiempo.

Recuerda, un callback que se añade al loop de eventos debe esperar su turno.

Uno de los inconvenientes clásicos de los callbacks, además de la indentación, resta legibilidad, dificulta su mantenimiento y añade complejidad ciclomática. Al Callback Hell también se le conoce como Pyramid of Doom o Hadouken.

```js
setTimeout(function() {
  console.log("Etapa 1 completada");
  setTimeout(function() {
    console.log("Etapa 2 completada");
    setTimeout(function() {
      console.log("Etapa 3 completada");
      setTimeout(function() {
        console.log("Etapa 4 completada");
        // Podríamos continuar hasta el infinito...
      }, 4000);
    }, 3000);
  }, 2000);
}, 1000);
```

### Promesas

_Recomendable: leer https://developer.mozilla.org/es/docs/Web/JavaScript/Guide/Usar_promesas_

Una promesa es un objeto que representa el resultado de una operación asíncrona. Este resultado podría estar disponible ahora o en el futuro. Las promesas se basan en callbacks.

Cuando llamamos a una función asíncrona implementada con este patrón, nos devolverá inmediatamente una promesa como garantía de que la operación asíncrona finalizará en algún momento, ya sea con éxito o con fallo. Una vez que tengamos el objeto promesa en nuestro poder, registramos un par de callbacks: uno para indicarle a la promesa 'que debe hacer en caso de que todo vaya bien' (resolución de la promesa o resolve) y otro para determinar 'que hacer en caso de fallo' (rechazo de la promesa o reject).

A resumidas cuentas, una promesa es un objeto al que le adjuntamos callbacks, en lugar de pasarlos directamente a la función asíncrona. La forma en que registramos esos dos callbacks es mediante el método .then(resolveCallback, rejectCallback). En terminología de promesas, decimos que una promesa se resuelve con éxito (resolved) o se rechaza con fallo (rejected). Echa un vistazo al siguiente ejemplo:

```js
const currentURL = document.URL.toString();
const promise = fetch(currentURL);

promise.then(
  (result) => console.log(result),
  (e) => console.error(error))
);
```

En el ejemplo anterior, pedimos al servidor un recurso utilizando la función asíncrona `fetch` y nos devuelve una promesa. Configuramos la promesa con dos callbacks: uno para resolver la promesa, que mostrará la página por consola en caso de éxito, y otro para rechazarla en caso de fallo que mostrará el error asociado.

Una característica interesante de las promesas es que pueden ser encadenadas. Esto es posible gracias a que la llamada .then() también devuelve una promesa. Esta nueva promesa devuelta será resuelta con el valor que retorne el callback de resolución original (el que hemos pasado al primer then()):

```js
fetch(document.URL.toString())
  .then(result => console.log(result))
  .then(() => console.log(`Fetch completado, página mostrada`))
  .catch(e => console.log(`Error capturado:  ${e}`));
```

### Creando Promesas

Una promesa se crea instanciando un nuevo objeto Promise. En el momento de la creación, en el constructor, debemos especificar un callback que contenga la carga de la promesa, aquello que la promesa debe hacer.

Este callback nos provee de dos argumentos: resolveCallback y rejectCallback. Son los dos mismos callbacks registrados al consumir la promesa. De este modo, depende de ti como desarrollador llamar a resolveCallback y rejectCallback cuando sea necesario para señalizar que la promesa ha sido completada con éxito o con fallo.

Ejemplo:

```js
const checkServer = url => {
  return new Promise((resolve, reject) => {
    fetch(url)
      .then(response =>
        resolve(
          `Estado del Servidor: ${response.status === 200 ? "OK" : "NOT OK"}`
        )
      )
      .catch(() => reject(`Error al localizar URL`));
  });
};

checkServer(document.URL.toString())
  .then(result => console.log(result))
  .catch(e => console.log(e));
```

Las promesas son muy útiles para envolver antiguas APIs asíncronas que funcionan a través de callbacks puros. De esta forma podemos hacerlas funcionar via promesas:

```js
const delay = time =>
  new Promise(resolveCallback => setTimeout(resolveCallback, time));

delay(3000)
  .then(() => console.log(`Este es un retardo de al menos 3 segundos`))
  .catch(() => console.log(`Retardo fallido`));
```

Si tratásemos las promesas con la misma prioridad que el resto de mensajes asíncronos, retrasariamos innecesariamente la ejecución de sus callbacks. Podrían acabar 'perdiéndose' entre otros mensajes en la cola de eventos, como por ejemplo mensajes de renderizado o eventos de usuario. Dado que las promesas suelen ser fruto de la interacción con importantes APIs asíncronas, y por tanto, son una parte importante de la que se sirve tu aplicación, no queremos que se retrasen. Es preferible darles una prioridad mayor. El estándar ECMAScript describe el uso de una cola especial, llamada cola de microtareas o microtask queue, con una mayor prioridad dedicada a la gestión de callbacks de promesas.

La idea detrás de una segunda cola de alta prioridad es que los callbacks de cada promesa se almacenen aquí, de modo que cuando un nuevo tick del bucle de eventos tenga lugar, esta cola prioritaria será atendida primero. Asi pues, nos aseguramos que los callbacks de las promesas se ejecutarán en un futuro, si, pero lo antes posible.

```js
setTimeout(() => console.log("1"), 0);
Promise.resolve().then(() => console.log("2"));

// 2
// 1
```

## Fetch API

fetch es una abstracción de JS que nos permite realizar peticiones http, funciona mediante el uso de promesas y con lo cual podemos dar comportamiento a su resolución o fallo.

_Nota: recomendable la lectura de https://developer.mozilla.org/es/docs/Web/API/Fetch_API/Utilizando_Fetch_

Ejemplo:

```js
fetch("/json/cats.json")
  .then(response => response.json())
  .then(data => {
    document.getElementById("message").innerHTML = JSON.stringify(data);
  });
```

La primera línea es la que hace la solicitud. Por lo tanto, fetch (URL) realiza una solicitud GET a la URL especificada. El método devuelve una promesa.

Después de que se devuelve una Promesa, si la solicitud fue bien, se ejecuta el método, que toma la respuesta y la convierte al formato JSON.

El método then también devuelve una Promesa, que se maneja con el siguiente método then. ¡El argumento en el segundo es el objeto JSON.

Despues seleccionamos el elemento que recibirá los datos mediante `document.getElementById()`. Luego modifica el código HTML del elemento insertando una cadena creada a partir del objeto JSON devuelto por la solicitud.

Ejercicio:
Implementa una llamada a la api de coinDesk: https://api.coindesk.com/v1/bpi/historical/close.json?start=2013-09-01&end=2013-09-05

Aceptando el parametro fecha de inicio, fecha de fin y mapea los precios del bitcoin ordenandolos por fecha.
Puedes hacer uso del metodo `Object.values(tuObjeto)` para obtener un array de valores de las propiedades de un objeto.

Implementa un callback de reject de la promesa, en el que logeas por consola un mensaje de error. Prueba a usarlo haciendo fallar la llamada a la api.

Ejercicio:

```js
function job() {
  return new Promise(function(resolve, reject) {
    reject();
  });
}

let promise = job();

promise

  .then(function() {
    console.log("Success 1");
  })

  .then(function() {
    console.log("Success 2");
  })

  .then(function() {
    console.log("Success 3");
  })

  .catch(function() {
    console.log("Error 1");
  })

  .then(function() {
    console.log("Success 4");
  });
```

¿Cual es el resultado de la siguiente promesa?

- Error 1
- Success 1, Error 1
- Success 1, Success 2, Success 3, Success 4
- Success 1, Success 2, Success 3, Error 1, Success 4
- Error1, Success 1, Success 2, Success 3, Success 4
- Error1, Success 4

Compruebalo!

Ejercicio:

```js
function job(state) {
  return new Promise(function(resolve, reject) {
    if (state) {
      resolve("success");
    } else {
      reject("error");
    }
  });
}

let promise = job(true);

promise

  .then(function(data) {
    console.log(data);

    return job(false);
  })

  .catch(function(error) {
    console.log(error);

    return "Error caught";
  })

  .then(function(data) {
    console.log(data);

    return job(true);
  })

  .catch(function(error) {
    console.log(error);
  });
```

¿Cual es el resultado de la siguiente promesa?

- error, success, Error caught
- success, success
- success, error, success, error
- success, error, Error caught
- error, Error caught, success
- success, error, error
- success, success, success

Compruebalo!

Ejercicio:

Escribe una función testNum que tome un número como argumento y devuelva una Promesa que pruebe si el valor es menor o mayor que el valor 10.

Ejercicio:

Escribe dos funciones que usen Promesas que puedes encadenar. La primera función, makeAllCaps (), tomará una serie de palabras y las capitalizará, y luego la segunda función, sortWords (), ordenará las palabras en orden alfabético. Si el array contiene cualquier cosa menos cadenas, debería arrojar un error.

Ejercicio:

Crea una función que toma un solo parámetro y devuelve una nueva promesa usando setTimeout, después de 500 milisegundos, la promesa será resuelta o rechazada. Si la entrada es una cadena, la promesa se resuelve con esa misma cadena en mayúsculas. Si la entrada es cualquier cosa menos una cadena se rechaza con esa misma entrada.

Llama a la función delayedUpperCase.

Ejercicio:

Sabiendo que la API de DOM, document, nos permite acceder acceder y crear un elemento de la siguiente manera:

```js
const element = document.createElement("p");
element.innerHTML = "foo";

// puedes añadir una clase a un elemento mediante el uso de:
element.classList.add("miClase");
```

Crea la triada, html, js, css:

Llama a la api de coindesk usada anteriormente, mapea cada valor del bpi a un elemento `<li>fecha: precio</li>` y añadelo a un `<ul>` que tengas en el html.

- Incluye reglas de precio, de tal manera que si el precio es superior a inferior a un valor, ese `<li>` se pinta de color verde. (Establece la clase correcta) y en negrita, y si es superior en rojo.

  - Los limites deben pasarse por parametros a la funcion:

  ```js
  render("11-11-2015", "12-12-2018", 100);
  ```

  - Repasa el concepto de fecha en JS: https://developer.mozilla.org/es/docs/Web/JavaScript/Referencia/Objetos_globales/Date e intenta que la funcion no acepte strings sino objetos fecha de JS.

- Sabiendo que la api de dom permite localizar un elemento mediante:

  ```js
  const button = document.querySelector("#idDeMiBoton");
  ```

  y que puedes añadirle una funcion de callback a su onClick:

  ```js
  const miCallback = () => console.log("ejecutando callback");
  const button = document.querySelector("#idDeMiBoton");
  button.onclick = miCallback;
  ```

  Reordena los precios de manera descendente
