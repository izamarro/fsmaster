# Herencia y cadena de prototipos

Cuando hablamos de herencia en programación no nos referimos a que un objeto lejano nos ha podido dejar una fortuna...

En realidad se trata de uno de los pilares fundamentales de la programación orientada a objetos. Es el mecanismo por el cual una clase permite heredar las características (atributos y métodos) de otra clase.

La herencia permite que se puedan definir nuevas clases basadas de unas ya existentes a fin de reutilizar el código, generando así una jerarquía de clases dentro de una aplicación. Si una clase deriva de otra, esta hereda sus atributos y métodos y puede añadir nuevos atributos, métodos o redefinir los heredados.

No hay nada mejor en programación que poder usar el mismo código una y otra vez para hacer nuestro desarrollo más rápido y eficiente. El concepto de herencia ofrece mucho juego. Gracias a esto, lograremos un código mucho más limpio, estructurado y con menos líneas, lo que lo hace más legible.

En algunos lenguajes como Java tenemos que tener claro cómo llamar a la clase principal de la que heredamos y aquella que hereda de ella, así, la clase que se hereda se denomina superclase. La clase que hereda se llama subclase. Por lo tanto, una subclase es una versión especializada de una superclase. Hereda todas las variables y métodos definidos por la superclase y agrega sus propios elementos únicos.

- Superclase: la clase cuyas características se heredan se conoce como superclase (o una clase base o una clase principal).

- Subclase: la clase que hereda la otra clase se conoce como subclase (o una clase derivada, clase extendida o clase hija). La subclase puede agregar sus propios campos y métodos, además de los campos y métodos de la superclase.

- Reutilización: la herencia respalda el concepto de “reutilización”, es decir, cuando queremos crear una clase nueva y ya hay una clase que incluye parte del código que queremos, podemos derivar nuestra nueva clase de la clase existente. Al hacer esto, estamos reutilizando los campos/atributos y métodos de la clase existente.

Por ejemplo:

```text
clase Animal {
    propiedades.patas = null;
}

clase Perro extiende Animal {
    propiedades.patas = 4;
    metodo.ladrar {
        log(guau!)
    }
}
```

## Herencia en JS

En el anterior ejemplo declaramos una clase Animal y una clase Perro que hereda de la misma. Perro recibe todas las propiedades de la clase Animal, es decir tiene la propiedad patas, pero puede extenderla con un metodo ladrar que es unico de los perros.

JavaScript provoca cierta confusión en desarrolladores con experiencia en lenguajes basados en clases (como Java o C++), por ser dinámico y no proporcionar una implementación de clases en sí mismo (la palabra clave class se introdujo en ES2015, pero sólo para endulzar la sintaxis, ya que JavaScript sigue estando basado en prototipos).

En lo que a herencia se refiere, JavaScript sólo tiene una estructura: objetos. Cada objeto tiene una propiedad privada (referida como su [[Prototype]]) que mantiene un enlace a otro objeto llamado su prototipo. Ese objeto prototipo tiene su propio prototipo, y así sucesivamente hasta que se alcanza un objeto cuyo prototipo es null. Por definición, null no tiene prototipo, y actúa como el enlace final de esta cadena de prototipos.

Casi todos los objetos en JavaScript son instancias de Object que se sitúa a la cabeza de la cadena de prototipos.

A pesar de que a menudo esto se considera como una de las principales debilidades de JavaScript, el modelo de herencia de prototipos es de hecho más potente que el modelo clásico. Por ejemplo, es bastante simple construir un modelo clásico a partir de un modelo de prototipos.

## Herencia de propiedades

```js
// Supongamos que tenemos un objeto persona, con propiedades manos y piernas:
// {manos: 2, piernas: 2}
// persona.[[animal]] ( su prototipo ) tiene propiedades cabeza y brazos:
// {cabeza: 1, brazos: 2}
// Finalmente, persona.[[animal]].[[Prototype]] seria null.
// Este es el final de la cadena de prototipos, ya que null,
// por definición, no tiene [[Prototype]].
// La cadena completa de prototipos es:
// {manos:2, piernas:2} ---> {cabeza:1, piernas:2} ---> null

console.log(persona.manos); // 2
// ¿Hay una propiedad 'manos' en persona? Sí, y su valor es 2.

console.log(persona.piernas); // 2
// ¿Hay una propiedad 'piernas' en persona? Sí, y su valor es 2.
// El prototipo también tiene una propiedad 'piernas', pero no se ha llamado.
// Esto se llama "solapamiento de propiedades"

console.log(persona.cabeza); // 1
// ¿Hay una propiedad 'cabeza' en persona? No, comprobamos su prototipo.
// ¿Hay una propiedad 'cabeza' en persona.[[Animal(Prototype)]]? Sí, y su valor es 1.

console.log(persona.pies); // undefined
// ¿Hay una propiedad 'pies' en persona? No, comprobamos su prototipo.
// ¿Hay una propiedad 'pies' en persona.[[Animal(Prototype)]]? No, comprobamos su prototipo.
// persona.[[Animal]].[[Prototype]] es null, paramos de buscar.
// No se encontró la propiedad, se devuelve undefined
```

_Nota: Recuerda que el simple hecho de dar valor a una propiedad, crea dicha propiedad._

## Herencia de metodos

En JavaScript, cualquier función puede añadirse a un objeto como una propiedad. Una función heredada se comporta como cualquier otra propiedad, viéndose afectada por el solapamiento de propiedades.

Cuando una función heredada se ejecuta, el valor de `this` apunta al objeto que hereda, no al prototipo en el que la función es una propiedad.

Ejemplo

```js
let animal = {
  b: 2,
  m: function(b) {
    return this.a + 1;
  }
};

console.log(o.m()); // 3
// Cuando en este caso se llama a o.m, 'this' se refiere a o

let p = Object.create(o);
// p es un objeto que hereda de o

p.a = 12; // crea una propiedad 'a' en p
console.log(p.m()); // 13
// cuando se llama a p.m, 'this' se refiere a p.
// De esta manera, cuando p hereda la función m de o,
// 'this.a' significa p.a, la propiedad 'a' de p
```

## Como crear objetos y cadenas de prototipos

### Mediante clases

Aunque en JS no existen las clases, siguen siendo cadenas de prototipos, la version actual acerca el lenguaje a una sintaxis mas familiar entre los desarrolladores

```js
class Polygon {
  constructor(height, width) {
    this.height = height;
    this.width = width;
  }
}

class Square extends Polygon {
  constructor(sideLength) {
    super(sideLength, sideLength);
  }
  get area() {
    return this.height * this.width;
  }
  set sideLength(newLength) {
    this.height = newLength;
    this.width = newLength;
  }
}

let square = new Square(2);
```

Hay que tener en cuenta que cuanto mas hacemos descender la herencia, más penalizamos el rendimiento al obligar a JS a revisar toda la cadena de prototipos.

Tambien hay que evitar sobreescribir las cadenas de prototipos de los tipos basicos de JS, esto es una mala práctica.

Ejercicio:

Modela una agenda de contactos con clases.

- Debe permitir añadir contactos
- Debe permitir buscarlos
- Debe permitir borrarlos
- Debe permitir editar un contacto
