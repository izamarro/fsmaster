# Switch

## Seleccionando entre multiples opciones con estamentos switch

Si tienes muchas opciones entre las que elegir, usa un estamento `switch`. Estos estamentos testean un valor y pueden tener multiples `case` contra los que se definen posibles valores. Los estamentos de cada `case` son ejecutados hasta que uno de ellos coincide con el valor o se realiza un brek.

Las comparaciones de cada `case` son evaluadas con el comparador de igualdad estricta `===`. La palabra `break` le indica a JS que debe parar la ejecucion de estamentos, si esta se omite el siguiente será ejecutado a continuación.

```js
switch (letra) {
  case "a":
    console.log("es una 'a'");
    break;
  case "b":
    console.log("es una 'b'");
    break;
}
```

Ejercicio:

Construye una función que incorpore un switch, y que establece la variable respuesta, en cada caso.

```js
/*
1 ---> "alpha";
2 ---> "beta";
3 ---> "gamma";
4 ---> "delta";
*/

function mapearNumeros(value) {
  let respuesta = "";

  return respuesta;
}

[1, 2, 3, 4].forEach(function(value) {
  console.log(mapearNumeros(value));
});
```

## Incluyendo casos por defecto en los estamentos switch

En un switch no siempre podemos especificar todos los posibles valores de los estamentos de `case`. En vez de eso, puedes añadir un estamento por defecto, `default`, que seran ejecutados si no encuentra ningun `case` que corresponda. Puedes pensar en ello como el `else` de un `if ... else`.

Los estamentos `default` deben ser los últimos de un switch:

```js
switch (letra) {
  case "a":
    console.log("es una 'a'");
    break;
  case "b":
    console.log("es una 'b'");
    break;
  default:
    console.log("es cualquier otra letra");
    break;
```

Ejercicio:

onstruye una función que incorpore un switch, y que establece la variable respuesta, en cada caso.

```js
/*
1 ---> "one";
2 ---> "two";
3 ---> "three";
4 ---> "four";
default ----> "any"
*/

function mapearNumeros(value) {
  let respuesta = "";

  return respuesta;
}

[1, 2, 3, 4].forEach(function(value) {
  console.log(mapearNumeros(value));
});
```

## Misma respuesta para diferentes casos

Si tienes multiples valores con la misma respuesta, puedes representarlos juntando casos parecidos, que produciran el mismo resultado.

```js
switch (letra) {
  case "a":
  case "A":
    console.log("es una 'a'");
    break;
  case "b":
  case "B":
    console.log("es una 'b'");
    break;
  default:
    console.log("es cualquier otra letra");
    break;
```

Ejercicio:

Escribe el estamento switch correctamente y establece la respuesta para la siguiente funcion:

```js
/*
1-3 ---> "Small";
4-6 ---> "Medium";
7-9 ---> "Large";
default ----> "any"
*/

function mapearTallas(value) {
  let respuesta = "";

  return respuesta;
}

[1, 2, 3, 4, 5, 6, 7, 8, 9, 12].forEach(function(value) {
  console.log(mapearTallas(value));
});
```

## Reemplazando cadenas de `if...else if` con switch

Los estamentos con switch, son mucho más sencillos de escribir para expresar la seleccion entre múltiples opciones que un encadenado largo de `if...else if`.

El siguiente código:

```js
if (val === 1) {
  answer = "a";
} else if (val === 2) {
  answer = "b";
} else {
  answer = "c";
}
```

puede refactorizarse como:

```js
switch (val) {
  case 1:
    answer = "a";
    break;
  case 2:
    answer = "b";
    break;
  default:
    answer = "c";
}
```

Ejercicio:

Sustituye los estamentos con if/else if por switch:

```js
function chainToSwitch(val) {
  let answer = "";

  if (val === "bob") {
    answer = "Marley";
  } else if (val === 42) {
    answer = "The Answer";
  } else if (val === 1) {
    answer = "There is no #1";
  } else if (val === 99) {
    answer = "Missed me by this much!";
  } else if (val === 7) {
    answer = "Ate Nine";
  }

  return answer;
}

chainToSwitch(7);
```
