# Number

Number es un tipo de datos en JavaScript que representa datos numéricos.
En programación, incluso el simple sistema numérico decimal que todos conocemos tan bien, es más complicado de lo que podrías pensar. Usamos diferentes términos para describir diferentes tipos de números decimales, por ejemplo:

- Enteros son números sin parte decimal, e.g. 10, 400 o -5.
- Números con coma flotante (floats): tienen punto decimal y parte decimal, por ejemplo, 12.5 y 56.7786543.
- Doubles: son un tipo específico de números de coma flotante que tienen una mayor precisión que los numeros de coma flotante comunes (lo que significa que son más precisos en cuanto a la cantidad de decimales que poseen).

Incluso tenemos distintos tipos de sistemas numéricos. El decimal es base 10 (quiere decir que utiliza 0-9 en cada columna), pero también tenemos cosas como:

- Binario — El lenguaje de computadora de nivel más bajo; 0s y 1s.
- Octal — De base 8, utiliza de 0–7 en cada columna.
- Hexadecimal — De base 16, utiliza de 0–9 y luego de a–f en cada columna. (Colores CSS)

## Operadores

![operadores](assets/operators.png)

### Suma

Ahora intentemos agregar dos números usando JavaScript.
JavaScript usa el símbolo `+` como operador de suma cuando se coloca entre dos números.

Ejemplo:

```js
let myVar = 5 + 10; // 15
```

Ejercicio:

Declara dos variables que sean una suma.
Suma el contenido de ambas variables resultantes en una tercera.

### Resta

También podemos restar dos números.
JavaScript usa el símbolo `-` como operador de resta cuando se coloca entre dos números.

Ejemplo:

```js
let myVar = 10 - 5; // 5
```

Ejercicio:

Declara dos variables que sean una suma.
Resta el contenido de ambas variables resultantes en una tercera.

### Multiplicación

También podemos multiplicar dos números.
JavaScript usa el símbolo `*` como operador de multiplicación cuando se coloca entre dos números.

Ejemplo:

```js
let myVar = 10 * 5; // 50
```

Ejercicio:

Declara dos variables que sean una suma y una resta.
Multiplica el contenido de ambas variables resultantes en una tercera.

### División

También podemos dividir dos números.
JavaScript usa el símbolo `/` como operador de división cuando se coloca entre dos números.

Ejemplo:

```js
let myVar = 10 / 5; // 2
```

Ejercicio:

Declara dos variables que sean una suma y una resta.
Multiplica el contenido de ambas variables resultantes en una tercera.
Dividelo entre un número en una cuarta asignación.

### Precedencia de operadores

Dado el siguiente código, ¿cómo se interpreta?

```js
let num2 = 50;
let num1 = 10;

console.log(num2 + num1 / 8 + 2);
```

Parece lógico leerlo como:

"50 más 10 es igual a 60", luego "8 más 2 es igual a 10", y finalmente "60 dividido por 10 es igual a 6".

Pero el navegador hace "10 dividido por 8 es igual a 1.25", luego "50 más 1.25 más 2 es igual a 53.25".

Esto es debido a la precedencia de operadores — algunos operadores son aplicados antes de otros cuando se calcula el resultado de una suma (referida como una expresión, en programación). La precedencia de operadores en JavaScript es la misma que en las matemáticas de la escuela — La multiplicación y la división se resuelven siempre primero, luego la suma y resta (la suma siempre se evalua de izquierda a derecha).

Si quieres alterar la precedencia de operación, puedes colocar paréntesis alrededor de las partes que quieras explícitamente evaluar primero. Para obtener un resultado de 6, podríamos hacer esto:

```js
console.log((num2 + num1) / (8 + 2));
```

### Azucar sintáctico

#### Incremento ++

Puede sincrementar o agregar fácilmente uno a una variable con el operador ++.

```js
i++;
```

es el equivalente de

```js
i = i + 1;
```

Nota:
Toda la línea se convierte en `i++` ;, eliminando la necesidad del signo igual. Ten en cuenta que no puedes realizar esto directamente sobre un número, sino que debemos operar sobre él, por ejemplo asignarlo a una variable.

El operador de incremento incrementa (agrega uno a) su operando y devuelve un valor.

- Si se usa postfijo, con el operador después del operando (por ejemplo, x++), devuelve el valor antes de incrementar.
- Si se usa prefijo, con el operador antes del operando (por ejemplo, ++x), devuelve el valor después de incrementar.

Ejemplo:

```js
// Postfijo
let x = 3;
y = x++; // y = 3, x = 4

// Prefijo
let a = 2;
b = ++a; // a = 3, b = 3
```

#### Decremento --

Puede sincrementar o agregar fácilmente uno a una variable con el operador ++.

```js
i--;
```

es el equivalente de

```js
i = i - 1;
```

Nota:
Toda la línea se convierte en `i--` ;, eliminando la necesidad del signo igual. Ten en cuenta que no puedes realizar esto directamente sobre un número, sino que debemos operar sobre él, por ejemplo asignarlo a una variable.

El operador de decremento decrece (resta uno a) su operando y devuelve un valor.

- Si se usa postfijo, con el operador después del operando (por ejemplo, x--), devuelve el valor antes de decrementar.
- Si se usa prefijo, con el operador antes del operando (por ejemplo, --x), devuelve el valor después de decrementar.

Ejemplo:

```js
// Postfijo
let x = 3;
y = x--; // y = 3, x = 2

// Prefijo
let a = 2;
b = --a; // a = 1, b = 1
```

#### Exponenciación `**`

El operador de exponenciación devuelve el resultado de elevar el primer operando al segundo operando de potencia. es decir, var1var2, en la declaración anterior, donde var1 y var2 son variables. El operador de exponenciación es asociativo a la derecha. `a ** b ** c` es igual a `a ** (b ** c)`.

```js
let i = 2 ** 2;
```

#### Resto `%`

El operador restante `%` da el resto de la división de dos números.

Ejemplo

```text
5% 2 = 1 porque
Math.floor (5/2) = 2 (Cociente)
2 * 2 = 4
5 - 4 = 1 (resto)
```

Uso
En matemáticas, se puede verificar que un número sea par o impar marcando el resto de la división del número por 2.

```text
17% 2 = 1 (17 es impar)
48% 2 = 0 (48 es par)
```

Nota
El operador restante a veces se denomina incorrectamente operador de "módulo". Es muy similar al módulo, pero no funciona correctamente con números negativos.

### Operadores de asignación

Los operadores de asignación son operadores que asignan un valor a una variable. Ya usamos el más básico, =, simplemente asigna a la variable de la izquierda, el valor de la derecha.

Pero hay algunos tipos más complejos, que proporcionan atajos útiles para mantener tu código más ordenado y más eficiente:

![operadores de asignación](assets/assignment_operators.png)

#### Asignación por aumento `+=`

En programación, es común usar asignaciones para modificar el contenido de una variable. Recuerda que primero se evalúa todo a la derecha del signo igual, por lo que podemos decir:

```js
myVar = myVar + 5;
```

Para agregar 5 a myVar. Dado que este es un patrón tan común, hay operadores que realizan tanto una operación matemática como una asignación en un solo paso.

Uno de estos operadores es el operador `+=`.

```js
let myVar = 1;
myVar += 5;
console.log(myVar); // Devuelve 6
```

Ejercicio:

```js
var a = 3;
var b = 17;
var c = 12;

a = a + 12;
b = 9 + b;
c = c + 7;
```

Modifica el codigo para usar la asignación por aumento, corrige el uso de `var` por lo que consideres que es más adecuado.

#### Asignación por disminución `-=`

Al igual que el operador `+=`, `-=` resta un número de una variable.

```js
myVar = myVar - 5;
```

restará 5 de myVar. Esto puede reescribirse como:

```js
myVar - = 5;
```

Ejercicio:

```js
var a = 11;
var b = 9;
var c = 3;

a = a - 6;
b = b - 15;
c = c - 1;
```

Modifica el codigo para usar la asignación por disminución, corrige el uso de `var` por lo que consideres que es más adecuado.

#### Asignación por multiplicación `*=`

El operador `*=` multiplica una variable por un número.

```js
myVar = myVar * 5;
```

multiplicará myVar por 5. Esto puede reescribirse como:

```js
myVar *= 5;
```

Ejercicio:

```js
var a = 5;
var b = 12;
var c = 4.6;

a = a * 5;
b = 3 * b;
c = c * 10;
```

Modifica el codigo para usar la asignación por multiplicación, corrige el uso de `var` por lo que consideres que es más adecuado.

#### Asignación por división `/=`

El operador `/=` divide una variable por un número.

```js
myVar = myVar / 5;
```

multiplicará myVar por 5. Esto puede reescribirse como:

```js
myVar /= 5;
```

Ejercicio:

```js
var a = 48;
var b = 108;
var c = 33;

a = a / 12;
b = b / 4;
c = c / 11;
```

Modifica el codigo para usar la asignación por división, corrige el uso de `var` por lo que consideres que es más adecuado.

Realiza el ejercicio: [Canvas](exercises/canvas/index.md)
