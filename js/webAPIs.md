# Web APIs

Las Interfaces de Programacion de Aplicaciones (APIs por sus siglas en inglés) son nos permiten a los desarrolladores crear funcionalidades complejas de una manera más simple.

Abstraen funcionalidades complejas para proveer una sintaxis más fácil de usar en su lugar.

A modo de ejemplo podemos pensar en nuestro router de casa. Tú te conectas directamente a tu proveedor de servicios de internet a través de él, aislandote de la complejidad técnica de intentar tirar un cable hasta sus edificios...

De igual modo si quieres programar una web con gráficos en JS, utilizas APIs de gráficos que te permiten renderizar los datos de manera gráfica, en lugar de programarte el renderizado de todos ellos, puedes centrarte en usarlo sencillamente en tu programa.

## APIs de cliente

avaScript del lado cliente, particularmente, tiene muchas APIs disponibles — estas no son parte del lenguaje en sí, sino que están construidas sobre el núcleo de este lenguaje de programación, proporcionándote superpoderes adicionales para usar en tu código. Por lo general, se dividen en dos categorías:

- Las APIs de navegador están integradas en el navegador exponen datos del navegador y del entorno de tu programa, etc. Por ejemplo, la API de Geolocalización proporciona permite obtener datos complejos, de manera simple. Es el navegador el que haceuso de códigos de bajo nivel, para comunicarse con el hardware GPS del dispositivo, recuperar datos de posición y devolverlos al entorno del navegador para su uso en tu código. Pero una vez más, la API se encarga de abstraer esta complejidad.

- Las APIs de terceros no están incluídas por defecto en el navegador, y por lo general es necesario obtener el código e información desde algún lugar de la Web.

### Apis de navegador

En particular, las categorías más comunes de APIs de navegador más usadas (y que trataremos con mayor detalle en este módulo) son:

- APIs para manipular documentos cargados en el navegador. El ejemplo más obvio es la API DOM (Document Object Model), que permite manipular HTML y CSS

- APIs que obtienen datos del servidor, comunmente usadas para actualizar pequeñas secciones de una página web.

Las APIs de dispositivos son básicamente APIs para manipular y recuperar información de dispositivos modernos de hardware de forma que sean útiles para aplicaciones web. Ya hemos hablado de la API de geolocalización, que accede a la información de ubicación del dispositivo, de forma que te pueda localizar en un mapa. Otros ejemplos incluyen indicar al usuario de que una actulización útil está disponible en una aplicación web mediante notificaciones de sistema (ver Notifications API) o la vibración de hardware (ver Vibration API).
Las APIS de almacenamiento en el lado del cliente se están popularizando en los navegadores. La habilidad de almacenar información en el lado del cliente es muy útil para hacer aplicaciones que salven su estado entre carga de páginas, e incluso trabajar cuando el dispositivo está fuera de línea. Hay varias opciones disponibles, por ejemplo el almacenamiento en pares de clave/valor con Web Storage API, y una forma más compleja de almacenar datos tabulados mediante la IndexedDB API.

### Como utilizo una API

Cada API tiene una forma diferente de uso, pero casi todas tienen patrones comunes en como podemos usarlas.

- Exponen objetos
- Tienen puntos de acceso documentados o intuibles
- Usan eventos para manejar cambios en su estado

Usemos por ejemplo la API conocida de mapas:

### Ejemplo práctico:

#### Preparando el código

**Crea la tríada JS, CSS, HTML**

En nuestro HTML incluimos:

```js
<link
  rel="stylesheet"
  href="https://unpkg.com/leaflet@1.6.0/dist/leaflet.css"
  integrity="sha512-xwE/Az9zrjBIphAcBb3F6JVqxf46+CDLwfLMHloNu6KEQCAWi6HcDUbeOfBIptF7tcCzusKFjFw2yuvEpDL9wQ=="
  crossorigin=""
/>
<!-- El Script tiene que venir después del CSS -->
<script src="https://unpkg.com/leaflet@1.6.0/dist/leaflet.js"
   integrity="sha512-gZwIG9x3wUXg2hdXF6+rVkLF/0Vi9U8D2Ntg4Ga5I5BZpVkVxlJWbSQtXPSiUTtC0TjtGOmxa1AJPuV0CPthew=="
   crossorigin="">
</script>
```

En nuestro `<body>` incluimos un nuevo id, donde se renderizara nuestro mapa:

```js
<body>
  <div id="mapa"></div>
</body>
```

Indicaremos la ruta a nuestro propio CSS en el HTML, al menos hay que darle un tamaño al mapa!

```css
#mapa {
  height: 100%;
}
```

Ya estamos listo para crear nuestro mapa, mediante el uso de la API de JS que expone la librería de mapas que hemos incluido.

### Creando el mapa

Vamos a crear un mapa en torno a la ubicación de neoland, con zoom.

Creamos nuestra funcion de renderizado

```js
function render() {
  const zoom = 18;
  const neolandGeo = [40.423434, -3.713119];
  const map = L.map("mapa").setView(neolandGeo, zoom);
  const layerURL =
    "https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw";
  const layerOptions = {
    maxZoom: zoom,
    attribution:
      'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, ' +
      '<a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
      'Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
    id: "mapbox/streets-v11"
  };

  L.tileLayer(layerURL, layerOptions).addTo(map);
  L.marker(neolandGeo).addTo(map);
}
```

```js
const map = L.map("mapa").setView(geo, zoom);
```

Esta línea no es parte de la API del navegador sino de la librería JS que hemos cargado previamente, y nos permite renderizar nuestro mapa y mover la vista del mismo a una posición, con un determinado zoom.

**Nota: Muchos métodos de muchas librerías retornan el mismo objeto para permitir encadenar métodos, estos objetos suelen ser internos a la librería y conviene inspeccionarlos en su documentación**

En la línea:

```js
L.tileLayer(layerURL, layerOptions).addTo(map);
```

Volvemos a hacer uso de la API para dibujar las imagenes del mapa, que obtenemos de una URL concreta, con unas opciones determinadas como el zoom máximo.

**Nota: gracias al uso de defer en tu script js, lograrás que el dom y la librería esté disponible durante la ejecución de tu código ;D**

Añadimos un marcador a la posición de Neoland

```js
L.marker(neolandGeo).addTo(map);
```

Podemos detectar eventos en el mapa, gracias a la librería y añadir nuestro propio popUp cada vez que se haga click en un punto del mapa.

Incluyamos en nuestro render un manejador del evento:

```js
const onMapClick = event =>
  L.popUp()
    .setLatLng(event.latlng)
    .setContent("Has clickado en las coordenadas " + event.latlng.toString())
    .openOn(mymap);

L.on("click", onMapClick);
```

_Ejercicio:_

**Sustituye el código de la función `onMapClick` por uno que añada marcadores en la posición clickada**
**Investiga la API de Leaflet para ver como borrar el marcador al hacer click sobre él**
**Investiga la API de Leaflet, intenta añadir una linea que vaya de marcador en marcador**
