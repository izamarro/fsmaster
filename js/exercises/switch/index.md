## Contando cartas

En el juego del Blackjack, un jugador puede obtener una ventaja sobre la casa haciendo un seguimiento del número relativo de cartas altas y bajas que quedan en el mazo.

Tener más cartas altas restantes en el mazo favorece al jugador. A cada tarjeta se le asigna un valor de acuerdo con la siguiente tabla. Cuando el conteo es positivo, el jugador debe apostar alto. Cuando el conteo es cero o negativo, el jugador debe apostar menos.

![](./cards.png)

Escribe una función de conteo de cartas. Recibirá un parámetro llamado `card`, que puede ser un `number` o un `string`, e incrementará o disminuirá la variable global `count` de acuerdo con el valor de la tarjeta (ver tabla). La función devolverá un `string` si `count` es positivo: "Apuesta", o "Aguanta" si `count` es cero o negativo, seguido de un espacio y la variable `count`.

Ejemplos de salida:

- `Aguanta -3`
- `Apuesta 4`
