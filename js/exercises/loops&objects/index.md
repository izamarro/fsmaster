# Agenda de contactos

Vamos a implementar una agenda de contactos, que permita introducir, borrar y actualizar un contacto, ademas de buscarlos.

Un contacto debe tener la forma:

```text
{
    id: Number,
    name: String,
    surname: String,
    email: String,
    phones: Array [Objects],
}

// phone

{
    type: String,
    value: String
}
```

Debes:

- Poder introducir nuevos telefonos.
  - Si no hay coincidencias, retornara un string: "No existe el contacto"
- Poder buscar por nombre, todos los que empiezan por un determinado nombre.
  - Si no hay coincidencias, retornara un string: "No existe el contacto"
- Poder buscar todos los que empiezan por un determinado telefono
- Borrar cualquier contacto
- Se debe poder exportar los contactos, como una lista que contiene el string: "nombre apellidos: email, telefono, ... telefono"
