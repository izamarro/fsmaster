# Contando opciones

El siguiente HTML presenta una serie de opciones de un selector.

Completa la función `howMany` de tal manera que retorne, cuantos elementos del `select` multiple han sido seleccionados.

La función recibe directamente el `select`, y una opción sabemos que ha sido seleccionada si tiene su propiedad selected a `true`.

Trucos:

- Inspecciona el objeto que entra en la función para saber dónde ( en qué propiedad )se encuentran las opciones del selector.
