function howMany(selectObject) {
  let count = 0;

  for (let i = 0; i < selectObject.options.length; i++) {
    const option = selectObject.options[i];

    if (option.selected) {
      count++;
    }
  }

  return count;
}

document.getElementById("btn").onclick = function() {
  alert(
    "Número de opciones seleccionadas: " +
      howMany(document.selectForm.musicTypes)
  );
};
