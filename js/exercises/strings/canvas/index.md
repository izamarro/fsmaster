En el cuadro de código editable anterior, hay dos líneas marcadas claramente con un comentario que debes actualizar para hacer que el cuadro crezca o reduzca a ciertos tamaños, utilizando ciertos operadores y/o valores en cada caso. Intenta lo siguiente:

- Cambia la línea que calcula x, para que el recuadro tenga un ancho de 50px, y que el 50 se calcule utilizando los números 43 y 7, y un operador aritmético.

- Cambia la línea que calcula y, para que la casilla tenga 75px de altura y que el 75 se calcule utilizando los números 25 y 3, y un operador aritmético.

- Cambia la línea que calcula x, para que el recuadro tenga un ancho de 250px, y que el 250 se calcule utilizando dos números y el operador del resto.

- Cambia la línea que calcula y, para que el cuadro tenga 150px de altura, y que el 150 se calcule utilizando tres números, y los operadores de resta y división.

- Cambia la línea que calcula x, para que el cuadro tenga 200px de ancho y que el 200 se calcule utilizando el número 4 y un operador de asignación.

- Cambia la línea que calcula y, para que el cuadro tenga 200px de altura y que el 200 se calcule utilizando los números 50 y 3, el operador de multiplicación y el operador de asignación de suma.

- Crea un desafio matematico para tu compañero.
