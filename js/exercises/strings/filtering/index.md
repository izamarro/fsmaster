# Filtrando

Tenemos una coleccion de mensajes de tarjetas de felicitación, pero queremos ordenarlos para que aparezcan solo los mensajes de Navidad. Completa un test condicional dentro de la estructura if( ... ), para comprobar cada cadena y solo imprimirla en la lista si es un mensaje de Navidad.

- Primero piensa en cómo puedes probar si el mensaje en cada caso es un mensaje de Navidad. ¿Qué cadena está presente en todos esos mensajes, y qué método podrías usar para comprobar si está presente?

- A continuación, deberá escribir un test condicional de la forma: ¿Es lo que está a la izquierda igual a lo que está a la derecha? O en este caso, ¿el método llamado a la izquierda devuelve el resultado a la derecha?

- Sugerencia: En este caso, probablemente sea más útil comprobar si la llamada al método no es igual a un determinado resultado.
