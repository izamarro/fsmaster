# Extrayendo cadenas

El array contiene un conjunto de cadenas que contienen información sobre estaciones de tren en el Norte de Inglaterra. Las cadenas son elementos de datos que contienen el código de estación de tres letras, seguidos de más datos legibles por máquina, seguidos de un punto y coma, seguidos por el nombre de la estación legible por humanos. Por ejemplo:

```str
MAN675847583748sjt567654;Manchester Piccadilly
```

Queremos extraer el código y el nombre de la estación, y juntarlos en una cadena con la siguiente estructura:

```str
MAN: Manchester Piccadilly
```

1. Extrae las tres letras del código de estación y almacénalo en una nueva variable.
2. Encuentra el número de índice de caracter del punto y coma.
3. Extrae el nombre de la estación legible por humanos utilizando el número de índice de caracter del punto y coma a modo de referencia y guardalo en una nueva variable.
4. Concatenar las dos nuevas variables y un literal de cadena para hacer la cadena final.
5. Cambia el valor de la variable de result a igual a la cadena final, no a input.
