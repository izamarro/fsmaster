const DOM_MAP_ID = "map";

class Geo {
  constructor(latitude, longitude) {
    this.latitude = latitude;
    this.longitude = longitude;
  }

  static fromMarker = m => {
    const latLang = m.getLatLng();
    const geo = new Geo(latLang.lat, latLang.lng);

    return geo;
  };

  toArray = () => {
    return [this.latitude, this.longitude];
  };
}

class MapApp {
  constructor() {
    this.map = null;
  }

  renderMap = (geo, zoom) => {
    this.map = L.map(DOM_MAP_ID).setView(geo.toArray(), zoom);
  };

  renderTiles = zoom => {
    const layerURL =
      "https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw";
    const layerOptions = {
      maxZoom: zoom,
      attribution:
        'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, ' +
        '<a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
        'Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
      id: "mapbox/streets-v11"
    };
    L.tileLayer(layerURL, layerOptions).addTo(this.map);
  };

  render = (geo, zoom) => {
    this.renderMap(geo, zoom);
    this.renderTiles(zoom);
  };
}

// const app = new MapApp();

//app.render(new Geo(40.423434, -3.713119), 18);

function render() {
  let polyline = null;
  const zoom = 18;
  const neolandGeo = new Geo(40.423434, -3.713119);
  const map = L.map("map").setView(neolandGeo.toArray(), zoom);
  const layerURL =
    "https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw";
  const layerOptions = {
    maxZoom: zoom,
    attribution:
      'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, ' +
      '<a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
      'Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
    id: "mapbox/streets-v11"
  };
  const neolandMarker = L.marker(neolandGeo.toArray()).addTo(map);
  const markers = [neolandMarker.addTo(map)];
  const mapMarkerToGeo = m => {
    const latLang = m.getLatLng();
    const geo = new Geo(latLang.lat, latLang.lng);

    return geo;
  };

  function onMarkerClick(marker) {
    markers.filter(filterByGeo);
    marker.remove();
  }

  function onMapClick(e) {
    const markerGeo = new Geo(e.latlng.lat, e.latlng.lng);
    const marker = L.marker(markerGeo.toArray())
      .addTo(map)
      .on("click", () => onMarkerClick(marker));

    markers.push(marker);

    polyline = L.polyline(
      markers.map(mapMarkerToGeo).map(g => g.toArray()),
      { color: "red" }
    ).addTo(map);

    map.fitBounds(polyline.getBounds());
  }

  L.tileLayer(layerURL, layerOptions).addTo(map);

  map.on("click", onMapClick);
}

render();
