# Ejercicio de funciones

En programacion una cola, es una abstracción que mantiene los items en un orden concreto. Los items pueden ser añadido al final de la cola, y pueden ser extraidos del inicio de la misma.

Escribe una funcion llamada: `siguiente` que tome un array y un numero como argumentos.

Añade el numero al final del array, luego elimina el primer elemento del array.

La funcion debe devolver el elemento eliminado.
