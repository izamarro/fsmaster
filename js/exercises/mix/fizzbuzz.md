# FizzBuzz

Escriba un programa que imprima los números del 1 al 100. Para los múltiplos de 3 imprime "Fizz" en lugar del número y para los múltiplos de cinco imprime "Buzz". Para números que son múltiplos de tres y cinco, imprime "FizzBuzz".

```js
fizzBuzz();

// 1
// 2
// Fizz
// 4
// Buzz
// ...
// 14
// FizzBuzz
```
