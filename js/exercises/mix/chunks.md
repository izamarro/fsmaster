# Generador de chunks

Escriba una función que divida un array (primer argumento) en grupos de la longitud del tamaño (segundo argumento) y los devuelva como una array bidimensional.

Ejemplo:

```js
chunkArray(["a", "b", "c", "d"], 2); // [["a", "b"], ["c", "d"]]
```

Testea tu función con los siguientes valores:

- `chunkArray(["a", "b", "c", "d"], 2) debe retornar [["a", "b"], ["c", "d"]].`
- `chunkArray([0, 1, 2, 3, 4, 5], 3) debe retornar [[0, 1, 2], [3, 4, 5]].`
- `chunkArray([0, 1, 2, 3, 4, 5], 2) debe retornar [[0, 1], [2, 3], [4, 5]].`
- `chunkArray([0, 1, 2, 3, 4, 5], 4) debe retornar [[0, 1, 2, 3], [4, 5]].`
- `chunkArray([0, 1, 2, 3, 4, 5, 6], 3) debe retornar [[0, 1, 2], [3, 4, 5], [6]].`
- `chunkArray([0, 1, 2, 3, 4, 5, 6, 7, 8], 4) debe retornar [[0, 1, 2, 3], [4, 5, 6, 7], [8]].`
- `chunkArray([0, 1, 2, 3, 4, 5, 6, 7, 8], 2) debe retornar [[0, 1], [2, 3], [4, 5], [6, 7], [8]].`
