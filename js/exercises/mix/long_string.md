# El string mas largo de una frase

Completa la función de manera que: retorne un objeto que tenga las propiedades: `len` y `value`, representando la longitud y el valor del string más largo dentro de una frase.

```js
function longestStr(str) {
  return str.length;
}

longestStr(
  "El zorro marrón, más rápido que el perro, saltó sobre el mismo, no esperaba la llegada del murcielago"
);
```

Ejemplo:

```
longestStr("Hola mundo"); // logs { len: 5, value: "mundo" };
```
