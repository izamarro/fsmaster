function reverseStr(str) {
  return str
    .split("")
    .reverse()
    .join("");
}

function isPalindrome(str) {
  return reverseStr(str).toLowerCase() === str.toLowerCase();
}

console.log(reverseStr("Hola Mundo!"));
console.log(isPalindrome("Hola Mundo!"));
console.log(isPalindrome("Abalaba"));
console.log(isPalindrome("oso"));
