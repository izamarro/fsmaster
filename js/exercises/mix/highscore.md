Gestiona la lista de mejores puntuaciones de un jugador.

Tu tarea es construir un componente de puntuaciones altas, del clásico juego Frogger, uno de los juegos más vendidos y adictivos de todos los tiempos, y un clásico de la era de los juegos arcade. Tu tarea es escribir métodos que devuelvan la puntuación más alta de la lista, la última puntuación agregada y las tres mas altas.
