function caniStr(str) {
  const splitted = str.toLowerCase().split("");

  for (let i = 0; i < splitted.length; i++) {
    const mustBeInUpperCase = Math.random() > 0.5;
    const mustHaveAnH = Math.random() > 0.6;
    const mustHaveAnHBefore = Math.random() > 0.5;

    if (mustBeInUpperCase) {
      splitted[i] = splitted[i].toUpperCase();
    }

    switch (splitted[i]) {
      case "a":
      case "e":
      case "i":
      case "o":
      case "u":
        if (mustHaveAnH) {
          if (mustHaveAnHBefore) {
            splitted[i] = "h" + splitted[i];
          } else {
            splitted[i] = splitted[i] + "h";
          }

          break;
        }
      default:
        break;
    }
  }

  return splitted.join("");
}

console.log(caniStr("Soy un personaje"));
