## Puntuación de Scrabble

Dada una palabra, calcula la puntuación de scrabble para esa palabra.

Valor de la letra
A, E, I, O, U, L, N, R, S, T => 1
D, G => 2
B, C, M, P => 3
F, H, V, W, Y => 4
K => 5
J, X => 8
Q, Z => 10

Ejemplos

"Cabbage" ("repollo") debe puntuarse con un valor de 14 puntos:

3 puntos para C
1 punto para A, dos veces
3 puntos por B, dos veces
2 puntos para G
1 punto para E

Extiendelo:

- Puedes jugar una letra doble o triple.
- Puedes jugar una palabra doble o triple.
