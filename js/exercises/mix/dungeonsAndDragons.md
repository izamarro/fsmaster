Para un juego de Dungeons & Dragons, cada jugador comienza generando un personaje con el que puede jugar. Este personaje tiene, entre otras cosas, seis habilidades; fuerza, destreza, constitución, inteligencia, sabiduría y carisma. Estas seis habilidades tienen puntos que se determinan al azar. Para ello, tira cuatro dados de 6 lados y registra la suma de los tres dados más grandes. Lo haces seis veces, una por cada habilidad.

Los puntos de vida iniciales de tu personaje son 10 + el modificador de constitución de tu personaje. Encuentra el modificador de constitución de tu personaje restando 10 de la constitución de tu personaje, divide entre 2 y redondea hacia abajo.

Escriba un generador de caracteres aleatorios que siga las reglas anteriores.

Por ejemplo, los seis lanzamientos de cuatro dados pueden verse así:

    5, 3, 1, 6: descarta el 1 y suma 5 + 3 + 6 = 14, que asigna a la fuerza.
    3, 2, 5, 3: descarta el 2 y suma 3 + 5 + 3 = 11, que asigna a la destreza.
    1, 1, 1, 1: descarta el 1 y suma 1 + 1 + 1 = 3, que asigna a la constitución.
    2, 1, 6, 6: descarta el 1 y suma 2 + 6 + 6 = 14, que asigna a la inteligencia.
    3, 5, 3, 4: descarta el 3 y suma 5 + 3 + 4 = 12, que asigna a la sabiduría.
    6, 6, 6, 6: descarta el 6 y suma 6 + 6 + 6 = 18, que asigna al carisma.

Como la constitución es 3, el modificador de constitución es -4 y los puntos de vida son 6.
