# Termómetro

Sabiendo que la fórmula para convertir los grados celsius a fahrenheit es:

`F = ((9/5) * C) + 32`

Implementa un convertidor doble, de Celsius a Fahrenheit y de Fahrenheit a Celsius:

```js
function convertToF(celsius) {
  let fahrenheit;
  return fahrenheit;
}

function convertToC(fahrenheit) {
  let celsius;
  return celsius;
}

convertToF(30);
convertToC(54);
```
