## Poligonizador de strings

En los 90's en España, durante el uso del desaparecido Messenger la gente solía usar nicks que mezclaban letras mayúsculas y minúsculas en una suerte de aleatoriedad.

Implementa una función, que dado un string, sea capaz de devolver dicha construcción, ejemplo:

```js
caniStr("soy un moreno guapo"); // sHoy uN mOrEnoH GUaPO
```

- Intenta que introduzca de manera aleatoria alguna `h` antes o despues del uso de una vocal
- Recuerda o busca el código para generar un booleano aleatorio.
