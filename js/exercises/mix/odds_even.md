# Pares e impares

Implementa el juego de pares y nones, dos jugadores deben dar su numero y elegir entre pares y nones. La suma del numero elegido por ambos jugadores determina quien ha ganado.

Debes pedir tanto el numero como el string que corresponde de la apuesta a cada jugador, y validar que solo pueden elegir entre: "pares", "nones".

- No es necesesario HTML
- Se puede solicitar información al usuario con el método `prompt`
