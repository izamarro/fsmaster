function endsWith(str, end) {
  return str.includes(end, str.length - end.length);
}

function endsWith2(str, end) {
  return str.substring(str.length - end.length, str.length) === end;
}

console.log(endsWith("vaca", "aca"));
console.log(endsWith2("vaca", "co"));
