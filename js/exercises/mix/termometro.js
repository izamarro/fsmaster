function convertToF(celsius) {
  return (9 / 5) * celsius + 32;
}

function convertToC(fahrenheit) {
  return ((fahrenheit - 32) * 5) / 9;
}
