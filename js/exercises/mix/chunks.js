function chunkArray(chunk, number) {
  let chunkLength = chunk.length;
  let totalResult = [];

  if (number < chunk.length || number >= 0) {
    const loopNumber = Math.ceil(chunkLength / number);

    for (let i = 0; i < loopNumber; i++) {
      let result = chunk.slice(0, number);
      chunk = chunk.splice(number, chunk.length);

      totalResult.push(result);
    }

    return totalResult;
  }
}
