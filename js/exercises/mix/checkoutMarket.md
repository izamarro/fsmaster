## Checkout de supermercado

Aplicando clases JS, implementemos el código para el pago de un supermercado, que calcula el precio total de varios artículos. En un supermercado normal, las cosas se identifican mediante Unidades de stock de existencias o SKU. En nuestra tienda, utilizaremos letras individuales del alfabeto (A, B, C, etc.). Nuestros productos tienen un precio individual.

Además, algunos artículos tienen reglas de compra: compre n de ellos y le costarán menos.

Por ejemplo, el artículo "A" podría costar 50 cents individualmente, pero esta semana tenemos una oferta especial: compre tres "A" y le costarán \€ 1.30. De hecho, los precios de esta semana son:

```text
Item   Unit Price  Special Price
--------------------------
  A     50       3 for 130
  B     30       2 for 45
  C     20
  D     15
```

Nuestro pago acepta artículos en cualquier orden, de modo que si escaneamos una B, una A y otra B, reconoceremos las dos B y las valoraremos a 45 (por un precio total hasta ahora de 95). Debido a que los precios cambian con frecuencia, necesitamos poder pasar un conjunto de reglas de precios cada vez que comenzamos a manejar una transacción de pago.

La API para el pago debería verse así:

```js
const co = new CheckOut(priceRules);

co.scan(item);
co.scan(item);

const price = co.total;
```
