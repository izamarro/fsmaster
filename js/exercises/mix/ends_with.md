## Termina con...

Implementa una función, que dada una palabra y una cadena, retorna un booleano que confirma si termina o no en dicha cadena. Ejemplo

```js
endsWith("clase", "a"); // false
endsWith("perro", "ro"); // true
```

**No puedes hacer uso del metodo endsWith() de un string.**
