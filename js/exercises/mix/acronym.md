# Generador de acrónimos

Haz una funcion que con convierta nombres largos en sus acrónimos, por ejemplo:

```js
toAcronym("Sistema Nacional de Salud"); // SNS

function toAcronym(str) {
  const splitted = str.split(" ");
  const initials = [];

  for (let i = 0; i < splitted.length; i++) {
    const word = splitted[i];
    const initial = word[0];

    if (initial === initial.toUpperCase()) {
      initials.push(initial);
    }
  }

  return initials.join("");
}
```

Acronimos de ejemplo:

- Objeto Volador No Identificado: OVNI
- Sindrome de la Inmuno Deficencia Adquirida: SIDA
- Alta Velocidad Española: AVE
- Fédération Internationale de Football Association: FIFA

- Recuerda que las preposiciones no forman parte de los acronimos
