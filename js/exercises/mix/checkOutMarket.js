class CheckOut {
  constructor(rules) {
    this.items = {};
  }
  scan(item) {
    const actualValue = this.items[item.sku];
    if (actualValue != null) {
      actualValue = [item];
      return this.items;
    }

    actualValue.push(item);
    return this.items;
  }

  get total() {
    const reduceItemsPrice = (acc, next) => [...acc, ...next.price];
    const reduceSumPrice = (acc, next) => acc + next;

    return Object.values(this.items)
      .reduce(reduceItemsPrice, [])
      .reduce(reduceSumPrice, 0);
  }
}

const co = new CheckOut(priceRules);

co.scan(item);
co.scan(item);

const price = co.total;
