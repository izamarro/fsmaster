function longestStr(str) {
  let palabras = str.split(" ");
  let palabraMasLarga = "";

  for (let i = 0; i < palabras.length; i++) {
    if (palabras[i].length > palabraMasLarga.length) {
      palabraMasLarga = palabras[i];
    }
  }

  return {
    len: palabraMasLarga.length,
    value: palabraMasLarga
  };
}

console.log(longestStr("Hola mundo"));
