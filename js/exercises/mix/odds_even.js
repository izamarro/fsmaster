const even = "pares";
const odd = "nones";

function getPlayerSelection(playerNum) {
  let playerNumber = parseInt(prompt(`JUGADOR ${playerNum}: Elige un número`));

  if (isNaN(playerNumber)) {
    alert(`JUGADOR ${playerNum}: Debes introducir un número entero`);
    return getPlayerSelection();
  }

  return playerNumber;
}

function getPlayerBet(playerNum) {
  let paresOnones = prompt(
    `JUGADOR ${playerNum}: Elige PARES o NONES`
  ).toLowerCase();

  if (paresOnones !== even && paresOnones !== odd) {
    alert(`JUGADOR ${playerNum}: Debes introducir PARES o NONES`);
    return getPlayerBet();
  }

  return paresOnones;
}

function play() {
  let suma = getPlayerSelection(1) + getPlayerSelection(2);
  let player1Bet = getPlayerBet(1);
  let player2Bet = getPlayerBet(2);
  let isEven = suma % 2 === 0;

  if (isEven && player1Bet === even && player2Bet === odd) {
    alert("JUGADOR 1 HAS GANADO");
  } else if (isEven && player1Bet === odd && player2Bet === even) {
    alert("JUGADOR 2 HAS GANADO");
  } else if (!isEven && player1Bet === odd && player2Bet === even) {
    alert("JUGADOR 1 HAS GANADO");
  } else if (!isEven && player1Bet === even && player2Bet === odd) {
    alert("JUGADOR 2 HAS GANADO");
  } else {
    alert("HABÉIS EMPATADO!!!");
  }
}

play();
