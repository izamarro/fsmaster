function toAcronym(str) {
  const initials = [];
  const splitted = str.split(" ");

  for (let i = 0; i < splitted.length; i++) {
    const word = splitted[i];
    const initial = word[0];
    const isInUpperCase = initial === initial.toUpperCase();

    if (isInUpperCase) {
      initials.push(initial);
    }
  }

  return initials.join("");
}
