# Jugadas de golf;

En el juego de golf, cada hoyo tiene un `par` que significa el número promedio de `golpes` que se espera que haga un golfista para hundir la pelota en un hoyo para completar el juego. Dependiendo del numero de `golpes`, hay un apodo diferente para la jugada.

Su función recibe `par` y `golpes` como argumentos, que siempre serán numericos y positivos. Devuelve la cadena correcta de acuerdo con esta tabla que enumera la logica en orden de prioridad;

![](golflogic.png)
