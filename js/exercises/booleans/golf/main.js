const names = [
  "Hole-in-one!",
  "Eagle",
  "Birdie",
  "Par",
  "Bogey",
  "Double Bogey",
  "Go Home!"
];

function golfScore(par, strokes) {}

// tests
console.assert(
  golfScore(4, 1) === "Hole-in-one!",
  'El valor es diferente de: "Hole-in-one!"'
);
console.assert(
  golfScore(4, 2) === "Eagle",
  'El valor es diferente de: "Eagle"'
);
console.assert(
  golfScore(5, 2) === "Eagle",
  'El valor es diferente de: "Eagle"'
);
console.assert(
  golfScore(4, 3) === "Birdie",
  'El valor es diferente de: "Birdie"'
);
console.assert(golfScore(4, 4) === "Par", 'El valor es diferente de: "Par"');
console.assert(
  golfScore(1, 1) === "Hole-in-one!",
  'El valor es diferente de: "Hole-in-one!"'
);
console.assert(golfScore(5, 5) === "Par", 'El valor es diferente de: "Par"');
console.assert(
  golfScore(4, 5) === "Bogey",
  'El valor es diferente de: "Bogey"'
);
console.assert(
  golfScore(4, 6) === "Double Bogey",
  'El valor es diferente de: "Double Bogey"'
);
console.assert(
  golfScore(4, 7) === "Go Home!",
  'El valor es diferente de: "Go Home!"'
);
console.assert(
  golfScore(5, 9) === "Go Home!",
  'El valor es diferente de: "Go Home!"'
);
