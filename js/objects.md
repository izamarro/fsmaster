# Objetos

Los objetos son similares a los Arrays, excepto que en lugar de usar índices para acceder y modificar sus datos, accedemos a los datos en los objetos a través de lo que se llaman propiedades.

## Construcción

Los objetos son útiles para almacenar datos de forma estructurada y pueden representar objetos del mundo real.

```js
const gato = {
  nombre: "Snow",
  patas: 4,
  enemigos: ["agua", "perros"],
  maullar: function() {
    alert("miau");
  }
};
```

Ejercicio:

Haz un objeto que represente a un perro, debe incluir almenos una función que le permita ladrar por consola.

## Acceso de las propiedades

Existen dos formas de acceder a las propiedades de un objeto, mediante el uso de `[]` o el uso de un `.`.

Usamos la notación por `.` cuando conocemos exactamente el nombre de la propiedad a la que estamos accediendo.

```js
const miObjeto = { propiedad1: "propiedad1", propiedad2: "propiedad2" };

const prop1Val = miObjeto.propiedad1;
const prop2Val = miObjeto.propiedad2;
```

Usamos la notación por `[]` cuando el nombre de la propiedad contiene espacios en blanco. Aunque es perfectamente valida de usar si no los contiene.

```js
const miObjeto = {
  "mi propiedad1": "propiedad1",
  "mi propiedad2": "propiedad2"
};

const prop1Val = miObjeto["mi propiedad1"];
const prop2Val = miObjeto["mi propiedad2"];
```

El nombre de la propiedad debe ir como un string al acceder a ella en esta notación.

Ejercicio:

Crea un objeto que describa la vestimenta de cabeza, cuerpo y piernas de una persona, con valores que elijas. Despues accede a dichas propiedades y asigna su valor a variables.

Otra utilidad del acceso por notación `[]` es cuando queremos acceder al valor de una propiedad como si fuera una tabla de correspondencias:

```js
const movimientos = {
  up: "saltar",
  left: "retroceder",
  right: "avanzar",
  down: "agacharse"
};
/*...*/

let tecla = "down";
const accion = movimientos[tecla];
```

## Actualización de objetos

Tras haber creado un objeto JavaScript, puedes actualizar sus propiedades en cualquier momento al igual que actualizarías cualquier otra variable. Puedes usar la notación de punto o corchete para actualizar.

Ejemplo:

```js
const datosDeContacto = {
  direccion: "C/ Azurita, 7",
  telefono: "666876321"
};

// podemos actualizar el telefono con notacion por punto:
datosDeContacto.telefono = "666123123";

// podemos actualizar la direccion con notacion por corchete:
datosDeContacto["direccion"] = "Plza. España, 8B 3ºA";
```

Ejercicio:

1. Crea una funcion que devuelva un objeto que vamos a llamar `User`, cuyos campos son:

- username
- password (puntos extras a quien cifra cob base64 la contraseña al crear el objeto)
- email

2. Crea una función por cada campo, que recibe el objeto y el valor a actualizar y devuelve el objeto `User` con el campo modificado a su nuevo valor.

## Añadir nuevas propiedades a un objeto

Podemos añadir nuevos campos en todo momento a un objeto, con la notacion por punto o por corchete, aunque aumentar el numero de campos de nuestros objetos en tiempo de ejecución, no siempre es una buena idea..

```js
const dog = { name: "FooDog" };

dog.legs = 4;
dog["tail"] = 1;
```

Ejercicio:

Amplia el código anterior, para añadir una función que crea datos de contacto en un objeto `User`. Los datos de contacto deben ser otro objeto, con propiedades:

- phone
- name
- surname
- address

## Borrado de propiedades

Podemos borrar propiedades de un objeto JS, con la palabra reservada `delete`, aunque nuevamente esto no suele ser una buena práctica, tal vez establecer el campo a borrar con una representación de ausencia de valor sea más correcto.

```js
const dog = { name: "FooDog", tails: 1 };

delete dog.tails;
console.log(dog); // { name: "FooDog" };
```

## Objetos como tablas

Podemos usar los objetos como tablas de correlacion entre valores, en vez de caer en enormes grupos de condicionales, switch o if..else.

Si sabes que tus valores tienen un rango dado, esta técnica es muy util en el uso de objetos:

```js
function colorRGB(color) {
  switch (color) {
    case "red":
      return "#ff0000";
    case "green":
      return "#00ff00";
    case "blue":
      return "#0000ff";
    default:
      throw new Error(`color ${color}: not Implemented`);
  }
}
```

Aunque válido para tres valores, este tipo de códigos tiende a crecer, es más facil si:

```js
const RED_COLOR = "#ff0000";
const GREEN_COLOR = "#00ff00";
const BLUE_COLOR = "#0000ff";

const RGBColors = {
  red: RED_COLOR,
  green: GREEN_COLOR,
  blue: BLUE_COLOR
};

export function colorRGB(color) {
  return RGBColors[color];
}
```

Ejercicio:

Convierte la siguiente funcion en una busqueda del valor por tabla.

```js
function phoneticLookup(val) {
  let result = "";
  switch (val) {
    case "alpha":
      result = "Adams";
      break;
    case "bravo":
      result = "Boston";
      break;
    case "charlie":
      result = "Chicago";
      break;
    case "delta":
      result = "Denver";
      break;
    case "echo":
      result = "Easy";
      break;
    case "foxtrot":
      result = "Frank";
  }

  return result;
}

// Cambia esto para testear tu funcion
phoneticLookup("charlie");
```

## Testeando la existencia de propiedades de un objeto

Podemos testear si una propiedad existe, mediante el uso del metodo `hasOwnProperty`, que forma parte de la API de los objetos.

Esto es muy util para logicas dentro de condicionales.

```js
const dog = { name: "FooDog" };

dog.hasOwnProperty("name"); // true
dog.hasOwnProperty("legs"); // false
```

Ejercicio:

Comprueba cada propiedad del objeto y devuelve "Not Found" si no existe y "Found" si existe.

```js
let myObj = {
  gift: "pony",
  pet: "kitten",
  bed: "sleigh"
};

function checkObj(checkProp) {
  return "Change Me!";
}

checkObj("gift");
```

## Objetos complejos

JavaScript Object Notation (JSON) se ha extendido como un standard de facto para el intercambio entre clientes y servidores de información (incluso intercambio servidor-servidor).

La flexibilidad que nos ofrecen los objetos, permiten que podamos crear combinaciones arbitrarias de otros tipos como strings, numbers, arrays u otros objetos anidados.

Por ejemplo:

```js
// games shop
const stock = [{
    sku: "12a1d23af"
    title: "Pokemon Espada",
    cost: { value: 39.99, currency: "EUR" },
    metas: ["nintendo", "switch", "pokemon", "games",],
    withGift: true,
}]
```

## Accediendo objetos anidados

Las propiedades de un objeto valor de otro, son accesibles encadenando las notaciones conocidas:

```js
const user = {
  username: "foo",
  contact: {
    phone: "666123123"
  }
};

console.log(user.contact.phone); // "666123123"
console.log(user.contact["phone"]); // "666123123"
console.log(user["contact"].phone); // "666123123"
console.log(user["contact"]["phone"]); // "666123123"
```

## Acediendo arrays anidados

Los indices de un array valor de otro objeto, son accesibles encadenando las notaciones conocidas:

```js
const user = {
  username: "foo",
  groups: ["games", "health"]
};

console.log(user.groups[1]); // "health"
console.log(user["groups"][1]); // "health"
```

Ejercicio:
Vamos a crear una lista de direcciones de un telefono.

Implementa:

- Una funcion que permita crear objetos `Contact`, de este tipo:

  ```js
  const contact = {
      id: 1,
      name: "Foo Guy",
      surname: "Bar Boy",
      phones: [
          { type: "mobile": value: "666123123", },
          { type: "home": value: "913234567", }
      ],
      favorite: false,
  }
  ```

- Una variable global `contacts` de tipo objeto, cuyas keys son los ids de los `Contact`, como esta:

```js
const contactList = { 1: {
      id: 1,
      name: "Foo Guy",
      surname: "Bar Boy",
      phones: [
          { type: "mobile": value: "666123123", },
          { type: "home": value: "913234567", }
      ],
      favorite: false,
  }}
```

- Una funcion que permita añadir un `Contact` a `contactList`, añadiendo el id, y luego el objeto.

- Una funcion que dado un id, devuelva el `Contact` de la contactList con esa id, sino devuelve null;

- Una funcion que dado un id, permita añadir un nuevo telefono a la lista de telefonos de un contacto concreto de la `contactList`.

- Una funcion que dado un id, permita marcar como favorito un contacto de la `contactList`.

- Implementa un array de favoritos, cada vez que se marca como favorito un contacto en la `contactList`, se actualiza este array, que es de tipo global, y se le añade el id del favorito.

## this

Los metodos de un objeto, pueden acceder a las propiedades del mismo.

Ejemplo

```js
const contacto = {
  name: "foo",
  surname: "bar",

  fullName: function() {
    return this.name + " " + this.surname;
  }
};
```

La palabra reservada `this` se refiere al objeto en si, de esta manera podemos dar comportamientos parecidos a los objetos, en el caso del contacto, cada uno en su contexto dará un string u otro, pero representara el nombre completo.

Es decir, si tuvieramos dos instancias del objeto contacto, contacto1 y contacto2, al tener diferentes valores en la propiedad: `name` y `surname`, el metodo `fullName` retornará valores diferentes.

Ejercicio:

Crea un objeto de tipo persona, que diga su nombre completo por pantalla al invocar el metodo `sayYourName`.
El objeto debe contener nombre y apellidos. Luego crea dos instancias del objeto y haz que se ejecute el metodo.

## Funciones constructoras

Hasta ahora hemos creado nuestras propias funciones, que devuelven objetos nuevos en JavaScript. Hay veces que esto se hace largo.

Podemos hacer uso de las funciones constructoras para ello, un ejemplo:

```js
function Persona(nombre) {
  this.nombre = nombre;

  this.saludar = function() {
    alert("Hola! Soy " + this.nombre + "!");
  };
}

const yo = new Persona("Iván");
```

La palabra clave `new` se usa para indicarle al navegador que queremos crear una nueva instancia de objeto, seguida del nombre de la función con sus parámetros requeridos entre paréntesis, y el resultado se almacena en una variable — muy similar a cómo se llama una función estándar.

Ejercicio:

Crea un dos objetos de tipo Persona, que saluden con su propio nombre.

## Extendiendo los constructores con parámetros

Supongamos que estamos escribiendo un programa para realizar un seguimiento de cientos o incluso miles de perros diferentes en una asociación de acogida. Se necesitaría mucho tiempo para crear todos los perros, luego cambiar las propiedades a diferentes valores para cada una. Para crear más fácilmente diferentes objetos Dog, puede diseñar su constructor Dog para aceptar parámetros:

```js
function Dog(name, breed) {
  this.name = name;
  this.breed = breed;
}

const mastin = new Dog("Foo", "masting");
const terrier = new Dog("Foo", "terrier");
```

Ejercicio:

Crea un constructor de Contacto. Configuralo para tomar el nombre y el telefono de los parámetros, y tenga la propiedad favorito fijada en `false`.
