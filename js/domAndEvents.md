# DOM API

Hemos aprendido variables, estructuras de selección y bucles. Ahora es el momento de aprender sobre la manipulación del DOM

## Que es el DOM

Antes de sumergirnos en la codificación, aprendamos qué es realmente el Dom:

    El Document Object Model (DOM) es una interfaz de programación para documentos HTML y XML. Representa la página para que los programas puedan cambiar la estructura, el estilo y el contenido del documento. El DOM representa el documento como nodos y objetos. De esa manera, los lenguajes de programación pueden conectarse a la página.

Básicamente, cuando un navegador carga una página, crea un modelo de objeto de esa página y la imprime en la pantalla. El modelo de objeto se representa en una estructura de datos de árbol, cada nodo es un objeto con propiedades y métodos, y el nodo superior es el objeto del documento.

Se puede usar un lenguaje de programación para acceder y modificar este modelo de objetos, y esta acción se denomina manipulación DOM.

Creemos un index.html y index.js de ejemplo:

```html
<html>
  <head>
    <title>DOM Manipulation</title>

    <script src="index.js" defer></script>
  </head>
  <body>
    <div id="division"><h1 id="head"><em>DOM<em> manipulation</h1>
      <p class="text" id="middle">Tutorial</p><p>Sibling</p>
      <p class="text">Neoland Tutorial</p>
    </div>
    <p class="text">Out of the div</p>
  </body>
</html>
```

Tenemos un div con el id de `division`. Dentro de eso tenemos un elemento `h1`, y en la misma línea tenemos dos elementos `p` y la etiqueta de cierre `div`. Finalmente tenemos un elemento `p` con una clase `text`.

## Accediendo a los elementos

Podemos acceder a un solo elemento o a múltiples elementos desde JS.

### Accediendo a un sólo elemento

Para acceder a un sólo elemento, vamos a usar dos métodos: `getElementByID` y `querySelector`.

```js
let id = document.getElementById("head"); // accedemos al elemento con id head y lo almacenamos en nuestra variable
let q = document.querySelector("div p"); // accedemos al primer p del primer elemento div encontrado y lo guardamos en variable

id.style.color = "red"; // cambiamos su color a rojo
q.style.fontSize = "30px"; // cambiamos su fuente a 30px
```

Ahora hemos accedido a dos elementos, el elemento h1 con el id de head y el primer elemento p dentro del div.
getElementById toma como argumento un id, y querySelector toma como argumento un selector CSS y devuelve el primer elemento que coincide con el selector.

Ejercicio:
Accede al ultimo p mediante ambos metodos y cambia su color a verde y ponlo en negrita.

### Accediendo a múltiples elementos

Al acceder a múltiples elementos, se devuelve una lista de nodos. No es un array, pero funciona como tal. Por lo tanto, puedes recorrerlo y usar la propiedad de longitud para obtener el tamaño de la lista de nodos. Si deseas obtener un elemento específico, puedes usar la notación de array o el método del elemento.

Para acceder a múltiples elementos, vamos a utilizar tres métodos: getElementsByClassName, getElementsByTagName y querySelectorAll.

```js
// obtenemos todos los elementos con la clase text
let className = document.getElementsByClassName("text");
console.log(className); // imprimimos la lista de nodos
console.log(className[2]); // imprimimos el tercer elemento de la lista con notacion de array
console.log(className.item(2)); // imprimimos el tercer elemento de la lista mediante el uso del metodo item
let tagName = document.getElementsByTagName("p");
let selector = document.querySelectorAll("div p");
```

### Recorriendo el DOM

Hasta ahora hemos encontrado una forma de acceder a elementos específicos. ¿Qué sucede si queremos acceder a un elemento al lado de un elemento al que ya hemos accedido, o acceder al nodo padre de un elemento al que se accedió anteriormente? Las propiedades firstChild, lastChild, parentNode, nextSibling y previousSibling pueden hacer este trabajo por nosotros.

- firstChild se usa para obtener el primer elemento hijo de un nodo.
- lastChild, como adivinó, obtiene el último elemento hijo de un nodo.
- parentNode se usa para acceder a un nodo padre de un elemento.
- nextSibling obtiene para nosotros el elemento al lado del elemento al que ya hemos accedido.
- previousSibling nos obtiene el elemento anterior al elemento al que ya hemos accedido.

```js
let fChild = document.getElementById("division").firstChild;
console.log(fChild); /
let lChild = document.querySelector("#division").lastChild;
let parent = document.querySElector("#division").parentNode;
console.log(parent);
let middle = document.getElementById("middle");
console.log(middle.nextSibling);
```

El código anterior primero obtiene el elemento firstChild del elemento con la identificación de divison y luego lo imprime en la consola. Luego obtiene el elemento lastChild del mismo elemento con el ID de division. Luego obtiene el ParentNode del elemento con el id de division y lo imprime en la consola. Finalmente, selecciona el elemento con la identificación de middle e imprime su nodo nextSibling.

La mayoría de los navegadores tratan los espacios en blanco entre los elementos como nodos de texto, lo que hace que estas propiedades funcionen de manera diferente en los diferentes navegadores.

Ejercicio:

Tomate 10 min para obtener los diferentes elementos del html y jugar a llegar a sus hermanos, pregunta cualquier duda que tengas.

### Obteniendo y actualizando elementos

Podemos obtener o establecer el contenido de texto de los elementos. Para lograr esta tarea, vamos a utilizar dos propiedades: nodeValue y textContent.

- nodeValue se usa para establecer u obtener el contenido de texto de un nodo de texto.
- textContent se usa para establecer u obtener el texto de un elemento contenedor.

```js
let nodeValue = document.getElementById("middle").firstChild.nodeValue; // obtenemos el texto con nodeValue
console.log(nodeValue); // establecemos el texto con nodeValue
document.getElementById("middle").firstChild.nodeValue = "nodeValue text"; // obtenemos el texto con nodeValue
let textContent = document.querySelectorAll(".text")[1].textContent; //
console.log(textContent); // establecemos el texto con textContent
document.querySelectorAll(".text")[1].textContent = "new textContent set";
```

Si observas detenidamente el código anterior, verás que para que podamos obtener o establecer el texto con nodeValue, primero tuvimos que seleccionar el nodo de texto. Primero, obtuvimos el elemento con la identificación de middle, luego obtuvimos su firstChild, que es el nodo de texto, luego obtuvimos el nodeValue que devolvió la palabra Tutorial.

Ahora con textContent, no hay necesidad de seleccionar el nodo de texto, solo obtuvimos el elemento y luego obtuvimos su textContent, ya sea para establecer u obtener el texto.

### Añadiendo y eliminando contenido HTML

Puedes agregar y eliminar contenido HTML en el DOM. Para eso, vamos a ver tres métodos y una propiedad.

Comencemos con la propiedad innerHTML porque es la forma más fácil de agregar y eliminar contenido HTML. innerHTML puede usarse para obtener o establecer contenido HTML. Pero ten cuidado al usar innerHTML para establecer contenido HTML, ya que elimina el contenido HTML que está dentro del elemento y agrega el nuevo.

```js
document.getElementById("division").innerHTML = `<ul>
    <li>React</li>
    <li>Vue</li>
    <li>Angular</li>
</ul>`;
```

Si ejecutas el código, notarás que todo lo demás en el div con el id de divison desaparece, y se agregará la lista.

Podemos usar los métodos: createElement(), createTextNode() y appendChild() para resolver este problema.

- createElement se usa para crear un nuevo elemento HTML.
- creatTextNode se usa para crear un nodo de texto.
- appendChild se usa para agregar un nuevo elemento a un elemento padre.

```js
let newElement = document.createElement("p");
let text = document.createTextNode("Text Added!");
newElement.appendChild(text);
document.getElementById("division").appendChild(newElement);
```

También hay un método llamado removeChild que se usa para eliminar elementos HTML.

```js
let toBeRemoved = document.getElementById("head");
let parent = toBeRemoved.parentNode;
parent.removeChild(toBeRemoved);
```

Primero obtenemos el elemento que queremos eliminar, y luego obtenemos su nodo padre. Luego llamamos al método removeChild para eliminar el elemento.

### Atributos de nodo

Ahora sabemos cómo manejar elementos, así que aprendamos cómo manejar los atributos de estos elementos. Hay algunos métodos como getAttribute, setAttribute, hasAttribute, removeAttribute y algunas propiedades como className e id.

- getAttribute como su nombre lo sugiere, se usa para obtener un atributo. Al igual que el nombre de la clase, el nombre de identificación, la href de un enlace o cualquier otro atributo HTML.

- setAttribute se usa para establecer un nuevo atributo para un elemento. Toma dos argumentos, primero el atributo y segundo el nombre del atributo.

- hasAttribute utilizado para verificar si existe un atributo, toma un atributo como argumento.

- removeAttribute utilizado para eliminar un atributo, toma un atributo como argumento.

```js
let d = document.querySelector("div");
console.log("checks id: " + d.hasAttribute("id"));
d.setAttribute("class", "newClass");
console.log(d.className);
```

Ejercicio:

Investiga la api de dom para añadir funciones a los eventos onclick de un boton, que añadas.
Incluye un boton y bindea al mismo una funcion, que cambie el color de todos los elementos con la clase texto.
Incluye un boton y bindea al mismo una funcion, que restaure los estilos de toda la pagina.
Incluye una lista desordenada, que muestre el valor de los atributos de cada elemento text dentro del dom.

Ejercicio:

Escribe una funcion JS que muestre una de estas imagenes con los siguientes atributos de manera aleatoria, clickando en un boton.

- "http://farm4.staticflickr.com/3691/11268502654_f28f05966c_m.jpg", width: "240", height: "160"
- "http://farm1.staticflickr.com/33/45336904_1aef569b30_n.jpg", width: "320", height: "195"
- "http://farm6.staticflickr.com/5211/5384592886_80a512e2c9.jpg", width: "500", height: "343"
