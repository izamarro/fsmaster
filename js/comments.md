# Comentarios

Los comentarios son líneas de código que JavaScript ignorará intencionalmente. Los comentarios son una excelente manera de dejar notas para ti y para otras personas que luego necesitarán averiguar qué hace ese código.

Hay dos formas de escribir comentarios en JavaScript:

El uso de `//` le indicará a JavaScript que ignore el resto del texto en la línea actual:

```js
// Este es un comentario en línea.
```

Puede hacer un comentario de varias líneas que comience con `/*` y termine con `*/`:

```js
/* Esto es un
comentario de varias líneas * /
```

**Mejores prácticas**

A medida que escribes código, debes agregar comentarios regularmente para aclarar la función de partes del código. Un buen comentario puede ayudar a comunicar la intención del código, tanto para los demás como para ti en el futuro.

Ejercicio:

Intenta crear un comentario de cada tipo.
