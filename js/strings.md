# Strings

Las palabras son muy importantes para los humanos son una parte fundamental de nuestra comunicación. Dado que la Web es un medio en gran parte basado en texto diseñado para permitir a los humanos comunicarse y compartir información, es útil para nosotros tener control sobre las palabras que aparecen en él. HTML proporciona estructura y significado a nuestro texto, CSS nos permite personalizarlo con precisión, y JavaScript contiene una serie de funciones para manipular cadenas, crear mensajes personalizados de bienvenida, mostrar las etiquetas de texto adecuadas cuando sea necesario, ordenar los términos en el orden deseado y mucho más.

A primera vista, las cadenas se tratan de forma similar a los números, pero cuando profundizas empiezas a ver diferencias notables.

## Declaración de variables con cadenas

```
let myName = "tu nombre";
```

"myName" se llama literal de string. Es un string porque es una serie de cero o más caracteres encerrados entre comillas simples o dobles.

Al igual que con los números, declaramos una variable, inicializándola con el valor de una cadena, y luego retornamos dicho valor. La única diferencia es que al escribir una cadena, necesitas envolverla con comillas.

### Comillas simples vs dobles

Los valores de cadena en JavaScript pueden escribirse con comillas simples o dobles, siempre que comiences y termines con el mismo tipo de comillas. A diferencia de otros lenguajes de programación, las comillas simples y dobles funcionan igual en JavaScript.

```js
doubleQuoteStr = "Esto es una cadena";
singleQuoteStr = "Esto también es una cadena";
```

La razón por la que es posible que desee utilizar un tipo de cita sobre el otro es si desea utilizar ambos en una cadena. Esto puede suceder si deseas guardar una conversación en una cadena y tener la conversación entre comillas. Otro uso para esto sería guardar una etiqueta `<a>` con varios atributos entre comillas, todo dentro de una cadena.

```js
conversación = 'Finn exclama a Jake, "¡Algebraico!"';
```

Sin embargo, esto se convierte en un problema si necesita utilizar las comillas más externas dentro de él. Recuerda, una cadena tiene el mismo tipo de comillas al principio y al final. Pero si tienes esa misma cita en algún lugar en el medio, la cadena se detendrá antes y arrojará un error.

```js
    goodStr = 'Jake asks Finn, "Hey, let\'s go on an adventure?"';
    badStr = 'Finn responds, "Let's go!"'; // Lanza un error
```

En el goodStr anterior, puede usar ambas comillas de forma segura utilizando la barra invertida `\` como carácter de escape. Nota
La barra invertida `\` no debe confundirse con la barra diagonal `/`. No hacen lo mismo.

## Escapando caracteres

Cuando defines una cadena, debe comenzar y finalizar con una comilla simple o doble. ¿Qué sucede cuando necesitas una cita literal: "o 'dentro de su cadena?

En JavaScript, puede escapar de una cita al considerarla como una cita al final de la cadena colocando una barra invertida `(\)` delante de la cita.

```js
let sampleStr = "Iván dijo, \" Ana está aprendiendo JavaScript \ ".";
```

Esto le indica a JavaScript que la siguiente cita no es el final de la cadena, sino que debería aparecer dentro de la cadena. Entonces, si imprimieramos esto en la consola, obtendría:

`Alan dijo: "Peter está aprendiendo JavaScript".`

Ejercicio:

```js
let myStr = "";
```

Usa barras invertidas para asignar una cadena a la variable myStr de modo que si la imprimieras en la consola, verías:

`Soy una cadena de "comillas dobles" dentro de "comillas dobles".`

Ejercicio:

```js
let myStr = '<a href="http://www.example.com" target="_blank">Link</a>';
```

Cambia la cadena proporcionada a una cadena con comillas simples al principio y al final y sin caracteres de escape.

Las citas no es lo único que podemos escapar en JS, también podemos escapar caracteres. Hay dos razones para usar caracteres de escape:

- Para permitirte usar caracteres, que de lo contrario no podrías escribir, como un retorno de carro.
- Para permitirte representar múltiples comillas en una cadena sin que JavaScript malinterprete lo que quiere decir.

![caracters de escape](assets/escape_chars.png)

Ejercicio:

```js
let myStr;
```

Asigna las siguientes tres líneas de texto en la variable individual myStr usando secuencias de escape.

```text
Primera linea
    \Segunda linea
ThirdLine
```

Deberás usar secuencias de escape para insertar caracteres especiales correctamente. También deberás seguir el espaciado como se ve arriba, sin espacios entre las secuencias de escape o las palabras.

Aquí está el texto con las secuencias de escape escritas.

```text
FirstLine newline tab backslash SecondLine newline ThirdLine
```

## Concatenando caraceteres

En JavaScript, cuando el operador `+` se usa con un valor de cadena, se llama operador de concatenación. Puedes construir una nueva cadena a partir de otras cadenas concatenándolas juntas.

Ejemplo:

```js
'Mi nombre es Iván' + 'Yo Concateno strings'.
```

Nota:
Cuidado con los espacios. La concatenación no agrega espacios entre cadenas concatenadas, por lo que deberás agregarlas.

También podemos usar el operador `` para concatenar una cadena al final de una variable de cadena existente. Esto puede ser muy útil para romper una cadena larga en varias líneas.

```js
let ourStr = "I come first. ";
ourStr += "I come second.";
```

Todo esto sigue las mismas reglas si usamos variables para concatenar nuestro texto, por ejemplo es posible hacer:

```js
let ourName = "NodeLand";
let ourStr = "Hello, my name is " + ourName + ", how are you?";
```

## Numeros y Strings

Si intentamos hacer lo siguiente:

```js
1 + "foo"; // devuelve "1foo"
```

Podríamos esperar un error, pero JS es capaz de operar sobre ello, dado que convierte el número en un literal de string.

Si tienes una variable numérica, que deseas convertir en una string, pero no cambiar de otra forma, o una variable de string, que deseas convertir a número, pero no cambiarla de otra forma, puedes usar las siguientes construcciones:

```js
let myString = "123";
let myNum = Number(myString);
typeof myNum;
```

Por otra parte, cada número tiene un método llamado `toString()` que convertirá el equivalente en una string. Prueba esto:

```js
let myNum = 123;
let myString = myNum.toString();
typeof myString;
```

Estas construcciones pueden ser muy útiles en ciertas situaciones. Por ejemplo, si un usuario introduce un número en un campo de formulario de texto, será un string. Sin embargo, si quieres añadir ese número a algo, lo necesitas convertir a número, así que puedes usar esta construcción para hacerlo.

## Métodos de manejo de cadenas

**TODO en JS es un objeto**, metetelo en la cabeza. Cuando creas una cadena:

```js
let myStr = "foo";
```

Tu variable se convierte en una instancia del objeto `String` y, como resultado, tiene una gran cantidad de propiedades y métodos disponibles.

### Hallar la longitud de la cadena

Puede encontrar la longitud de un valor de cadena escribiendo `.length` después de la variable de cadena o literal de cadena.

```js
"Alan Peter".length; // 10
```

Ejercicio:

```js
let lastNameLength = 0;
let lastName = "Lovelace";
```

Usa la propiedad `.length` para conocer la longitud de la variable `lastName` y asignala `lastNameLength`.

### Extrayendo caracteres

La notación de `[]` es una forma de obtener un carácter en un índice específico dentro de una cadena.

La mayoría de los lenguajes de programación modernos, como JavaScript, no comienzan a contar en 1 como lo hacen los humanos. Comienzan en 0. Esto se conoce como indexación basada en cero.

Por ejemplo, el carácter en el índice 0 en la palabra "Charles" es "C". Entonces, si `let firstName = "Charles"`, puedes obtener el valor de la primera letra de la cadena utilizando `firstName[0]`.

Ejercicio:

```js
let firstLetter = "";
let lastName = "Lovelace";
```

Busca la primera letra de `lastName`y asignala a `lastLetter`;

```js
let lastLetter = "";
let randomWord = Math.random()
  .toString(36)
  .substring(7);
```

Como alcanzarias el ultimo caracter de una cadena de texto aleatoria con los 2 metodos presentados hasta ahora, trata de mezclar la longitud con la extracción y asignala a lastLetter.

### Inmutabilidad de Strings

En JavaScript, los valores de cadena son inmutables, lo que significa que no pueden modificarse una vez creados.
Por ejemplo, el siguiente código:

```js
let myStr = "Brabajo";
myStr[0] = "J";
```

No puede cambiar el valor de myStr a "Job", porque el contenido de myStr no puede modificarse. Ten en cuenta que esto no significa que myStr no se pueda cambiar, solo que los caracteres individuales de un literal de cadena no se pueden cambiar. La única forma de cambiar myStr sería asignarlo con una nueva cadena, como esta:

```js
let myStr = "Brabajo";
myStr = "Trabajo";
```

### Encontrar una subcadena dentro de una cadena y extraerla

Algunas veces querrás encontrar si hay una cadena más pequeña dentro de una más grande (generalmente decimos si una subcadena está presente dentro de una cadena). Esto se puede hacer utilizando el método indexOf(), que toma un único parametro — la subcadena que deseas buscar:

```js
let myStr = "mozilla";
myStr.indexOf("zilla");
```

Esto nos dá un resultado de 2, porque la subcadena "zilla" comienza en la posición 2 (0, 1, 2 — 3 caracteres) dentro de "mozilla". Este código podría usarse para filtrar cadena. Por ejemplo, podemos tener una lista de direcciones web y solo queremos imimprimir las que contienen "mozilla".

Si la cadena no existe arrojara un -1.

Cuando sabes donde comienza una subcadena dentro de una cadena, y sabes hasta cuál caracter deseas que termine, puede usarse slice() para extraerla:

```js
myStr.slice(0, 3);
```

Esto devuelve "moz" — El primer parámetro es la posición del caracte en la que comenzar a extraer, y el segundo parámetro es la posición del caracter posterior al último a ser extraído. Por lo tanto, el corte ocurre desde la primer posición en adelante, pero excluyendo la última posición.

Además, si sabes que deseas extraer todos los caracteres restantes de una cadena después de cierto caracter, ¡no necesitas incluir el segundo parámetro! En cambio, solo necesitas incluir la posición del caracter desde donde deseas extraer los caracteres restante en la cadena:

```js
myStr.slice(2);
```

Esto devuelve "zilla" — debido a que la posición del caracter de 2 es la letra z, y como no incluiste un segundo parámetro, la subcadena que que se devolvío fué el resto de los caracteres de la cadena.

### Cambiando todo a mayúscula o minúscula

Los métodos de cadena `toLowerCase()` y `toUpperCase()` toman cadena y convierten todos los caracteres a mayúscula o minúscula, respectivamente. Esto puede ser útil, por ejemplo, si deseas normalizar todos los datos ingresados por el usuario antes de almacenarlos en una base de datos.

```js
let radData = "sOY uN mOrEnHiThOoO rEShuLon dE lOs 90";
radData.toLowerCase();
radData.toUpperCase();
```

### Reemplazamiento

En una cadena puedes reemplazar una subcadena por otra usando el método `replace()`.

```js
let myStr = "foo bar";
myStr.replace("foo", "bar");
```
