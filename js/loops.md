# Loops

Los bucles de programación están relacionados con todo lo referente a hacer una misma cosa una y otra vez — que se denomina como iteración en el idioma de programación.

Un bucle cuenta con una o más de las siguientes características:

- Un contador, que se inicia con un determinado valor — este será el valor del punto inicial del bucle.

- Una condicion de salida, que será el criterio bajo el cual, el bucle se romperá — normalmente un contador que alcanza un determinado valor.

- Un iterador, que generalmente incrementa el valor del contador en una cantidad pequeña a cada paso del bucle, hasta que alcanza la condición de salida.

¿Por qué nos interesan los bucles?
Porque nos permiten realizar tareas repetitivas que de otra manera incrementarían la longitud de nuestro codigo.

## while

Puede ejecutar el mismo código varias veces utilizando un bucle.

El primer tipo de bucle que aprenderemos se llama bucle `while` porque se ejecuta "mientras" una condición específica es verdadera y se detiene una vez que esa condición ya no es verdadera.

```js
let arr = [];
let i = 0;

while (i < 5) {
  arr.push(i);
  i++;
}
```

En el ejemplo de código anterior, el bucle while se ejecutará 5 veces y agregará los números del 0 al 4 a nuestro array.

**Cuidado con los bucles infinitos!!! Si la condición del while no deja de cumplirse, nunca saldrás del mismo**

Ejercicio:

Incluye el numero 5, incluido, en orden descendiente dentro de un array.

## for loop

Puedes ejecutar el mismo código varias veces utilizando un bucle.

El tipo más común de bucle de JavaScript se denomina bucle `for` porque se ejecuta "por" un número específico de veces.

Los bucles `for` se declaran con tres expresiones opcionales separadas por punto y coma:

```text
for ([inicialización]; [condición]; [expresión final])
```

La instrucción de inicialización se ejecuta una sola vez antes de que comience el ciclo. Normalmente se usa para definir y configurar la variable de conteo del bucle.

La declaración de condición se evalúa al comienzo de cada iteración del bucle y continuará mientras se evalúe como verdadera. Cuando la condición es falsa al comienzo de la iteración, el bucle dejará de ejecutarse. Esto significa que si la condición comienza como falsa, su primera iteracion nunca se ejecutará.

La expresión final se ejecuta al final de cada iteración, antes de la siguiente verificación de la condición y generalmente se usa para aumentar o disminuir su contador del bucle.

En el siguiente ejemplo, inicializamos con i = 0 e iteramos mientras nuestra condición i < 5 es verdadera. Incrementaremos i en 1 en cada iteración de bucle con i ++ como nuestra expresión final.

```js
let arr = [];
for (let i = 0; i < 5; i++) {
  arr.push(i);
}

// 0, 1, 2, 3, 4
```

```text
Nota: Los bucles for, no tienen porque iterar de uno en uno, se puede incrementar la variable con cualquier modificador, por ejemplo: i+=2:

for (let i = 0; i < 10; i+=2) {
  arr.push(i);
}
```

Ejercicio:

Usa el bucle for, para introducir 5 objetos dentro de un array.

Ejercicio:

Contar seleccionados

## Iterando con for...loop sobre un array

Una tarea común en JavaScript es iterar a través del contenido de un array. Una forma de hacerlo es con un bucle for.

```js
let arr = [1, 2, 3, 4, 5];
for (let i = 0; i < arr.length; i++) {
  console.log(arr[i]);
}
```

Los arrays tienen una indexación basada en cero, lo que significa que el último índice de la matriz es de longitud_del_array - 1. Nuestra condición para este ciclo es `i < arr.length`, que detiene el ciclo cuando i es igual a la longitud. En este caso, la última iteración es i === 4, es decir, cuando se vuelve igual a la longitud de arr.

Ejercicio:

Implementa un bucle for, que sume a `suma` los valores del array.

```js
let suma = 0;
const precios = [90.95, 32.0, 10.05, 33.5];
```

## Saliendo de un bucle for con `break`;

Podemos condicionar la salida de un bucle mediante la palabra reservada `break`, o `return` dentro de una funcion, por ejemplo si estamos recorriendo un array para buscar un elemento, podemos parar el bucle cuando lo encontremos.

Ejemplo:

```js
const contactos = [
  { id: 1, name: "Foo" },
  { id: 2, name: "Bar" }
];

let buscado;

for (let i = 0; i < contactos.length; i++) {
  if (contactos[i].id === id) {
    buscado = contactos[i];
    break;
  }
}
```

Ejercicio:

Encapsula el bucle anterior en una funcion que reciba el id de un contacto y retorne el mismo.

## Bucles anidados

Si tienes un array multidimensional, puedes usar los bucles para recorrer los arrays interiores:

```js
let arr = [
  [1, 2],
  [3, 4],
  [5, 6]
];
for (let i = 0; i < arr.length; i++) {
  for (let j = 0; j < arr[i].length; j++) {
    console.log(arr[i][j]);
  }
}
```

Ejercicio:

Implementa una funcion que sume los valores de cada array interno del array parametro, lo acumule y devuelva el total.

```js
function sumAll(arr) {}

sumAll([
  [1, 2],
  [3, 4],
  [5, 6, 7]
]); // 28
```

## for...in

La sentencia for...in itera una variable especificada sobre todas las propiedades enumerables de un objeto. Para cada propiedad distinta, JavaScript ejecuta las sentencias especificadas. Una sentencia for...in será como sigue:

```text
for (variable in objeto) {
  sentencias
}
```

Ejemplo:

```js
let duck = { name: "fooDuck", numLegs: 2 };
let ownProps = [];

for (let property in duck) {
  if (duck.hasOwnProperty(property)) {
    ownProps.push(property);
  }
}

console.log(ownProps); // logs [ "name", "numLegs" ]
```

Como norma general, no usaremos este tipo de bucles para recorrer un array, debido a que nos devolvería una dupla con los nombres de las propiedades y el índice.

## for...of

La sentencia for...of crea un bucle iterando sobre objetos iterables (incluyendo Array, Map, Set, argumentos, objetos etc), invocando una iteración personalizada conectando con sentencias para ser ejecutadas por el valor de cada propiedad distinta.

```js
const arr = [3, 5, 7];
for (let i of arr) {
  console.log(i); // logs "3", "5", "7"
}
```
