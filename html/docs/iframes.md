# Insertando contenido con `<iframe>`

Hace mucho tiempo en la Web, era popular usar marcos para crear sitios web, pequeñas partes de un sitio web almacenadas en páginas HTML individuales. Estos estaban incrustados en un documento maestro llamado conjunto de marcos , que le permitía especificar el área en la pantalla que ocupaba cada cuadro, como el tamaño de las columnas y las filas de una tabla. Estos fueron considerados el colmo del frescor a mediados y finales de los 90, y había evidencia de que tener una página web dividida en trozos más pequeños como este era mejor para velocidades de descarga, especialmente notable con conexiones de red tan lentas en ese momento. Sin embargo, tuvieron muchos problemas, que superaron con creces cualquier aspecto positivo ya que las velocidades de red se hicieron más rápidas, por lo que ya no se ve que se usen.

Poco tiempo después (finales de los 90, principios de 2000), las tecnologías de complementos se volvieron muy populares, como los Applets de Java y Flash . Esto permitió a los desarrolladores web incorporar contenido enriquecido en páginas web como videos y animaciones, que simplemente no estaban disponibles solo a través de HTML. . La incrustación de estas tecnologías se logró a través de elementos como `<object>` y el `<embed>` menos utilizado, y fueron muy útiles en ese momento. Desde entonces, pasaron de moda debido a muchos problemas, incluidos el acceso, la seguridad, el tamaño del archivo y más; en la actualidad, la mayoría de los dispositivos móviles ya no son compatibles con estos complementos, y el soporte de escritorio está en camino de desaparecer.

Finalmente, apareció el elemento `<iframe>` (junto con otras formas de incrustación de contenido, como `<canvas>` , `<video>` , etc.). Esto proporciona una forma de insertar un documento web entero dentro de otro, como si fuera un `<img>` u otro elemento similar, y se usa regularmente hoy.

## `<iframe>`

Los elementos `<iframe>` están diseñados para permitirle incrustar otros documentos web en el documento actual. Esto es excelente para incorporar contenido de terceros en su sitio web sobre el que no tenga control directo y no quiera implementar su propia versión,como video de proveedores de video en línea, sistemas de comentarios como Disqus, mapas de mapas en línea proveedores, banners publicitarios, etc.

Hay algunos conceptos de seguridad a tener en cuenta con `<iframe>` s, pero esto no significa que no debas usarlas en tus sitios web, solo requiere un poco de conocimiento y cuidado.

```html
<iframe
  src="https://holadoctor.com/es"
  width="100%"
  height="500"
  frameborder="0"
  sandbox
>
  <p>
    <a href="https://holadoctor.com/es">
      Fallback link for browsers that don't support iframes
    </a>
  </p>
</iframe>
```

_Nota: generalmente es mejor insertar el atributo src con JS debido a que esto mejorará los tiempos de carga si lo hacemos despues de que nuestra página cargue_

El atributo más importante que aquí encontramos es:

- `sandbox`: Este atributo, permite una mayor configuración de seguridad del `<iframe>`.

## Seguridad

Los creadores de navegadores y los desarrolladores web han aprendido por las malas que los iframes son un vector de ataque, para que crackers ataquen modificando maliciosamente su página web, o engañar a las personas para que hagan algo que no quieren hacer, como revelar información confidencial como nombres de usuario y contraseñas. Debido a esto, los ingenieros de especificaciones y los desarrolladores de navegadores han desarrollado varios mecanismos de seguridad para hacer que los `<iframe>` sean más seguros.

De todos modos repasemos algunas medidas:

- Solo inserta contenido si es necesario
- Usa HTTPS **SIEMPRE**
- Configura las directivas CSP
- Usa el atributo `sandbox`para establecer permisos uno por uno, pero nunca con valores "allow-scripts" y "allow-same-origin"
