# Hipervínculos HTML

Los Hipervínculos ó enlaces son verdaderamente importantes — son los que hacen que la Web sea navegable.

Los hyperlinks han formado parte de la Web desde el principio, pero hacen que la Web sea Web — permiten enlazar nuestros documentos a cualquier otro documento (o recurso), hacer referencia a partes específicas de los mismos, podemos hacer las aplicaciones accesibles con una sencilla dirección web (al contrario de las apps nativas que deben ser instaladas con todo lo que esto conlleva). Cualquier contenido web puede ser convertido en un link, para que al pulsarlo el navegador se dirija a la dirección web a la que apunte el link. (URL)

_Nota: Una URL puede apuntar a ficheros HTML, ficheros de texto, imágenes, documentos de texto, archivos de audio o video, y cualquier otra cosa que pueda ser mostrada en la Web. Si el navegador no sabe cómo manejar el archivo, nos preguntará si lo queremos abrir (en cuyo caso la tarea de abrirlo y manejarlo será transferida a la aplicación nativa instalada en el dispositivo) o lo queremos descargar._

La web de `meneame.net` incluye muchisimos enlaces a diferentes partes de noticias, tanto internas como externas, imagenes, etc.

![web de meneame](assets/meneame.png)

## Anatomía de los links

Un link básico se crea incluyendo el texto  que deseemos convertir en un link dentro de un elemento ancla `<a>`, dándole un atributo `href` que contendrá la dirección web hacia dónde deseemos que apunte el link:

```html
<p>
  He creado un link <a href="https://linkadondequeramos.es"> a donde quiero</a>.
</p>
```

### Atributo `title`

Adicionalmente podemos proporcionar información adicional sobre el destino del link con el atributo `title`.

```html
<p>
  He creado un link <a href="https://linkadondequeramos.es" title="El lugar donde esta mi link, una buena página"> a donde quiero</a>.
</p>
```

### Atributo `target`

El atributo `target` especifica dónde abrir el documento vinculado.

Toma los siguientes valores:

**_blank**: - Abre el documento vinculado en una nueva ventana o pestaña
**_self**:: abre el documento vinculado en la misma ventana / pestaña en la que se hizo clic (por defecto)
**_parent**: - Abre el documento vinculado en el frame primario

```html
<p>
  He creado un link <a href="https://linkadondequeramos.es" target="_blank"> a donde quiero</a>.
</p>
```

### Bloques de contenido como links

Como hemos mencionado anteriormente, puedes convertir cualquier contenido en un link. Si dispones de una imagen que desees convertirla en un link, simplemente ánclala entre elementos `<a></a>`.

```html
<a href="https://milink.es">
  <img src="mi-imagen.png" alt="mi imagen vinculada a la pagina de mi link">
</a>
```

## URLS y referencias


Una URL (de las iniciales en inglés Uniform Resource Locator - localizador de recursos estandarizado) es simplemente una secuencia de caracteres de texto que definen donde está situado algo en la web. Por ejemplo la página de Neoland está ubicada en https://www.neoland.es/

Las URLs utilizan las referencias para encontrar los archivos. Las referencias especifican donde se encuentra el archivo que buscas dentro del sistema de archivos.

Suponiendo una estructura de directorios:

```
index.html
seccion.html
assets/
    imagen.png
projects/
    index.html
```

`index.html` sería el punto de entrada a nuestra web, aunque tengamos dos, el del directorio más alto es el que hace de página de inicio. Al estar alojados en directorios diferentes, esto no supone un problema.

- **Referencia en el mismo directorio:** Si quisieramos crear un link al archivo `seccion.html` desde nuestra página de inicio `index.html`, simplemente especificaremos el nombre del archivo al que hagamos referencia, pues este se encuentra en el mismo directorio en el que se encuentra el archivo `index.html` desde donde lo queremos llamar. Por lo tanto, la URL que usaremos será "seccion.html":

  ```html
  <p>
    He creado un link <a href="seccion.html"> a mi sección</a>.
  </p>
  ```

- **Bajando directorios**: Si quisieramos crear un link hacia el `index.html` de nuestro directorio `projects/`, desde nuestra página de inicio `index.html`. Debemos bajar al mismo apuntando a `projects/index.html` desde el link:
  ```html
  <p>
    He creado un link <a href="projects/index.html"> a mis proyectos</a>.
  </p>
  ```

- **Subiendo directorios**: Si quisieramos crear un link hacia el archivo `seccion.html` de nuestro directorio raiz, desde nuestra página de inicio `index.html` de proyectos. Debemos subir al mismo apuntando a `../seccion.html` desde el link:
  ```html
  <p>
    He creado un link <a href="../seccion.html"> a mi sección</a>.
  </p>
  ```

### URLS absolutas vs relativas

- **URL absoluta:** Hace referencia a una dirección definida por su ubicación absoluta en la web, incluyendo el protocolo y el nombre del dominio. Una URL absoluta siempre apuntará hacia la misma dirección, sin importar desde donde se utilice.

- **URL relativa:** Hace referencia a una dirección que depende de la posición del archivo desde donde se utiliza, son las que hemos visto en la sección anterior. Por ejemplo, si quisiéramos enlazar desde un archivo ubicado en: "http://www.example.com/projects/index.html" hacia un archivo PDF ubicado en el mismo directorio, la URL sería simplemente el nombre del archivo, no necesitaríamos más información para referenciarlo. Una URL relativa hará referencia a diferentes direcciones dependiendo de dónde se encuentre el archivo desde el cual sea utilizada

## Apuntando a fragmentos de un documento

Es posible apuntar hacia una parte concreta de un documento HTML en vez a todo un documento. Para hacerlo deberemos asignar previamente un atributo `id` al elemento hacia el que queremos apuntar.

```html
<p>Escribe un email <a href="#Mailing_address">a la dirección</a> que encontrarás al final de la página.</p>

<!-- contenido -->

<h2 id="Mailing_address">Mailing address</h2>
```

## Buenas prácticas

Algunas de las siguientes, son las prácticas que debemos seguir a la hora de crear links.

### Describe los links

Es fácil escribir links en una página sin más. Pero debemos hacer que nuestros links sean accesibles para todo tipo de lectores, sin importar el contexto o las herramientas que prefieran:

- Los lectores autmáticos van saltando de link en link en la página y los leen todos de forma consecutiva.

- Los motores de búsqueda utilizan los links de texto para indexar los archivos buscados, por lo que es buena idea incluir palabras clave al definir los links de texto para describir de forma efectiva el sitio al que apuntan.

- Los usuarios echan un vistazo rápido a la página leyendo solo aquello que les interesa en lugar de leer todo el texto palabra por palabra, y sus miradas van directamente a las características destacadas de la página, como son los links. Este tipo de usuarios encuentran útiles los textos descriptivos de los mismos.


```html
<!-- BIEN -->
<p><a href="https://firefox.com/">Download Firefox</a></p>

<!-- MAL -->
<p><a href="https://firefox.com/">Click here</a>to download Firefox</p>
```

- No se debe repetir la URL como parte del texto — las URLs suenan horrible, y todavía suenan peor si el que las lee lo hace letra a letra.

- No escribir "link" o "link a" o "enlace" o "enlace a" en el texto del link — esto es redundante. Los lectores automáticos indican que hay un link al encontrarlo. Los usuarios también saben que hay un link, ya que normalmente se suele cambiar el color del texto y se suele subrayar.

- Redactar la etiqueta del link de la manera más breve y concisa posible los textos de link largos son especialmente molestos para los usuarios que utilizan lectores automáticos, ya que tienen que escuchar todo el texto de la página.

- Reducir las ocurrencias de copias exactas del mismo texto que apunten a lugares diferentes, ya que los lectores automáticos los leen como una lista descontextualizada — varios links etiquetados como "click here", "click here", "click here" pueden resultar confusos para los usuarios.

### Usa referencias relativas

De las indicaciones anteriores podemos llegar a pensar que es mejor utilizar referencias absolutas en todos los casos; después de todo, estas no se rompen cuando la página se traslada como ocurre con las referencias relativas. Sin embargo, se deben usar referencias relativas siempre y cuando estemos haciendo referencia a direcciones dentro de una misma página web (cuando hagamos referencia a páginas web externas, utilizaremos las referencias absolutas):

1. Primero, es mucho más fácil leer la codificación — las URLs relativas son por lo general mucho más cortas que las absolutas, lo que hace que el código sea mucho más fácil de leer.

2. Segundo, es mucho más eficiente utilizar URLs relativas cuando sea posible. Al utilizar una URL absoluta, el navegador comienza por la dirección real del servidor haciendo un requerimiento al DNS del Dominio DNS, a continuación se dirige a ese servidor y busca el archivo requerido. En cambio, con una URL relativa, el navegador solo busca el fichero requerido dentro del mismo servidor. Por lo tanto si utilizamos URLs absolutas en lugar de relativas, estamos haciendo que el navegador realice constantemente trabajos extra, haciendo que el rendimiento sea menos eficiente.

### Si enlazas a un recurso no-HTML: ¡SEÑALALO!

Cuando referenciamos a recursos para ser descargados (como PDFs o documentos Word) o reproducidos (como audio o video) o que tengan un efecto inesperado deberemos señalarlo para no confundir al usuario. Resulta bastante molesto por ejemplo; si tienes una conexión con bajo ancho de banda, pulsas un link y de repente comienza a descargarse un archivo pesado de forma inesperada.

```html
<p><a href="http://www.example.com/large-report.pdf">
  Descarga el informe de ventas (PDF, 10MB)
</a></p>

<p><a href="http://www.example.com/video-stream/">
  Ver el video (abre en otra ventana en calidad HD )
</a></p>
```

Si queremos hacer referencia a una descarga en lugar de a algo abierto por el navegador, disponemos del atributo `download` para proporcionar un nombre al archivo guardado.

```html
<a
  href="https://download.mozilla.org/?product=firefox-39.0-SSL&os=win&lang=en-US"
  download="firefox-39-installer.exe">
    Download Firefox 39 for Windows
</a>
```