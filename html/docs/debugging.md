# Depurando nuestra web

Escribir código es una tarea que al ser realizada por seres humanos (por ahora), no es inmune a la introducción de errores.

Estos errores son comunicados por los compiladores o ejecutadores del código, señalando los puntos que este ha considerado erroneos.

La tarea de corregir dichos fallos, se conoce como debug. Esto no debe asustarnos, familiarizarse con el lenguaje y las herramientas de depuración nos ayudaran a sentirnos cómodos con esta tarea.

## Depurando HTML

HTML no es tan complicado como Javascript, al ser un lenguaje interpretado no pasa por un proceso de compilación y los errores ocurren saltan segun el navegador lee el código. Además su ejecución es muy permisiva comparado con el anterior.

### Código permisivo

¿Qué queremos decir con permisivo? Bien, normalmente cuando hacemos algo mal al codificar, hay principalmente dos tipos de error:

- **Errores sintácticos:** Son errores de escritura en el código que hacen que el programa no funcione, como el error en Rust de arriba. Son normalmente fáciles de arreglar si estamos familiarizados con las herramientas adecuadas y sabemos el significado de los mensajes de error.

- **Errores lógicos:** En estos errores la sintaxis es correcta, pero el código no hace lo que debería, por lo que el programa funciona de forma incorrecta. Estos errores son, por lo general, más difíciles de solucionar que los sintácticos, pues no hay mensajes de error que nos avisen de los mismos.

HTML en sí mismo no suele producir errores sintácticos pues los navegadores son permisivos con estos, o sea, el código sigue ejecutándose aun ¡habiendo errores presentes! Los navegadores disponen de reglas internas para saber cómo interpretar incorrectamente los errores de escritura encontrados, por lo que seguirán funcionando aunque no produzcan el resultado esperado. Esto por supuesto puede también ser un problema.

Observemos el siguiente código:

```html
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">

    <title>HTML debugging examples</title>
  </head>

  <body>
    <h1>HTML debugging examples</h1>

    <p>What causes errors in HTML?

    <ul>
      <li>Unclosed elements: If an element is <strong>not closed properly, then its effect can spread to areas you didn't intend

      <li>Badly nested elements: Nesting elements properly is also very important for code behaving correctly. <strong>strong <em>strong emphasised?</strong> what is this?</em>

      <li>Unclosed attributes: Another common source of HTML problems. Let's look at an example: <a href="https://www.mozilla.org/>link to Mozilla homepage</a>
    </ul>
  </body>
</html>
```

Si lo renderizamos en un navegador veremos que hay cosas que no se ven muy bien:

![Ejemplo de inspector web](assets/html-inspector.png)

Si usamos el inspector DOM veremos en detalle que ha realizado el navegador:

- Se han añadido etiquetas de cierre a los párrafos y las lineas de las listas.

- Al no estar claro el final del elemento `<strong>`, el navegador lo ha aplicado individualmente a todos los bloques siguientes de texto, a cada uno le ha añadido su propia etiqueta strong, desde donde está hasta el final del documento.

- El navegador ha arreglado el anidamiento incorrecto de la siguiente forma:

```html
<strong
  >strong
  <em>strong emphasised?</em>
</strong>
<em> what is this?</em>
```

El enlace a cuyo atributo le faltan las comillas del final ha sido ignorado. La última lista la ha dejado como sigue:

```html
<li>
  <strong>
    Unclosed attributes: Another common source of HTML problems. Let's look at
    an example:
  </strong>
</li>
```

En el siguiente enlace encontraremos un validador del código HTML, provisto por la W3C, organización que se encarga de definir las especificaciones de HTML, CSS, y otras tecnologías web.

[Servicio de validación de HTML](https://validator.w3.org/)

Si pasamos nuestro código de ejemplo por el validador obtendremos:

![Validación del codigo de ejemplo en W3C](assets/checker.png)

No debemos preocuparnos si no podemos corregir todos los mensajes de error — es práctico tratar de arreglar unos pocos errores cada vez y volver a pasar el validador para ver los que quedan. A veces, al arreglar unos cuantos se arreglan automáticamente otros muchos mensajes — con frecuencia muchos errores son causados por uno solo en un efecto dominó.
