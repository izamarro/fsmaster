# Estructura de una web

Hemos visto elementos individuales, que nos ayudan en la construcción de una página y otorgan semántica a la misma, pero HTML ofrece elementos estructurales que a forma de bloque nos ayudan a organizar las areas de nuestra web. Algunos siguen otorgando semántica, otros no.

## Elementos básicos de un documento HTML

Aunque cada web difiere de otra, hay elementos comunes, compuestos de los siguientes componentes:

- **header:** Normalmente formado por una gran franja que cruza la parte superior de la página con un encabezado y/o un logo. Esta parte suele permanecer invariable mientras navegamos entre página y página dentro de un sitio web.

- **navigation bar:** Son los enlaces a las principales secciones del sitio web; normalmente está formado por un menú con botones, enlaces o pestañas. Al igual que el encabezado, este contenido normalmente permanece invariable en las diferentes páginas del sitio — tener un menú inconsistente en nuestra web, inducirá a los usuarios a confusión y frustración. Muchos diseñadores web consideran el menú de navegación como parte del encabezado en lugar de un componente individual, pero esto no es necesario.

- **main content:** Es la parte ancha central de nuestra página y contiene el contenido único de la misma, por ejemplo el video que quieres ver, la narración que estás leyendo, el mapa que quieres consultar, los titulares de las noticias, etc. Esta parte es distinta de una página a otra dentro de nuestro sitio web.

- **sidebar:** Suele incluir algún tipo de información adicional, enlaces, citas, ads comerciales, etc. Normalmente está relacionado con el contenido principal de la página (por ejemplo en una página de noticias, la barra lateral podría contener la biografía del autor o enlaces a artículos relacionados) pero en otras ocasiones encontraremos elementos recurrentes como un menú de navegación secundario.

- **footer:** Parte inferior de la página que generalmente contiene la letra pequeña, el copyright o la información de contacto. Es el sitio donde colocar información común (al igual que en el encabezado) pero normalmente esta información no es tan importante o es secundaria para la propia página. El pie también se suele usar para propósitos SEO, incluyendo enlaces de acceso rápido al contenido más popular.

![estructura web tipicia](assets/typical-web.png)

Ejemplo:

```html
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8" />

    <title>My page title</title>
    <link
      href="https://fonts.googleapis.com/css?family=Open+Sans+Condensed:300|Sonsie+One"
      rel="stylesheet"
      type="text/css"
    />
    <link rel="stylesheet" href="style.css" />

    <!-- the below three lines are a fix to get HTML5 semantic elements working in old versions of Internet Explorer-->
    <!--[if lt IE 9]>
      <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
  </head>

  <body>
    <!-- Aquí empieza el encabezado principal que se mantendrá en todasa las páginas de nuestro sitio web -->

    <header>
      <h1>Header</h1>

      <!-- Aunque no es obligatorio, es una práctica común el incluir el apartado menú de navegación dentro de la cabecera o encabezado de la página -->

      <nav>
        <ul>
          <li><a href="#">Home</a></li>
          <li><a href="#">Our team</a></li>
          <li><a href="#">Projects</a></li>
          <li><a href="#">Contact</a></li>
        </ul>

        <!-- Un formulario de búsqueda es una forma no-lineal de realizar busquedas en un sitio web. -->

        <form>
          <input type="search" name="q" placeholder="Search query" />
          <input type="submit" value="Go!" />
        </form>
      </nav>
    </header>

    <!-- Aquí empieza el contenido principal de nuestra página -->
    <main>
      <!-- Incluimos un artículo -->
      <article>
        <h2>Article heading</h2>

        <p>
          Lorem ipsum dolor sit amet, consectetur adipisicing elit. Donec a diam
          lectus. Set sit amet ipsum mauris. Maecenas congue ligula as quam
          viverra nec consectetur ant hendrerit. Donec et mollis dolor. Praesent
          et diam eget libero egestas mattis sit amet vitae augue. Nam tincidunt
          congue enim, ut porta lorem lacinia consectetur.
        </p>

        <h3>subsection</h3>

        <p>
          Donec ut librero sed accu vehicula ultricies a non tortor. Lorem ipsum
          dolor sit amet, consectetur adipisicing elit. Aenean ut gravida lorem.
          Ut turpis felis, pulvinar a semper sed, adipiscing id dolor.
        </p>

        <p>
          Pelientesque auctor nisi id magna consequat sagittis. Curabitur
          dapibus, enim sit amet elit pharetra tincidunt feugiat nist imperdiet.
          Ut convallis libero in urna ultrices accumsan. Donec sed odio eros.
        </p>

        <h3>Another subsection</h3>

        <p>
          Donec viverra mi quis quam pulvinar at malesuada arcu rhoncus. Cum
          soclis natoque penatibus et manis dis parturient montes, nascetur
          ridiculus mus. In rutrum accumsan ultricies. Mauris vitae nisi at sem
          facilisis semper ac in est.
        </p>

        <p>
          Vivamus fermentum semper porta. Nunc diam velit, adipscing ut
          tristique vitae sagittis vel odio. Maecenas convallis ullamcorper
          ultricied. Curabitur ornare, ligula semper consectetur sagittis, nisi
          diam iaculis velit, is fringille sem nunc vet mi.
        </p>
      </article>

      <!-- el contenido lateral puede también estar anidado en el apartado principal (main) -->
      <aside>
        <h2>Related</h2>

        <ul>
          <li><a href="#">Oh I do like to be beside the seaside</a></li>
          <li><a href="#">Oh I do like to be beside the sea</a></li>
          <li><a href="#">Although in the North of England</a></li>
          <li><a href="#">It never stops raining</a></li>
          <li><a href="#">Oh well...</a></li>
        </ul>
      </aside>
    </main>

    <!-- Y aquí está el pie de página principal que utilizaremos en todas las páginas de nuestro sitio web -->

    <footer>
      <p>©Copyright 2050 by nobody. All rights reversed.</p>
    </footer>
  </body>
</html>
```

Renderiza como:

![Web de ejemplo](assets/sample-website.png)

### `<main>`

Contendrá el contenido único de esta página. Utilizaremos `<main>` solamente una vez para cada página, y lo situaremos inmediatamente interior al elemento `<body>`. Mejor que no lo anidemos con otros elementos.

### `<article>`

Encuadra un bloque de contenido que tiene sentido por sí mismo aparte del resto de la página (por ejemplo una entrada en un blog).

### `<section>`

Es parecido al elemento `<article>`, pero se usa más para agrupar cada parte de la página que, por su funcionalidad, constituye una sección en sí misma (por ejemplo un mini-mapa, o un conjunto de titulares y resúmenes). Se considera una buena práctica comenzar cada una de estas secciones con un título heading; nótese que podemos subdividir artículos `<article>`s en distintas secciones `<section>`, o también `<section>` en distintos artículos `<article>`, dependiendo del contexto.

### `<aside>`

Incluye contenido que no está directamente relacionado con el contenido principal, pero que puede aportar información adicional relacionada indirectamente con él (resúmenes, biografías del autor, links relacionados, etc.).

### `<header>`

Representa un grupo de contenido introductorio. Si este es "hijo" de un elemento `<body>` entonces se convertirá en el encabezado principal del sitio web, pero si es hijo de un elemento `<article>` o un elemento `<section>` entonces simplemente será el encabezado particular de cada sección.

### `<footer>`

Representa el contenido del pie de pagina

## Elementos sin semántica

A veces nos encontramos situaciones en las que no encontramos un elemento semántico adecuado para agrupar ciertos elementos juntos o englobar cierto contenido. Podríamos querer agrupar ciertos elementos para referirnos a ellos como una entidad que comparta cierto CSS o JavaScript. Para casos como estos, HTML dispone del elemento div `<div>` y del elemento `<span>`. Estos elementos deberán ser utilizados con su correspondiente atributos `class`, para conferirles su etiqueta correspondiente para ser fácilmente referenciados.

`<span>` Es un elemento no-semántico para ser utilizado en el interior de una línea, lo utilizaremos cuando no se nos ocurra el uso de otro elemento semántico de texto en el que incluir el contenido, o si no se desea añadir ningún significado específico:

```html
<p>
  El rey caminó borracho de regreso a su habitación a la 01:00, la cerveza no
  hizo nada para ayudarlo mientras se tambaleaba por la puerta
  <span class="editor-note">
    [Nota del editor: en este punto de la obra, las luces deberían estar
    apagadas bajo] </span
  >.
</p>
```

Por otro lado `<div>` Es un elemento de bloque no-semántico, lo utilizaremos cuando no se nos ocurra el uso de otro elemento semántico mejor, o si no se desea añadir ningún significado concreto:

```html
<div class="shopping-cart">
  <h2>Carrito de compras</h2>
  <ul>
    <li>
      <p>
        <a href=""> <strong> Pendientes de plata </strong> </a>: € 99.95.
      </p>
      <img src="../products/3333-0985/" alt="Pendientes de plata" />
    </li>
    <li>
      ...
    </li>
  </ul>
  <p>Total: € 237.89</p>
</div>
```

Este elemento, no es parte de un `<aside>` ni siquiera es una `<section>`, es encerrado en un div ya que puede ser reutilizado en cualquier punto de la página, en este caso al pertenecer a un producto de una tienda online.

## Planificando nuestra web

Una vez hemos planificado el contenido de una página web simple, el siguiente paso lógico es intentar trabajar el contenido para todo el sitio web, las páginas que necesitamos, y la forma de disponerlas las conexiones entre ellas para producir la mejor experiencia a los usuarios visitantes. Esto se conoce con el nombre de arquitectura web.

Una web grande y compleja necesitará mucha planificación, pero para una web sencilla compuesta por unas cuantas páginas, el proceso puede ser sencillo y divertido.

1. Tengamos en cuenta que tendremos varios elementos comunes a muchas (sino a todas) páginas — como el menú de navegación y el contenido del pie de página. Si la web está dedicada a un negocio, por ejemplo, sería una buena idea disponer de la información de contacto en el pie de página en todas las páginas. Anotemos qué información deseamos que aparezca en todas las páginas.

2. A continuación, realizaremos un sencillo esquema de cuál podría ser la estructura deseada de la apariencia de cada página.

3. A continuación, realizaremos un sencillo esquema de cuál podría ser la estructura deseada de la apariencia de cada página y los ordenaremos en grupos. Anotemos los bloques principales.

![Mockup web 1](assets/mockup1.png)

4. Ahora tratemos de esquematizar un mapa de nuestro sitio — dibujando una burbuja para cada página de nuestro sitio, y dibujar líneas que identifiquen el flujo de datos entre las distintas páginas. La página de inicio, normalmente, estará ubicada en el centro y se encontrará enlazada al resto de páginas; la mayoría de las páginas en una web sencilla estarán enlazadas desde el menú de navegación principal, aunque puede haber excepciones.

![Mockup web 2](assets/mockup2.png)
