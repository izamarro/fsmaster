# Introducción a HTML

HTML es un lenguaje muy sencillo compuesto de elementos, que pueden ser aplicados a piezas de texto para darles un significado diferente en un documento, estructurar un documento en secciones lógicas e incrusta contenido como imágenes y vídeos en una página.

## Guías

- [Primeros pasos con HTML](first-steps.md)
- [Cabecera HTML y metadatos](head-metadata.md)
- [Textos y semántica HTML](texts-semantic.md)
- [Hipervínculos HTML](hyperlinks.md)
- [Estructura de una web](architecture.md)
- [Depurando nuestra web](debugging.md)
- [Imágenes en HTML](images.md)
- [Vídeo y audio en HTML](video-and-audio.md)
- [Insertando contenido con `<iframe>`](iframes.md)
- [Trabajando con SVG](svg.md)
- [Tablas en HTML](tables.md)
- [Formularios en HTML](forms.md)
