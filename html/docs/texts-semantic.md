# Textos y semántica HTML

Uno de los trabajos principales de HTML es dar estructura de texto y significado (también conocida como semántica), de forma que un navegador pueda mostrarlo correctamente.

## Encabezados y párrafos

Cuando leemos un libro, un periódico, una historia, etc. La estructura de textos más común que encontramos son los párrafos y los encabezados.

Esta estructuración facilita la lectura y experiencia de su usuario.

### Párrafos

En HTML cada párrafo se envuelve en una etiqueta `<p>`:

```html
<p>Esto es un párrafo</p>
```

Los navegadores automáticamente añadiran un espacio antes y despues del parrafo.
Además como no sabemos el resultado de como puede ser renderizado, debido a que distintas resoluciones de pantalla (large, medium, small) crearan diferentes resultados, no podemos añadir espacios o líneas extras para formatear el texto ya que el navegador las eliminará.

```html
<p>
 Esto es un párrafo, con múltiples
 saltos de línea,
 pero ignorado
 por nuestro navegador
</p>
```

Si queremos que nuestros saltos de línea sean conservados, podemos hacer uso de varias técnicas:

#### El elemento `<br>`

El elemento `<br>` anidado en un párrafo, nos permite formatear los saltos de línea del mismo, sin empezar un nuevo párrafo.

```html
<p>
 Esto es un párrafo, con múltiples <br>
 saltos de línea, <br>
 formateado con &lt;br&gt;
</p>
```

*Recuerda: `<br>` es una etiqueta vacía, no le corresponde etiqueta de cierre*

#### El elemento `<pre>`

Este elemento define contenido preformateado, conservando espacios y saltos de línea, pero dando un estilo de fuente por defecto.

```html
<pre>
 Esto es un párrafo, con múltiples
 saltos de línea,
 preformateado con &lt;pre&gt;
</pre>
```

### Encabezados

Cada título tiene que ser envuelto en un elemento de título:

```html
<h1>Soy el título del documento</h1>
```

Hay seis elementos de título — `<h1>`, `<h2>`, `<h3>`, `<h4>`, `<h5>`, y `<h6>`.
Cada elemento representa un nivel diferente de contenido en el documento; `<h1>` representa el título principal, `<h2>` representa el subtítulo, `<h3>` representa el subtítulo del subtítulo, y así sucesivamente.

Los navegadores suelen añadir algun margen antes y despues de un encabezado.
Los motores de búsqueda utilizan los encabezados para indexar la estructura y el contenido de tu página.

Los usuarios a menudo hojean una página por sus encabezados. Es importante utilizar encabezados para mostrar la estructura del documento.

_¡¡Utilizalos unicamente para los encabezados de tus textos, no para hacer un texto más grande!!_

Adicionalmente a los títulos y para separar conceptos, podemos hacer uso del elemento `<hr>`, que renderizara una línea horizontal

```html
<h1>Este es el encabezado 1</h1>
<p>Algo de texto.</p>
<hr>
<h2>Este es el encabezado 2</h2>
<p>Algo más de texto.</p>
<hr>
```

## Estructurar jerárquicamente un documento

Los números de los elementos de encabezado marcan su importancia, por tanto podríamos hasta escribir una historia con dichos elementos:

```html
<h1>El camino del full stack</h1>
<p>Por Iván Zamarro</p>
<h2>Capítulo 1: La gran chapa</h2>
<p>Era un viernes despues de comer, teníamos sueño y el maldito desarrollador no dejó de taladrar nuestra cabeza con nuevos conceptos ...</p>
<h2>Capítulo 2: La iluminación</h2>
<p>Tras muchas horas de estudio, logramos ver la luz y entender que podríamos ser capaces de desarrollar ...</p>
<h3>El profesor dijo</h3>
<p>Han pasado 200horas de insufrible chapa, es hora de que vuelen solos...</p>
```

No existe una jerarquía establecida, pero trata de que tenga sentido. Ten en cuenta las siguientes buenas prácticas:

- Usa un único `<h1>` por página — este es el nivel de título superior, y todos los demás se situan por debajo de él jerárquicamente.

- Asegúrate que usas los títulos en el orden correcto en la jerarquía. No uses los `<h3>` para representar subtítulos, seguidos de los `<h2>` para representar los subtítulos de los subtítulos eso no tiene sentido y provocará resultados extraños.

- De los séis niveles de títulos disponibles, debes procurar no usar más de tres por página, a menos que sientas la necesidad. Los documentos con muchos niveles  se volverá difícil de manejar y dirigir. En esos casos, se recomienda, si es posible, separar el contenido en varias páginas.

Dar estructura jerarquica, es fundamental para:

- **Atraer y mantener la atención**: Los usuarios que miran una página web tienden a escanear rápidamente para encontrar contenido relevante, a menudo solo leen los encabezados para comenzar (usualmente pasamos muy poco tiempo en una página web). Si no pueden ver nada útil en unos segundos, es probable que se sientan frustrados e ir a otro lugar.

- **SEO**: Los motores de búsqueda que indexan su página consideran el contenido de los títulos como palabras clave importantes para influir en el ranking de búsqueda de la página. Sin encabezados, su página tendrá un rendimiento bajo en términos de SEO (optimización del motor de búsqueda).

- **Accesibilidad**: Las personas con discapacidad visual severa habitualmente no leen páginas web; en lugar de ello, las escuchan. Ésto lo hacen con un software llamado screen reader (lector de pantallas). Éste software proporciona acceso rápido a un contenido textual dado. Entre las variadas técnicas empleadas, proporcionan un esquema del documento al leer los encabecamientos, permitiendo a los usuarios encontrar la información que quieren rápidamente. Si no hay cabeceras disponibles, se verán obligados a ecuchar el documento entero.

## ¿Y de qué me sirve la semántica?

La semántica se encuentra en todo lo que nos rodea. Esperamos que un cartel con una mano alzada signifique "prohibido el paso". O que el triangulo de las señales de tráfico signifique "ceder el paso".

De la misma manera los elementos html propuestos conceden significado semántico adecuado a cada situación. `<h1>` significa "titular de primer orden". Y aunque el navegador le dará una apariencia CSS que adapta visualmente dicha importancia: texto grande, y marcado. Más importante es la semantica que ofrece, para motores de búsqueda o lectores de pantalla mencionados.

Cualquier elemento puede ser adaptado por CSS para parecer otra cosa.

Ejemplo:

```html
<span style="font-size: 32px; margin: 21px 0;">Parezco un título?</span>
```

Esto es un elemento `<span>` carece de semántica y se usa para envolver contenido de manera que podamos aplicar reglas CSS sobre él, sin más significado. Al aplicar los estilos propuestos, parece un titular, pero al carecer de semántica no se beneficia de las ventajas explicadas.

Es por ello que es siempre recomendable usar el elemento HTML adecuado para cada tarea.

## Listas

Las listas nos ayudan a remarcar direccionamiento en la lectura, orden o niveles de anidación. Las usamos diariamente, incluso este documento en markdown tiene algunas listas definidas. Tenemos 4 tipos en HTML:

### Desordenadas

Sirven para formatear items cuyo orden no tiene vital importancia, por ejemplo una lista de la compra:

```html
pan
leche
pasta
huevos
```

Se envuelve la lista con un elemento `ul` (unordered list):

```html
<ul>
pan
leche
pasta
huevos
</ul>
```

Seguidamente creamos los elementos de item de la lista con una etiqueta `<li>` (list item):

```html
<ul>
  <li>pan</li>
  <li>leche</li>
  <li>pasta</li>
  <li>huevos</li>
</ul>
```

#### Eligiendo el marcador de las listas desordenadas

La propiedad CSS `list-style-type`, nos permite elegir el marcador de cada elemento de la lista:

```html
<ul style="list-style-type:square;">
  <li>pan</li>
  <li>leche</li>
  <li>pasta</li>
  <li>huevos</li>
</ul>
```

Toma los siguientes valores:

- **disc**: Establece el marcador como un punto (por defecto aplicado)
- **circle**: Establece el marcador como un círculo
- **square**: Establece el marcador como un cuadrado
- **none**: Elimina el marcador, evitando su renderización

### Ordenadas

Nos permiten dar formato a elementos cuyo orden si importa, por ejemplo instrucciones de direcciones:

```html
Vaya al final de la avenida
Gire a la derecha
Gire la primera a la izquierda
Su destino se encuentra a su derecha, a 100 metros más adelante
```

La estructura de marcado es la misma que para las listas desordenadas, excepto que debe envolver los elementos de la lista en una etiqueta `<ol>` ("ordered list" - lista ordenada), en lugar de `<ul>`:

```html
<ol>
  <li>Vaya al final de la avenida</li>
  <li>Gire a la derecha</li>
  <li>Gire la primera a la izquierda</li>
  <li>Su destino se encuentra a su derecha, a 100 metros más adelante</li>
</ol>
```

#### El atributo `type` de las listas ordenadas

El atributo type de la etiqueta `<ol>` define el tipo de marcador de elemento de lista:

```html
<ol type="I">
  <li>Vaya al final de la avenida</li>
  <li>Gire a la derecha</li>
  <li>Gire la primera a la izquierda</li>
  <li>Su destino se encuentra a su derecha, a 100 metros más adelante</li>
</ol>
```

Toma los siguientes valores:

- **type="1"**: 	Los elementos de la lista se numerarán con números (por defecto aplicado)
- **type="A"**: 	Los elementos de la lista se numerarán con letras en mayúsculas
- **type="a"**: 	Los elementos de la lista se numerarán con letras en minúsculas
- **type="I"**: 	Los elementos de la lista se numerarán con números romanos en mayúsculas
- **type="i"**: 	Los elementos de la lista se numerarán con números romanos en minúsculas

#### El atributo `start` como control del orden de las listas ordenadas

Por defecto, una lista ordenada comenzará a contar desde 1. Si deseas comenzar a contar desde un número específico, puede usar el atributo de inicio:

```html
<ol start="20">
  <li>Vaya al final de la avenida</li>
  <li>Gire a la derecha</li>
  <li>Gire la primera a la izquierda</li>
  <li>Su destino se encuentra a su derecha, a 100 metros más adelante</li>
</ol>
```

### Anidadas

Como casi todos los elementos html, las listas se pueden beneficiar de la propiedad de anidamiento, para organizar conceptos. Tomemos por ejemplo la siguiente lista ordenada de una receta de cocina:

```html
<ol>
  <li> Retire la piel del ajo y pique en trozos gruesos. </li>
  <li> Retire todas las semillas y el tallo del pimiento, y pique en trozos gruesos. </li>
  <li> Agregue todos los ingredientes en un procesador de alimentos. </li>
  <li> Procese todos los ingredientes en una pasta. </li>
  <li> Si quieres un humus "grueso" grueso, procesalo por un corto tiempo. </li>
  <li> Si quieres un humilde suave, procesalo por más tiempo. </li>
</ol>
```

Dado que los dos últimos elementos estan estrechamente relacionados, pero su orden carece de importancia, podemos anidarlos en una lista desordenada dentro de nuestra lista ordenada:

```html
<ol>
  <li> Retire la piel del ajo y pique en trozos gruesos. </li>
  <li> Retire todas las semillas y el tallo del pimiento, y pique en trozos gruesos. </li>
  <li> Agregue todos los ingredientes en un procesador de alimentos. </li>
  <li> Procese todos los ingredientes en una pasta. </li>
  <ul>
    <li> Si quieres un humus "grueso" grueso, procesalo por un corto tiempo. </li>
    <li> Si quieres un humilde suave, procesalo por más tiempo. </li>
  </ul>
</ol>
```

### Descriptivas

Una lista de descripción es una lista de términos, con una descripción de cada término.

La etiqueta `<dl>` define la lista de descripción, la etiqueta `<dt>` define el término (nombre) y la etiqueta `<dd>` describe cada término:

```html
 <dl>
  <dt>Café</dt>
  <dd>- bebida negra caliente</dd>
  <dt>Leche</dt>
  <dd>- bebida fría blanca</dd>
</dl>
```

## Elementos de énfasis e importancia

Cuando nos comunicamos solemos remarcar ciertas palabras para alterar su significado, para darle énfasis o distinguirlas en algún sentido. HTML provee elementos para transmitir dicha semántica en el contenido textual.

### Énfasis

Consideremos estas dos frases:

- Me alegro de que no llegues tarde
- Me _alegro_ de que no llegues _tarde_

Es evidente que la segunda remarca algo de sarcasmo.

En HTML usamos el elemento `<em>` (emphasis) para remarcar relevancia. Así mismo se hace el documento más interesante al leerlo y es reconocido por los lectores de pantalla de tal forma que lo expresan con diferentes tonos de voz.

```html
<p>Me <em>alegro</em> de que no llegues <em>tarde</em>.</p>
```

El estilo de letra italica es el que aplica el navegador por defecto, pero no debes utilizar esta etiqueta solamente para establecer el estilo italica. Para usar ese estilo, debes utilizar únicamente la etiqueta del elemento `<span>` y algo de CSS u otra etiqueta con el elemento `<i>`

### Importancia

Al igual que al hablar sobre algo importante, remarcamos las palabras con un tono de gravedad, HTML provee sus elementos de semántica para dar trasmitir dicho contexto.

Porejemplo ante la frase:

- Este líquido es altamente **tóxico**.

Entendemos que la palabra "tóxico" es importante.

En HTML usamos el elemento `<strong>` para marcar tales expresiones. Así mismo hacemos el documento más útil, de nueva cuenta estos elementos son reconocidos por los lectores de pantallas y el tono de voz cambia a uno más fuerte.

```html
<p>Este líquido es <strong>altamente tóxico</strong>.</p>
```

El estilo negrita es el que aplican los navegadores por defecto, pero no debes usar esta etiqueta solamente para aplicar este estilo. Para hacer eso usao el elemento `<span>` y CSS, o un elemento `<b>`

### Elementos de cursiva, negrita o subrayado

Los elementos que hemos discutido hasta ahora tienen una semántica asociada clara. Las situaciones con `<b>` (negrita), `<i>` (cursiva), y `<u>` (subrayado) es algo más complicado.

Surgieron para que las personas pudieran escribir textos en negrita, cursiva o subrayado en un tiempo donde CSS era poco soportado o no lo era en absoluto. Elementos como estos, los cuales solo afectan la presentación y no a la semántica, son elementos de presentación y no deberían ser usados, porque como hemos visto antes, la semántica es muy importante para la accesibilidad, SEO, entre otros.

HTML5 redefinió los elementos `<b>`, `<i>` y `<u>` con nuevos, algo confusos, roles semánticos.

A las personas con lectores de pantalla, les es de poca utilidad dichos elementos. En la medida de lo posible deleguemos la presentación en CSS.

_Cuidado con `<u>`: La gente tiene muy asimilado el concepto de link como algo que queda subrayado en una web, no deberíamos subrayar nada que no sea un link_

## Citas

HTML también tiene funciones disponibles para marcar citas; qué elemento usa depende de si está marcando un bloque o una cita en línea.

### Citas en bloque

Si una sección de contenido a nivel de bloque (ya sea un párrafo, varios párrafos, una lista, etc.) se cita desde otro lugar, debe envolverla dentro de un elemento `<blockquote>` para indicar esto e incluir una URL que apunte a la fuente de la cita dentro de un atributo cite.

```html
<blockquote cite="https://urldelacita.com/cita">
  <p>De acuerdo con la cita...</p>
</blockquote
```

### Citas en línea

Las citas en línea funcionan exactamente de la misma manera, excepto que usan el elemento `<q>`:

```html
 <p>El objetivo de la empresa es: <q>Construir un mundo mejor en base a productos ecológicos</q></p>
```

Normalmente es usado para citas cortas, que el navegador renderizará entre parentesis angulados: `<<la cita>>`

### Citaciones

El contenido del atributo cite suena útil, pero desafortunadamente los navegadores, lectores de pantalla, etc. realmente no hacen mucho con él. No hay forma de hacer que el navegador muestre el contenido de cite, sin escribir su propia solución usando JavaScript o CSS. Si desea que la fuente de la cita esté disponible en la página, una mejor manera de marcarla es colocando el elemento <cite> al lado (o dentro) del elemento de la cita:

```html
<blockquote cite="https://urldelacita.com/cita">
  <cite>Pagina de urldelacita</cite>
  <p>De acuerdo con la cita...</p>
</blockquote
```

## Abreviaciones

Otro elemento bastante común es `<abbr>`: se utiliza para ajustar una abreviatura o acrónimo y proporcionar una expansión completa del término:

```html
<p>Usamos <abbr title="Hypertext Markup Language">HTML</abbr> para estructurar nuestros documentos web.</p>
```

La expansión del acrónimo aparecerá en un tooltip.

## Detalles de contacto

HTML tiene un elemento para formatear detalles de contacto: `<address>`:

```html
<address>
   <p> Chris Mills, Manchester, The Grim North, Reino Unido </p>
</address>
```

## Superíndice y subíndice

Sirven al formatear artículos como fechas, fórmulas químicas y ecuaciones matemáticas para que tengan el significado correcto.
Los elementos `<sup>` y `<sub>` se ocupan de esto:

```html
<p>Si x<sup>2</sup> es 9, x debe ser 3 o -3.</p>
```

## Tiempo y fechas

HTML también proporciona el elemento `<time>` para marcar tiempos y fechas en un formato legible por los navegadores:

```html
<time datetime = "2016-01-20"> 20 de enero de 2016 </time>
```

Hay muchas maneras diferentes en que los humanos escribimos las fechas. Pero estas formas diferentes no pueden ser fácilmente reconocidas por los navegadores: ¿qué pasaría si quisiera obtener automáticamente las fechas de todos los eventos en una página e insertarlos en un calendario? El elemento `<time>` nos permite adjuntar una hora / fecha inequívoca y legible por la máquina para este propósito.

Otros ejemplos:

```html
<! - Fecha simple estándar ->
<time datetime = "2016-01-20"> 20 de enero de 2016 </time>
<! - Solo año y mes ->
<time datetime = "2016-01"> enero de 2016 </time>
<! - Solo mes y día ->
<time datetime = "01-20"> 20 de enero </time>
<! - Solo tiempo, horas y minutos ->
<time datetime = "19:30"> 19:30 </time>
<! - ¡También puedes hacer segundos y milisegundos! ->
<time datetime = "19: 30: 01.856"> 19: 30: 01.856 </time>
<! - Fecha y hora ->
<time datetime = "2016-01-20T19: 30"> 7.30pm, 20 de enero de 2016 </time>
<! - Fecha y hora con desplazamiento de zona horaria ->
<time datetime = "2016-01-20T19: 30 + 01: 00"> 7.30pm, 20 de enero de 2016 son las 8.30pm en Francia </time>
<! - Llamando a un número de semana específico ->
<time datetime = "2016-W04"> La cuarta semana de 2016 </time>
```