# Vídeo y audio en HTML

Los desarrolladores llevamos queriendo usar vídeo y audio desde hace mucho tiempo. No fué hasta la decada pasada, con el crecimiento del ancho de banda disponible para consumo (un vídeo o audio pesa mucho más que una imagen) que pudimos soportar cualquier tipo de vídeo.

Inicialmente las tecnologías que hacían posible la insercción de vídeo en la web, como Flash o Silverlight, hacían posible esto. Pero arrastraron problemas de seguridad e incompatibilidad con los estándares de CSS que hicieron que cayeran desuso en favor de HTML5.

Afortunadamente, unos pocos años después la especificación HTML5 tenía tales características agregadas, con los elementos `<video>` y `<audio>`, y nuevas JavaScript APIs para controlar estos.

_Nota: aunque embeber vídeo y audio en tu web puede parecer interesante, a día de hoy es costoso hasta el punto de no merecer realmente la pena. Usar un proveedor de servicios del mismo como Youtube o Soundcloud es mucho más interesante, dadas las optimizaciones en el ancho de banda de su servicio que proveen. Estos servicios proveen codigo que puedes introducir en tu web para renderizar un elemento de vídeo muy optimizado._

## `<video>`

El elemento `<video>` nos permite introducir vídeo en la web de manera muy simple.

```html
<video src="rabbit320.webm" controls>
  <p>
    Tu navegador no soporta HTML5 video. Aquí está el
    <a href="rabbit320.webm">enlace del video</a>.
  </p>
</video>
```

Notamos varias cosas:

- `src`: al igual que `<img>`, hacemos uso del atributo para cargar la fuente de vídeo.

- `controls`: Los usuarios deben ser capaces de controlar la reproducción de video y audio (esto es especialmente crítico en personas que padecen epilepsia). Se debe utilizar el atributo controls para incluir la interfaz de control del browser, o construir la nuestra utilizando la API de JavaScript apropiada. Como mínimo la interfaz debe incluir una manera de empezar y terminar la reproducción y ajustar el volumen.

- `fallback`: Se lo llama fallback content (contenido de reserva) — y será mostrado si el browser desde el que se está accediendo a la página no soporta el elemento `<VIDEO>`, permitiéndonos proveer un fallback para browsers más antiguos. Puede ser de la manera que se quiera; en este caso proporcionamos un link directo al archivo de video, por lo que el usuario puede al menos acceder de alguna manera, independientemente del browser que esté usando

### Soporte de formatos de vídeo

No todos los formatos de vídeo son soportados por los mismos navegadores o incluso dispositivos.

Repasemos la terminología rápidamente. Formatos como MP3, MP4 y WebM son llamados formatos de contenedor. Estos contienen diferentes partes que componen toda la canción o video — como una pista de audio y una pista de video (en el caso del video), y metadatos para describir los contenidos que se presentan.

Las pistas de audio y video están también en diferentes formatos como por ejemplo:

- Un WebM usualmente contiene paquetes de Ogg Vorbis audio con VP8/VP9 video. Soportado principalmente por Firefox y Chrome.

- Un MP4 contiene a menudo paquetes AAC o audio MP3 con videos H.264. Principalmente soportados en Internet Explorer y Safari.

Un reproductor de audio tenderá a reproducir directamente un track de audio. Por ejemplo un archivo MP3 u Ogg. No necesitan contenedores.

Los formatos anteriores existen para comprimir los archivos de audio y video volviéndolos manejables (el tamaño sin comprimir es muy grande). Los browsers contienen diferentes Codecs, como Vorbis o H.264, los cuales son usados para convertir el sonido y video comprimidos en binario y viceversa. Pero desafortunadamente, como indicamos antes, no todos los browsers soportan los mismos codecs, por lo tanto, habrá que proveer varios archivos para cada producción multimedia. Si te falta el codec correcto para decodificar el medio, simplemente no se reproducirá.

_Nota: Debes estar preguntándote por qué sucede esto. El MP3 (para audio) y el MP4/H.264 (para video) son ampliamente compatibles y de buena calidad, sin embargo, también están patentados — sus patentes cubren MP3 al menos hasta 2017 y a H.264 hasta 2027, lo que significa que los browsers que no tienen la patente tienen que pagar grandes sumas de dinero para soportar estos formatos. Además, mucha gente no permite el software con restricciones, por estar a favor de formatos abiertos. Por todo esto es que tenemos que proveer múltiples formatos para los diferentes browsers._

¿Cómo soportamos múltiples formatos?

```html
<video controls>
  <source src="rabbit320.mp4" type="video/mp4" />
  <source src="rabbit320.webm" type="video/webm" />
  <p>
    Your browser doesn't support HTML5 video. Here is a
    <a href="rabbit320.mp4">link to the video</a> instead.
  </p>
</video>
```

Tomamos el atributo `src` del tag `<video>` y en su lugar incluimos elementos separados `<source>` que apuntan a sus propias fuentes. En este caso el browser irá a los elementos `<source>` y reproducirá el primero de los elementos que el codec soporte. Incluir fuentes WebM y MP4 debería bastar para reproducir el video en la mayoría de los browsers en estos días.

Cada elemento `<source>` también tiene un atributo de tipo. Esto es opcional, pero se recomienda que los incluya, ya que contienen los tipos MIME de los archivos de video, y los navegadores pueden leerlos y omitir inmediatamente los videos que no entienden. Si no están incluidos, los navegadores se cargarán e intentarán reproducir cada archivo hasta que encuentren uno que funcione, lo que tomará aún más tiempo y recursos.

El elemento `<video>` también nos permite jugar un poco más con el mismo.

```html
<video
  controls
  width="400"
  height="400"
  autoplay
  loop
  muted
  poster="poster.png"
>
  <source src="rabbit320.mp4" type="video/mp4" />
  <source src="rabbit320.webm" type="video/webm" />
  <p>
    Your browser doesn't support HTML5 video. Here is a
    <a href="rabbit320.mp4">link to the video</a> instead.
  </p>
</video>
```

Observamos nuevos atributos:

- `width`, `height`: Podemos controlar el tamaño del video con estos atributos o con CSS. En ambos casos, los videos mantienen su relación ancho-altura nativa, conocida como la relación de aspecto. Si la relación de aspecto no se mantiene con los tamaños establecidos, el video crecerá para llenar el espacio horizontalmente, y el espacio sin llenar solo tendrá un color de fondo sólido de forma predeterminada.

- `autoplay`: Hace que el audio o el video comience a reproducirse de inmediato, mientras se carga el resto de la página.

- `loop`: Hace que el video (o audio) comience a reproducirse cada vez que finaliza.

- `muted`: Hace que los medios se reproduzcan con el sonido apagado de forma predeterminada.

- `poster`: La URL de una imagen que se mostrará antes de reproducir el video.

- `preload`: Se utiliza para almacenar en búfer archivos grandes; Puede tomar uno de tres valores:

  - "none" no almacena el archivo en el búfer
  - "auto" almacena el archivo multimedia
  - "metadata" almacena solo los metadatos del archivo

## `<audio>`

Funciona exactamente igual que `<video>`, exceptuando por los atributos visuales que no son necesarios, dado que no hay nada que renderizar visualmente.

```html
<audio controls>
  <source src="viper.mp3" type="audio/mp3" />
  <source src="viper.ogg" type="audio/ogg" />
  <p>
    Your browser doesn't support HTML5 audio. Here is a
    <a href="viper.mp3">link to the audio</a> instead.
  </p>
</audio>
```

### `<track>`

Podemos proveer de subtítulos un vídeo mediante el elemento `<track>`, gracias al formato WebVTT (Web Video Text Tracks). Este es un formato de texto que contiene los subtítulos y la posición en el vídeo del mismo.

```
WEBVTT

1
00:00:22.230 --> 00:00:24.606
This is the first subtitle.

2
00:00:30.739 --> 00:00:34.074
This is the second.
```

Solo tenemos que hacer referncia desde el atributo `src` de `<track>`

```html
<video controls>
  <source src="example.mp4" type="video/mp4" />
  <source src="example.webm" type="video/webm" />
  <track kind="subtitles" src="subtitles_en.vtt" srclang="en" />
</video>
```
