# Imágenes en HTML

Al principio, la Web sólo era texto, pero no pasó mucho tiempo antes de que se añadiera la capacidad de insertar imágenes (y otros tipos de contenido más interesantes) dentro de las páginas web. Hay otros tipos de archivos multimedia que tomar en cuenta, pero es lógico comenzar con el humilde elemento `<img>`, utilizado para insertar una imagen simple en una página web.

## El elemento `<img>`

Para poner una imagen simple en una página web, utilizamos el elemento `<img>`. Un elemento vacío que requiere del atributo `src` para renderizar su contenido. Este atributo suele tomar como valor una urls.

```html
<img src="images/my-image.jpg" />
```

### El atributo `alt`

Este valor se supone que es una descripción textual de la imagen, para usarlo en situaciones en que la imagen no pueda ser vista/mostrada.

```html
<img
  src="images/my-image.jpg"
  alt="Esta es la descripción alternativa de mi imagen"
/>
```

No sólo tiene dicha utilidad sino que, puede ser útil en los siguientes contextos:

- El usuario tiene problemas de visión, y utiliza un lector de pantalla para poder leer el contenido de la web.

- Como se describió anteriormente, es posible que se haya escrito mal el nombre del archivo o su ruta.

- El navegador no soporta el tipo de imagen. Algunas personas aún usan navegadores de solo texto, como Lynx, que alternativamente muestran el texto del atributo alt.

- Podrías querer proporcionar texto para que los motores de búsqueda lo utilicen.

- Los usuarios han desactivado las imágenes para reducir el volumen de transferencia de datos y distracciones. Esto sucede especialmente en los teléfonos móviles, y en países donde el ancho de banda es limitado y caro.

¿Qué deberías escribir en el atributo alt? Esto depende en primer lugar de por qué la imagen está en ese lugar:

- **Decoración:** Si la imagen es sólo para decoración y no es parte del contenido, agregar un alt=""en blanco. Por ejemplo, un lector de pantalla no pierde el tiempo leyendo contenido que no es de necesidad básica para el usuario.

- **Contenido:** Si tu imagen provee información significante, provee la información en un breve texto alternativo . O mejor aún, en el texto principal que todos pueden ver. No escribas texto alternativo redundante. Si la imagen es descrita adecuadamente por el cuerpo principal del texto, puedes usar solo alt="".

- **Enlace:** Si pones una imagen en las etiquetas `<a>`, para convertir una imágen en un enlace, aún así debes proporcionar texto de enlace accesible. En tal caso podrías escribirlo dentro del mismo elemento `<a>`,o dentro del atributo alt de la imagen. Lo que mejor funcione en tu caso.

- **Texto:** No debes poner tu texto en imagen. Si tu encabezado principal necesita un sombreado paralelo, por ejemplo, usa CSS para eso en vez de mostrar texto en una imagen.

Esencialmente, la clave es ofrecer una experiencia utilizable, incluso cuando las imágenes no puedan ser vistas. Esto asegura que a todos los usuarios no les falte nada del contenido.

## Elemento `<figure>` y `<figcaption>`

Podríamos maquetar los subtítulos de una imagen con:

```html
<div class="figura">
  <img src="images/dinosaur.jpg"
       alt="La cabeza y el torso de un esqueleto de dinosaurio;
           tiene una cabeza grande con dientes largos y filosos"
       width="400"
       height="341">

  <p>Un T-Rex en exhibición en el Museo de la Universidad de Manchester.</p>
</div
```

Pero aquí hay un problema: no hay nada que vincule semánticamente la imagen con su título, lo que puede causar problemas a los Screen Readers. Por ejemplo, cuando tiene 50 imágenes y subtítulos, ¿qué subtítulo corresponde a cada imagen?

Los elementos HTML5 `<figure>` y `<figcaption>`. Estos se crean exactamente para este propósito: proporcionar un contenedor semántico para las figuras y vincular claramente la figura con el título:

```html
<figure>
  <img
    src="images/dinosaur.jpg"
    alt="La cabeza y el torso de un esqueleto de dinosaurio;
           tiene una cabeza grande con dientes largos y filosos"
    width="400"
    height="341"
  />

  <figcaption>
    Un T-Rex en exhibición en el Museo de la Universidad de Manchester.
  </figcaption>
</figure>
```

- `<figcaption>`: le dice al navegaro que el texto que contiene describe la imagen contenida por el elemento `<figure>`

- `<figure>`: no necesariamente tiene que contener una imagen. Es una unidad independiente de contenido que:

  - Expresa un significado en una forma compacta y fácil de entender.
  - Se puede poner en varios sitios en el flujo lineal de la página.
  - Provee de información esencial que apoya al texto principal.

  Un elemento `<figure>` podría contener varias imágenes, un trozo de código, audio, video, ecuaciones, una tabla, o cualquier otra cosa.

## Imágenes de fondo CSS

Podemos usar CSS para insertar imagenes de fondo en elementos.

```css
p {
  background-image: url("images/any-image.jpg");
}
```

La imagen resultante, podría decirse que es más fácil de posicionar y controlar que una imagen HTML. Entonces ¿para qué molestarse usando imágenes HTML? Porque las imágenes de fondo CSS son sólo para decoración. Si tan solo quieres añadir algo bonito para mejorar visualmente tu página, están bien. Sin embargo, no tienen ningún significado semántico. No pueden tener su equivalente en texto, son invisibles a los lectores de pantalla, etc.

Si una imagen tiene significado, en términos del contenido de tu página, entonces deberías usar una imagen HTML. Si la imagen es puramente decoración, deberías usar imágenes de fondo CSS.
