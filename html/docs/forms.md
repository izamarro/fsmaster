# Formularios en HTML

## ¿Qué es un formulario?

Los formularios HTML son uno de los puntos principales de interacción entre un usuario y un sitio web o aplicación. Permiten a los usuarios enviar información a un sitio web. La mayor parte de las veces se envía información a un servidor web, pero la pagina web también puede interceptarla para usarla por su lado.

Un formulario HTML está hecho de uno o más widgets. Estos widgets puede ser campos de texto (de una linea o multilínea), cajas de selección, botones, checkboxes, o botones de radio. La mayoría del tiempo, estos widgets están junto a un label que describe su propósito.

## Diseñando un formulario

Antes de picar código siempre es interesante tener las cosas claras a la hora de diseñar un formulario. Una buena práctica es dibujar el formulario y acotar así la información solicitada. La regla de oro es que a mayor información se solicite a un usuario, mayor es su perdida de interés en rellenarlo.

Vamos a diseñar un formulario de contacto parecido a este:

![Diseño del formulario](assets/form-sketch.jpg)

En términos de código HTML vamos a tener algo como lo siguiente:

```html
<form action="/my-handling-form-page" method="post">
  <div>
    <label for="name">Name:</label>
    <input type="text" id="name" />
  </div>
  <div>
    <label for="mail">E-mail:</label>
    <input type="email" id="mail" />
  </div>
  <div>
    <label for="msg">Message:</label>
    <textarea id="msg"></textarea>
  </div>
  <div class="button">
    <button type="submit">Send your message</button>
  </div>
</form>
```

_Los elementos `<div>` están allí para estructurar nuestro código de forma conveniente y poder darles estilos de una forma más sencilla._

## `<form>`

Todos los formularios HTML comienzan con el elemento `<form>` de la siguiente forma:

```html
<form action="/handler-page" method="post"></form>
```

Este elemento define formalmente un formulario. Es un contenedor como lo son `<div>` o `<p>`, pero también soporta algunos atributos específicos para configurar la forma en que el formulario se comporta. Todos sus atributos son opcionales pero se considera una buena práctica que siempre al menos el los atributos action y method se encuentren presentes.

### atributo `action`

El atributo `action` define la locación (una URL) desde donde la información recolectada por el formulario debería enviarse.

Si no se especifica, la información será enviada a la misma página desde la que fué renderizada la que contiene el formulario

### atributo `method`

El atributo `method` define con que método HTTP se enviará la información (puede ser "get" o "post").

- get: Usando este método, el navegador enviará una petición sin cuerpo en su request, y los datos del formulario serán adjuntos a la url como parte del query string.

  ```html
  <form action="http://example.com" method="get">
    <div>
      <label for="say">What greeting do you want to say?</label>
      <input name="say" id="say" value="Hi" />
    </div>
    <div>
      <label for="to">Who do you want to say it to?</label>
      <input name="to" id="to" value="Mom" />
    </div>
    <div>
      <button>Send my greetings</button>
    </div>
  </form>
  ```

  Al pulsar el botón, la url reflejará: `www.example.com/?say=Hi&to=Mom`

- post: La mayoría del tiempo usaremos este método, y el navegador creará un cuerpo en su request, no adjuntando la información en la url.

  ```
  POST / HTTP/2.0
  Host: example.com
  Content-Type: application/x-www-form-urlencoded
  Content-Length: 13

  say=Hi&to=Mom
  ```

### atributo `autocomplete`

Indica si los widgets en este formulario pueden tener sus valores autocompletados por el navegador de manera predeterminada. puede tomar los valores on y off

### atributo `name`

El nombre del formulario. Debe ser único para permitir diferenciarlo de otros formularios en el documento. El nombre no de debe ser una cadena vacía. Es habitual usar como nombre el valor del id.

### atributo `novalidate`

Cuando este atributo está presente, indica que el formulario no realizará una validación en el momento de enviarlo con submit.

## `<label>`

El elemento `<label>` es el modo formal de definir una etiqueta para un widget de formulario HTML. Este elemento es el más importante en cuanto a la accesibilidad cuando estás construyendo un formulario.

### atributo `for`

Un elemento `<label>` queda ligado a su widget mediante su atributo for. El atributo `for` referencia el `id` del correspondiente widget. Un widget se puede anidar dentro de su elemento `<label>` , pero incluso en tal caso, debe considerarse establecer explícitamente el atributo `for` como buena práctica.

```html
<form>
  <p>
    <input type="checkbox" id="id-escudo" name="escudo" value="1" />
    <label for="id-escudo"> Escudo </label>
  </p>
  <p>
    <label for="id-espada">
      <input type="checkbox" id="id-espada" name="espada" value="1" /> Espada
    </label>
  </p>
</form>
```

## elemento `<button>`

Un botón puede ser de tres tipos: submit, reset, o button.

- Un click en un botón submit envía la información del formulario a una pagina web definida por defecto en el atributo action del elemento `<form>`.

- Un click en un botón reset reinicia inmediatemente todos los widgets del formulario a sus valores por defecto. Desde un punto de viste de UX, esto se considera una mala práctica.

- Un click en un botón ¡no hace nada! Puede sonar tonto, pero es muy útil para construir botones customizados con JavaScript.

Puedes también usar el elemento `<input>` con el type correspondiente para producir un botón. La diferencia principal con el elemento `<button>` es que el elemento `<input>` únicamente permite texto plano como su label mientras que el elemento `<button>` permite contenido HTML en su label.

## elemento `<input>`

### atributos comunes

- autofocus: Este atributo booleano permite especificar que el elemento debe tener automáticamente el foco de entrada cuando se carga la página, a menos que el usuario lo anule. Solo un elemento asociado a un formulario en un documento puede tener este atributo especificado.

- `disabled`: Este atributo booleano permite especificar que el elemento no es interaccionable por el usuario.

- `name`: El nombre del elemento, este será enviado con el los datos del formulario.

- `value`: El valor inicial del elemento.

### inputs de texto

Los campos `<input>` de texto son los widgets de forma más básicos. Son una forma muy conveniente de permitir que el usuario ingrese cualquier tipo de datos. Los campos de texto también se pueden especializar con atributos HTML para lograr necesidades particulares.

Todos los campos de texto comparten algunos comportamientos comunes:

- Se pueden marcar como `readonly` (el usuario no puede modificar el valor de entrada, pero se envía con el resto de los datos del formulario).

- Pueden tener `placeholder`; Este es el texto que aparece dentro del cuadro de entrada de texto que debe usarse para describir brevemente el propósito del cuadro.

- Se pueden limitar `size` (el tamaño físico del cuadro) y `length` (el número máximo de caracteres que se pueden ingresar en el cuadro).

- Pueden beneficiarse de la corrección ortográfica, si el navegador lo admite.

#### `type=text`

```html
<input type="text" id="comment" name="comment" value="I'm a text field" />
```

#### `type=email`

```html
<input type="email" id="email" name="email" multiple />
```

Cuando se usa este tipo, el usuario debe escribir una dirección de correo electrónico válida en el campo; cualquier otro contenido hace que el navegador muestre un error cuando se envía el formulario. Tenga en cuenta que esta es una validación de error del lado del cliente, realizada por el navegador:

![Input email no validado](assets/email-invalid.png)

Ten en cuenta que a@b es una dirección de correo electrónico válida de acuerdo con las restricciones predeterminadas, ya que el tipo de entrada de correo electrónico por defecto permite valores de dirección de correo electrónico de intranet.

El tipo de entrada de correo electrónico permite varias direcciones de correo electrónico en la misma entrada (separadas por comas) al incluir el atributo `multiple`.

En algunos dispositivos, especialmente dispositivos táctiles con teclados dinámicos como teléfonos inteligentes, se puede presentar un teclado virtual diferente que sea más adecuado para ingresar direcciones de correo electrónico.

#### `type=password`

```html
<input type="password" id="pwd" name="pwd" />
```

No agrega restricciones especiales al texto ingresado, pero sí oculta el valor ingresado en el campo (por ejemplo, con puntos o asteriscos) para que otros no puedan leerlo.

Ten en cuenta que esto es solo una función de interfaz de usuario; a menos que envíe su formulario de forma segura, se enviará en texto plano, lo que es malo para la seguridad: una parte malintencionada podría interceptar sus datos y robar contraseñas, detalles de tarjetas de crédito o cualquier otra cosa que haya enviado. La mejor manera de proteger a los usuarios de esto es alojar cualquier página que involucre formularios a través de una conexión segura (es decir, en una dirección https: // ...), de modo que los datos se cifren antes de enviarse.

#### `type=tel`

```html
<input type="tel" id="tel" name="tel" />
```

Debido a la gran variedad de formatos de números de teléfono en todo el mundo, este tipo de campo no impone restricciones en el valor ingresado por un usuario (esto puede incluir letras, etc.). El atributo de `pattern`, se puede usar para imponer restricciones. Esto es principalmente una diferencia semántica, aunque en dispositivos con teclados dinámicos, se puede presentar un teclado digital que sea más adecuado para ingresar números.

#### `type=url`

```html
<input type="url" id="url" name="url" />
```

Agrega restricciones de validación especiales al campo, con el navegador informando un error si no se ingresa ningún protocolo, como http :, o si la URL está mal formada.

#### `<textarea>`

```html
<textarea cols="30" rows="10"></textarea>
```

La principal diferencia entre un área de texto y un campo de texto de una sola línea es que los usuarios pueden incluir saltos de línea que se incluirán con los datos que se envíen. Visualmente, el texto ingresado se ajusta y el control de formulario se puede cambiar de tamaño por defecto.

`<textarea>` acepta tres atributos para controlar su representación en varias líneas:

- `cols`: el ancho visible
- `rows`: el número de lineas visibles
- `wrap`: como se formatea el texto

### inputs de selección

Los widgets desplegables son una forma sencilla de permitir a los usuarios seleccionar una de las muchas opciones sin ocupar mucho espacio en la interfaz de usuario. HTML tiene dos formas de contenido desplegable: selects y autocompletados. En ambos casos, la interacción es la misma: una vez que se activa el navegador muestra una lista de valores entre los que el usuario puede seleccionar.

#### `<select>`

```html
<select id="simple" name="simple">
  <option>Banana</option>
  <option>Cherry</option>
  <option>Lemon</option>
</select>
```

Se crea un cuadro de selección con un elemento `<select>` con uno o más elementos `<option>` como elementos secundarios, cada uno de los cuales especifica uno de sus posibles valores.

Si es necesario, el valor predeterminado para el cuadro de selección se puede establecer utilizando el atributo `selected` en el elemento `<option>` deseado; esta opción se preselecciona cuando se carga la página. Los elementos `<option>` también se pueden anidar dentro de los elementos `<optgroup>` para crear grupos de valores visualmente asociados:

```html
<select id="groups" name="groups">
  <optgroup label="fruits">
    <option>Banana</option>
    <option selected>Cherry</option>
    <option>Lemon</option>
  </optgroup>
  <optgroup label="vegetables">
    <option>Carrot</option>
    <option>Eggplant</option>
    <option>Potato</option>
  </optgroup>
</select>
```

En el elemento `<optgroup>`, el valor del atributo de `label` se muestra antes que los valores de las opciones anidadas, pero incluso si se parece a una opción, no es seleccionable.

Si se establece un elemento `<option>` con un atributo de `value`, el valor de ese atributo se envía cuando se envía el formulario. Si se omite el atributo de `value`, el contenido del elemento `<option>` se usa como el valor del cuadro de selección.

Por defecto solo podemos seleccionar una opción, pero incluyendo el atributo `multiple`, podemos seleccionar varias.

```html
<select multiple id="multi" name="multi">
  <option>Banana</option>
  <option>Cherry</option>
  <option>Lemon</option>
</select>
```

Lo curioso es que entonces el `<select>` dejará de mostrar un menú de selección para mostrar toda la lista de opciones.

#### autocompletado de opciones

Puedes proporcionar valores sugeridos, completados automáticamente para widgets de formulario utilizando el elemento `<datalist`> con algunos elementos secundarios `<option`> para especificar los valores a mostrar.

La lista de datos se vincula a un campo de texto (generalmente un elemento `<input>`) utilizando el atributo de lista.

Una vez que una lista de datos está unida a un widget de formulario, sus opciones se utilizan para completar automáticamente el texto ingresado por el usuario; normalmente, esto se presenta al usuario como un cuadro desplegable que enumera las posibles coincidencias de lo que ha escrito en la entrada.

```html
<label for="myFruit">What's your favorite fruit?</label>
<input type="text" name="myFruit" id="myFruit" list="mySuggestion" />
<datalist id="mySuggestion">
  <option>Apple</option>
  <option>Banana</option>
  <option>Blackberry</option>
  <option>Blueberry</option>
  <option>Lemon</option>
  <option>Lychee</option>
  <option>Peach</option>
  <option>Pear</option>
</datalist>
```

### inputs de checking

Los elementos marcables son widgets cuyo estado puede cambiar haciendo clic en ellos o en sus etiquetas asociadas. Hay dos tipos de elementos marcables: la `type=checkbox` y el `type=radio`. Ambos usan el atributo marcado para indicar si el widget está marcado de forma predeterminada o no.

Estos widgets no se comportan exactamente como otros widgets. Para la mayoría de los widgets de formulario, una vez que se envía el formulario, se envían todos los widgets que tienen un atributo de nombre, incluso si no se ha completado ningún valor. En el caso de elementos verificables, sus valores se envían solo si están marcados. Si no están marcados, no se envía nada, ni siquiera su nombre.

Es conveniente agruparlos en un elemento `<fieldset>`.

#### `type=checkbox`

```html
<input type="checkbox" checked id="carrots" name="carrots" value="carrots" />
```

Al incluir el atributo `checked`, la casilla de verificación se marca automáticamente cuando se carga la página.

#### `type=radio`

```html
<input type="radio" checked id="soup" name="meal" />
```

Se pueden unir varios botones de radio. Si comparten el mismo valor para su atributo de `name`, se considerará que están en el mismo grupo de botones. Solo se puede verificar un botón en un grupo dado a la vez. Esto significa que cuando uno de ellos está marcado, todos los demás se desmarcan automáticamente. Cuando se envía el formulario, solo se envía el valor del botón de opción marcado. Si ninguno de ellos está marcado, se considera que el conjunto completo de botones de radio está en un estado desconocido y no se envía ningún valor con el formulario.

```html
<fieldset>
  <legend>What is your favorite meal?</legend>
  <ul>
    <li>
      <label for="soup">Soup</label>
      <input type="radio" checked id="soup" name="meal" value="soup" />
    </li>
    <li>
      <label for="curry">Curry</label>
      <input type="radio" id="curry" name="meal" value="curry" />
    </li>
    <li>
      <label for="pizza">Pizza</label>
      <input type="radio" id="pizza" name="meal" value="pizza" />
    </li>
  </ul>
</fieldset>
```

### otros inputs

#### `type=number`

```html
<input type="number" name="change" id="pennies" min="0" max="1" step="0.01" />
```

Se parece a un campo de texto, pero solo permite números y generalmente proporciona botones para aumentar y disminuir el valor del widget.

También es posible:

- Limitar el valor estableciendo los atributos `min` y `max`.

- Limitar la cantidad en la que los botones de aumento y disminución cambian el valor del widget configurando el atributo `step`.

#### `type=range`

```html
<input type="range" name="beans" id="beans" min="0" max="500" step="10" />
```

Un problema con los controles deslizantes es que no ofrecen ningún tipo de feedback visual en cuanto a cuál es el valor actual. Necesitas agregarlo con JavaScript:

En este ejemplo, agregamos un elemento <output> vacío, en el que escribiremos el valor actual del control deslizante, actualizándolo a medida que se modifica.

```html
<label for="beans">How many beans can you eat?</label>
<input type="range" name="beans" id="beans" min="0" max="500" step="10" />
<output class="beancount"></output>
```

```javascript
const beans = document.querySelector("#beans");
const count = document.querySelector(".beancount");

count.textContent = beans.value;

beans.oninput = () => {
  count.textContent = beans.value;
};
```

#### inputs de fechas y tiempo

```html
<input type="datetime-local" name="datetime" id="datetime" />
<input type="date" name="date" id="date" />
<input type="month" name="month" id="month" />
<input type="time" name="time" id="time" />
<input type="week" name="week" id="week" />
```

Todos ellos pueden limitarse con los atributos `min` y `max`.

#### `type=file`

```html
<input type="file" name="file" id="file" accept="image/*" multiple />
```

En algunos dispositivos móviles, el selector de archivos puede acceder a fotos, videos y audio capturados directamente por la cámara y el micrófono del dispositivo agregando `capture` al atributo `accept`.

```html
<input type="file" accept="image/*;capture=camera" />
<input type="file" accept="video/*;capture=camcorder" />
<input type="file" accept="audio/*;capture=microphone" />
```

#### `type=hidden`

A veces es conveniente, por razones técnicas, tener datos que se envían con un formulario pero no se muestran al usuario. Para hacer esto, podemos agregar un elemento invisible en su formulario.

````html
<input type="hidden" id="timestamp" name="timestamp" value="1286705410" />```
````
