# Trabajando con SVG

En la web, trabajarás con dos tipos de imágenes — imágenes de mapa de bits e imágenes vectoriales:

- Las imágenes de mapas de bits son definidas usando una cuadrícula de píxeles — un archivo de imagen de mapa de bits contiene información que muestra exactamente en dónde está localizado cada pixel, y de qué color debe ser exactamente. Los formatos de mapas de bits populares incluyen Bitmap (.bmp), PNG (.png), JPEG (.jpg), y GIF (.gif.)

- Las imágenes vectoriales son definidas usando algoritmos — un archivo de imagen vectorial contiene definiciones de formas y rutas para calcular cómo debe verse la imagen cuando se renderice en pantalla. El formato SVG nos permite crear gráficos vectoriales muy potentes para usar en la web.

Los archivos SVG suelen ser más ligeros que sus equivalentes en mapas de bits.

SVG es un lenguaje basado en XML para describir imágenes vectoriales. Es básicamente un marcado, como HTML, excepto que tendrás muchos elementos diferentes para definir las formas que quieres que aparezcan en tu imagen, y los efectos que quieres que se apliquen a estas formas. SVG es para marcar gráficos, no contenido. Tendrás elementos para crear formas simples, como `<circle>` y `<rect>.` Las características más avanzadas de SVG incluyen `<feColorMatrix>` (transformar colores usando una matriz de transformación), `<animate>` (animar partes de tu gráfico vectorial) y `<mask>` (aplicar una máscara sobre la parte superior de la imagen).

```html
<svg
  version="1.1"
  baseProfile="full"
  width="300"
  height="200"
  xmlns="http://www.w3.org/2000/svg"
>
  <rect width="100%" height="100%" fill="black" />
  <circle cx="150" cy="100" r="90" fill="blue" />
</svg>
```

Puedes escribir código simple SVG en un editor de texto, pero para una imagen compleja esto empieza rápidamente a volverse un poco difícil. Para crear imágenes SVG, la mayoría de personas usan un editor de gráficos vectoriales

Ventajas de usar SVG:

- El texto en imágenes vectoriales sigue siendo accesible (lo que también le beneficia a tu SEO).

- Los SVG se prestan muy bien para estilizar o cambiar dinamicamente, ya que cada componente de la imagen es un elemento que puede ser estilizado con CSS o modificado con JavaScript.

Desventajas de usar SVG:

- SVG puede volverse complicado muy rápido, lo que significa que el tamaño de los archivos pueden crecer; Los SVG complejos pueden tomar ser lentos de procesar por el navegador.

- SVG pueden ser más difíciles de crear que las imágenes de mapas de bits, dependiendo de qué tipo de imagen estás intentando crear.

- SVG no está soportado en navegadores antiguos, así que puede que nos sea adecuado si necesitas soportar versiones antiguas de Internet Explorer con tu sitio web (< IE9).

## Cómo los agregamos a nuestra web

### Mediante el elemento `<img>`

```html
<img
  src="equilateral.svg"
  alt="triangle with all three sides equal"
  height="87px"
  width="100px"
/>
```

Pros:

- Aprovechamiento de todas las características del elemento `<img>`

Contras:

- No puedes manipular la imagen con JavaScript.
- CSS se vuelve inutil en este tipo de imagenes.

### Mediante el elemento `<svg>`

```html
<svg width="300" height="200">
  <rect width="100%" height="100%" fill="green" />
</svg>
```

Pros:

- Poner SVG en línea ahorra una solicitud HTTP y, por lo tanto, puede reducir su tiempo de carga.
- Puedes asignar clases e identificadores a elementos SVG y aplicarles estilo con CSS.

Contras:

- Este método solo es adecuado si está utilizando el SVG en un solo lugar. Duplicación de codigo.
- El código SVG adicional aumenta el tamaño de su archivo HTML.
- El navegador no puede almacenar en caché SVG.
