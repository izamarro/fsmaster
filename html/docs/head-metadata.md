# Cabecera HTML y metadatos

La cabecera de un documento HTML es la parte no mostrada en el navegador al cargar la página. Contiene diversos tipos de información como el título de la misma (`<title>`), enlaces a CSS para la personalización del contenido de la misma, favicons o metadatos del documento, como quién lo escribió y palabras claves importantes del mismo.

Dado un documento HTML:

```html
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8" />
    <title>My test page</title>
  </head>
  <body>
    <p>This is my page</p>
  </body>
</html>
```

La cabecera HTML está contenida dentro del elemento `<head>`, el contenido de dicho elemento no es mostrado en la pantalla del navegador al cargar la página. Su trabajo es contener metadatos que describen el documento.

Las cabeceras de las páginas más complejas pueden abarcar varias decenas de líneas.

## `<title>`

En el ejemplo:

```html
<title>My test page</title>
```

El elemento `<title>` es usado para agregar el título del documento. No confundir con el elemento de nivel superior `<h1>` que aunque también es conocido como el título de la página, son cosas diferentes.`<title>`

- `<title>`: es un metadato que representa el título de todo el documento HTML, y puede ser usado por los marcadores para guardar tu pagina en el navegador.

- `<h1>`: contiene un dato que representa el título del contenido de la página (suele existir nada más que uno por página).

## Metadatos: elemento `<meta>`

Los me.tadatos son, en su definición más simple, datos que describen otros datos. HTML define una forma de agregarlos, el elemento `<meta>`.
Existen muchos tipos de elementos `<meta>` pero vamos a ver los más comunes.

### Codificación de caracteres

En el ejemplo:

```html
<meta charset="utf-8" />
```

Este elemento especifica la codifiacón de caracteres del documento, es decir: el conjunto de caracteres que el documento puede usar.
En este caso utf-8 es un conjunto de caracteres universal, que cubre la mayoría de idiomas humanos. De esta manera, incluyendolo tu pagina puede manejar idiomas como el japones y el ingles de manera simultanea.

![codificacion correcta](assets/correct-encoding.png)

Otras codificaciones pueden limitar el juego de caracteres usados. Y romper la renderización de los mismos.

![codificacion incorrecta](assets/bad-encoding.png)

### Author y Descripción

Muchos elementos `<meta>` incluyen atributos name y content:

- `name` especifica el tipo de elemento que es, qué tipo de información contiene.
- `content` especifica el contenido meta real.

Dos de esos metaelementos que son útiles para incluir en tu página definen al autor de la página y proporcionan una descripción concisa de la página.

Por ejemplo:

```html
<meta name="author" content="Iván Zamarro" />
<meta name="description" content="Esta es la descripción de tu pagina" />
```

Especificar una autor es útil de varias maneras: es útil saber quién escribió una página, si desean contactarnos con preguntas sobre el contenido. Algunos sistemas de gestión de contenido tienen facilidades para extraer automáticamente la información del autor de la página y ponerla a disposición para tales fines.

Especificar una descripción que incluya palabras clave relacionadas con el contenido de tu página es útil ya que tiene el potencial de hacer que la página aparezca más arriba en las búsquedas relevantes realizadas en los motores de búsqueda (SEO).

### Metadatos propietarios

A lo largo de la web encontraremos muchos tipos de metadatos que no estan definidos en el estandar, de caracter privado, que enriquecen la experiencia del usuario en plataformas privadas.

Por ejemplo:

```html
<meta property="og:image" content="https://domain/imagen.png" />
<meta property="og:description" content="Any description" />
<meta property="og:title" content="Any Title" />
```

Pertenece a un protocolo de metadatos desarrollado por Facebook para mostrar en sus plataformas los enlaces de manera más rica que una simple url.

![facebook opg](assets/facebook-meta.png)

## Iconos

Para un mayor enriquecimiento del diseño de tu sitio, puedes añadir referencias para personalizar iconos en tus metadatos, y estos se mostrarán en determinados contextos.

El humilde favicon, que ha existido durante muchos, muchos años, fue el primer icono de este tipo, un icono de 16 x 16 pixel usado en múltiples sitios. El favicon puede ser añadido a tu página:

Guardándolo en el mismo directorio que la página index de tu sitio, guardada en el formato .ico (la mayoria de los buscadores soportarán favicons en los formatos más comunes como .gif o .png, pero usar el formato ICO asegurará que funcionará desde Internet Explorer 6.)

Añadiendo la siguiente línea en tu HTML `<head>` para referenciarlo:

```html
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
```

Los modernos navegadores usan favicons en varios lugares, como en la etiqueta de la página que está abierta, y en el panel de favoritos cuando tu la añades a tus favoritas.

Hay un montón de otros tipos de iconos a considerar también en estos días. Como iconos para dispositivos como iPad que permiten guardar una pagina como una aplicación.

## Incluir CSS o Javascript en nuestra página

Como todos los sitios web, el tuyo usará CSS para que tenga un aspecto genial, así como JavaScript para potenciar las funcionalidades interactivas, como reproductores de video, mapas, juegos y demás. Estas son las aplicaciones más comunes para una página web, usando el elemento `<link>` y el elemento `<script>` respectivamente.

El elemento `<link>` siempre va dentro de la `<head>` del documento, acompañado de dos atributos: rel="stylesheet", que indica que es la hoja de estilo del documento, y href, que contiene la ruta del archivo de la hoja de estilo:

```html
<link rel="stylesheet" href="css-file.css" />
```

El elemento `<script>` no tiene por qué ir en el `<head>`. De hecho, a menudo es mejor colocarlo al final del `<body>` del documento (justo antes de la etiqueta de cierre `</body>`), para asegurarnos que todo el contenido HTML se ha leído por el navegador antes de que intente aplicarle JavaScript (si JavaScript intenta acceder a un elemento que ya no existe, el navegador lanzará un error).

```html
<script src="my-js-file.js"></script>
```

El elemento `<script>` pudiera parecer que es un elemento vacío, pero no lo es y, por tanto, necesita una etiqueta de cierre. En vez de apuntar a un archivo de secuencia de comandos o script, puedes también elegir colocar tu script dentro del elemento `<script>`.

```html
<script>
  const message = "Hola chicos!";
  console.log(message);
</script>
```

Adicionalmente, si el navegador no soporta JS podemos colocar una alternativa de texto para avisar al usuario:

```html
<noscript>Activa JavaScript en tu navegador para usar esta página!</noscript>
```

### Un poquito de JS

Javascript puede cambiar el contenido de un elemento HTML:

```html
<body>
  <p id="parrafo">Hola HTML!</p>
  <script>
    document.getElementById("parrafo").innerHTML = "Hola JavaScript!";
  </script>
</body>
```

Para seleccionar un elemento, JS, usa normalmente el metodo `document.getElementById()`

Javascript puede cambiar el estilo de una elemento HTML:

```html
<body>
  <p id="parrafo">Hola HTML!</p>
  <script>
    document.getElementById("parrafo").style.fontSize = "25px";
    document.getElementById("parrafo").style.color = "red";
    document.getElementById("parrafo").style.backgroundColor = "yellow";
  </script>
</body>
```

JS puede incluso cambiar los atributos de un elemento HTML:

```html
<body>
  <img src="https://someDomain.com/image.png" />
  <script>
    document.getElementById("image").src = "https://anotherDomain/image.gif";
  </script>
</body>
```
