# HTML

## Qué es HTML?

HTML (Hypertext Markup Language) no es un lenguaje de programación, es un lenguaje de marcas, usado para decirle a tu navegador como debe estructurar las paginas web que visitamos.

Puede ser todo lo complicado o simple que los desarrolladores deseamos que sea. HTML consiste en una serie de elementos, que pueden contener, envolver o remarcar diferentes partes del contenido de la pagina para aparecer o comportarse de cierta manera. Las etiquetas pueden hacer que una porción del contenido se convierta en un hyperlink para navegar a otra parte de la pagina web, italizaar palabras, etc.

Por ejemplo, la siguiente linea:

```html
Soy desarrollador full stack
```

Si quisieramos que la linea empezara por si misma, y especificarla como un parrafo, podemos encerrarla en el elemento `<p>`:

```html
<p>Soy desarrollador full stack</p>
```

_Las etiquetas en HTML no son afectadas por ir en mayúsculas o minúsculas. `<title>` puede ser escrito como `<Title>` o `<TITLE>` y seguir funcionando. Pero como práctica recomendada y convección, **usaremos siempre minúsculas**_

## Elementos

Un elemento es una parte de una página web. En XML y HTML, un elemento puede contener un dato, o una porción de un dato, imagenes o tal vez nada.
Típicamete un elemento inclye una etiqueta de apertura con algunos atributos, contenido, y una etiqueta de cierre.

![Elemento html](assets/element.png)

Elementos y tags **no** son la misma cosa. Una etiqueta empieza o termina un elemento en el código, mientras que un elemento es parte del DOM, el modelo del documento para mostrar la pagina en el navegador.

```
El DOM (Document Object Model en español Modelo de Objetos del Documento) es una API definida para representar e interactuar con cualquier documento HTML. El DOM es un modelo de documento que se carga en el navegador web y que representa el documento como un árbol de nodos, en donde cada nodo representa una parte del documento (puede tratarse de un elemento, una cadena de texto o un comentario)._

_El DOM es una de las APIs más usadas en la Web, pues permite ejecutar código en el navegador para acceder e interactuar con cualquier nodo del documento. Estos nodos pueden crearse, moverse o modificarse. Pueden añadirse a estos nodos manejadores de eventos (event listeners en inglés) que se ejecutarán/activarán cuando ocurra el evento indicado en este manejador._

El DOM surgió a partir de la implementación de JavaScript en los navegadores. A esta primera versión también se la conoce como DOM 0 o "Legacy DOM".
```

```
Una Interfaz de Programación de Aplicaciones (API, por sus siglas en inglés) define un conjunto de directivas que pueden ser usadas para tener una pieza de software funcionando con algunas otras.
```

Las tres partes principales de un elemento son:

- **La etiqueta de apertura:** Consiste en el nombre del elemento, en el ejemplo `p`, envuelto en parentesis angulados. Establece donde el elemento empieza. En el ejemplo, es el inicio del parrafo.

- **La etiqueta de cierre:** Idéntico a la etiqueta de apertura, exceptuando que esta incluye una barra invertida antes del nombre del elemento.

- **El contenido:** Es el conenido del elemento, en este ejemplo es unicamente texto.

### Etiquetas

En HTML una etiqueta es usada para crear un elemento. El **nombre** de un elemento HTML es el nombre usado en los parentesis angulares como `<p>` para un parrafo. El nombre de una etiqueta de cierre es precedida por el caracter de barra inclinada `/` como en `</p>`. Si no se mencionan atributos en su declaración, se usan los valores por defecto.

### Elementos anidados

Puedes poner elementos dentro de otros elementos, esta tecnica es conocida como **anidación**. Si dado un parrafo:

```html
<p>Soy desarrollador full stack</p>
```

Quisieramos remarcar, que somos **full stack**, podemos envolver esas dos palabras en una etiqueta `<strong>`:

```html
<p>Soy desarrollador <strong>full stack</strong></p>
```

Lo único de lo que debes asegurarte es de que tus elementos estan correctamente anidados, es decir que abren y cierran correctamente envolviendo el contenido de manera apropiada.

### Elementos de bloque vs elementos en línea

Hay dos categorías importantes en los ementos HTML que deberías conocer. Existen elementos en línea y elementos de bloque.

- Los elementos de bloque, forman un bloque visible en la página, apareceran en una nueva línea sea cual sea el contenido que venga antes de ellos y cualquier contenido tras ellos, también se posicionará en una nueva línea. Tienden a ser elementos estructurales de la pagina que representan cosas como: parrafos, listas, menus de navegación o pies de pagina, por ejemplo. Un elemento de bloque **no puede ser anidado** dentro de un elemento en línea, pero si dentro de otro elemento de bloque.

- Los elementos en línea son aquellos envueltos por elementos de bloque y envuelven pequeñas partes del contenido del documento. Por ejemplo, partes de un parrafo. Un elemento en línea, no causará la aparición de una nueva línea en el documento.

Por ejemplo, en el siguiente código:

```html
<em>soy el primero</em><em>soy el segundo</em><em>soy el tercero</em>

<p>soy el cuarto</p>
<p>soy el quinto</p>
<p>soy el sexto</p>
```

Si lo renderizamos, veremos que la etiqueta `<em>` conforma un elemento de línea y los tres primeros elementos, apareceran en la misma línea sin espacios entre si. Pero `<p>` es un elemento de bloque, por lo que cada elemento aparecerá en una nueva línea, con espacio por encima y debajo de ambos (debido a que es su estilo CSS por defecto).

_¿Por qué no pruebas a renderizarlos en un HTML?_

### Elementos vacíos

No todos los elementos siguen el patron, de apertura-contenido-cierre. Algunos elementos simplemente consisten en una sola tag, y suelen ser usados pra insertar contenido el documento en el lugar donde han sido incluidos. Por ejemplo la etiqueta `<img>` permite insertar archivos de imagenes como elementos en la posición donde han sido incluidas.

```html
<img src="https://domain/some-image.png" />
```

## Atributos

Los elementos pueden contener atributos en sus etiquetas, y lucen como el siguiente:

![atributo html de clase](assets/attribute.png)

Los atributos contienen información adicional que no quieres que aparezca en el contenido renderixado. En el ejemplo, el atributo `class` permite dar al elemento un identificador por nombre que puede ser usado luego para conceder estilo al elemento u otras cosas.

Un atributo debe tener:

1. Un espacio entre este y la etiqueta del elemento.
2. El nombre del atributo, seguido de un signo `=`
3. EL valor del atributo, con comillas alrededor suyo.

### Atributos booleanos

Habrá ocasiones en las que encuentres atributos sin valores. Esto esta permitido. Se denominan atributos booleanos y pueden tener únicamente un valor, que como norma general es el mismo que el nombre del atributo. Por ejemplo el atributo `disabled` que se puede asignar a elementos de entrada de formularios que se desean desactivar.

```html
<input type="text" disabled="disabled" />
<!-- es equivalente a -->
<input type="text" disabled />
```

### Omitir comillas alrededor de valores de atributos

Algunas paginas web ombiten las comillas en sus valores de atributo. Pero en general no es una buena idea. Ya que aunque si solo existe un unico atributo esto está permitido. En el momento en que aparezca un nuevo atributo el navegador puede mal interpretar los valores.

**Siempre han de escribirse con comillas**

El uso de comillas simples o dobles, es una cuestión de estilo, pero en general seguiremos el mismo estilo una vez elegido.

```html
<a href="http://www.example.com">✔️</a>

<a href="http://www.example.com">✔️</a>

<a href="http://www.example.com" title='Isn't this fun?'>❌</a>
```

### El atributo href

Los links en HTML son especificados con la etiqueta `<a>`. La dirección de dicho link se especifica con el atributo `href`

```html
<a href="https://www.neoland.es">This is a link to neoland</a>
```

### El atributo src

Las imagenes en HTML son especificadass con la etiqueta `<img>`. El nombre del archivo, es especificado con el atributo `src`.

```html
<img src="any_img.png" />
```

### El atributo width y height

Las imagenes en HTML también pueden especificar dos atributos para el alto y el ancho de la misma: `width` y `height` cuyos valores se calculan en píxeles.

```html
<img src="any_img.png" width="500" height="600" />
```

### El atributo alt

El atributo `alt` ofrece una alternativa en texto si una imagen no puede ser cargada.

También sirve de ayuda como atributo de accesibilidad para los lectores de pantalla de las personas invidentes.

```html
<img src="any_img.png" alt="Any image description" />
```

### El atributo style

El atributo `style` sirve para especificar **en línea** el estilo de un elemento, como su color, fuente, etc.

```html
<tagname style="property:value;">
```

_La propiedad y valor, son equivalentes a propiedades y valores CSS._

#### style="background-color:value";

Define el color de fondo de un elemento:

```html
<body style="background-color:red;">

<h1>This is a heading</h1>
<p>This is a paragraph.</p>

</body>
```

#### style="text-color:value";

Define el color del texto de un elemento:

```html
 <h1 style="color:blue;">This is a heading</h1>
```

#### style="font-family:value";

Define la fuente a usar en el texto de un elemento:

```html
 <h1 style="font-family:verdana;">This is a heading</h1>
```

#### style="font-size:value";

Define el tamaño de la fuente a usar en el texto de un elemento:

```html
 <h1 style="font-size:20px;">This is a heading</h1>
```

#### style="text-align:center|left|right|justify";

Define la fuente a usar en el texto de un elemento:

```html
 <h1 style="font-family:verdana;">This is a heading</h1>
```

Sus valores:

- center: centra el texto
- left: alinea el texto a la izquierda
- right: alinea el texto a la derecha
- justify: ajusta el texto de tal manera que cada línea tenga el mismo ancho

### El atributo lang

La etiqueta raiz, `<html>` tiene un atributo que permite declarar el lenguaje de un documento, esto ayuda a los lectores de pantalla de las personas invidentes a adaptar el idioma y a los buscadores a identificar el lenguaje de tu página.

```html
<html lang="en-US"></html>
```

Esto es útil de muchas maneras. Tu documento HTML será indexado de forma más efectiva por los motores de búsqueda si el idioma se establece (permitiéndole, por ejemplo, que aparezca correctamente en los resultados del idioma especificado) y es útil para que personas con discapacidad visual utilicen los lectores de pantalla (por ejemplo, la palabra "six" existe tanto en francés como en inglés, pero su pronunciación es diferente).

También puedes establecer subsecciones de tu documento para ser reconocido en diferentes idiomas. Por ejemplo, podemos establecer nuestra sección de japonés para ser reconocido como japonés, de la siguiente manera:

```html
<p>Japones: <span lang="jp">ご飯が熱い。</span>.</p>
```

Los códigos de lenguaje puedes encontrarlos en el [ISO 639-1](https://en.wikipedia.org/wiki/List_of_ISO_639-1_codes)

### El atributo title

Añadiendoselo, a un elemento `<p>`, puede por ejemplo mostrar un tooltip cuando el ratón pasa por encima.

```html
<p title="El tooltip">
  Un parrafo cualquiera
</p>
```

## Anatomía de un documento HTML

Los elementos HTML por sí mismos, no son muy útiles, pero combinados dan forma a una página HTML completa:

```html
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8" />
    <title>My test page</title>
  </head>
  <body>
    <p>This is my page</p>
  </body>
</html>
```

Observamos:

1. `<!DOCTYPE html>`: El doctype, es un legado histórico que se incluye para que todo funcione correctamente, y aunque se ignora, describe el conjunto de reglas que la pagina HTML debe seguir pra considerarse valida.

2. `<html</html>`: El elemento `<html>` engloba todo el contenido de la página, es conocido como el elemento root.

3. `<head></head>`: Conocido como cabecera, engloba todos los parámetros que se desean incluir en el documento que **no seran visibles** en la página. Tales como palabras claves, hojas de estilo, scripts, codificación de caracteres, etc.

4. `<meta charset="utf-8">`: Este elemento establece que tu documento HTML usará la codificación uft-8, que incluye la gran mayoría de caracteres de todos los lenguajes humanos conocidos.

5. `<title></title>`: Este elemento establece el título de tu página, que aparece en la pestaña/ventana de tu navegador cuando la página se carga y se utiliza para describir la página cuando la agregas a tus marcadores o la marcas como favorita.

6. `<body></body>`: El elemento `<body>`. Contiene todo el contenido que quieres mostrar a los usuarios cuando visitan tu página.

### Cuestión de legibilidad: espacios en blanco o saltos de línea.

Hay veces, como en el siguiente ejemplo:

```html
<p>
  Aqui encontramos, algunos saltos de línea que, en realidad, no van a ser
  renderizados.
</p>
```

Que puede interesarnos usar saltos de línea o espacios en blanco en el código. ¿Por qué? Por legibilidad de quien lee el codigo, en general hemos de intentar transmitir a otros desarrolladores códigos faciles de leer.

## Incluyendo caracteres especiales en HTML

En HTML, los caracteres `<`, `>`,`"`,`'` y `&` son especiales. Son parte de la sintaxis por si mismos. Entonces ¿cómo podríamos hacer para incluirlos en un texto?

Podemos usar referencias por código a los mismos, para referenciarlos. Dichas referencias empiezan con un `&` y terminan con `;`.

![Tabla de correspondencias de caracteres](assets/special_chars_html.png)

Por ejemplo, si quiero incluir el literal `<p>` en html, usaríamos:

```html
<p>Así puedo renderizar literalmente un elemento &lt;p&gt; como texto.</p>
```

## Comentarios en HTML

En html puedes incluir comentarios que no van a ser renderizados por el navegador. Pueden ser útiles para transmitir ideas sobre el código, o como recordatorios del trabajo pendiente.

Tienen una sintaxis especial, el contenido debe ser encerrado en `<!--` contenido, `-->`.

Por ejemplo:

```html
<p>Estoy trabajando en un parrafo</p>
<!--TODO: trabajar en el siguiente parrafo-->
```
