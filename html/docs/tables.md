# Tablas en HTML

Una tabla HTML es definida usando la etiqueta `<table>` para construir el elemento.
Cada fila se define con `<tr>`, el encabezado de una tabla con `<th>` y los datos con `<td>`.

```html
<table>
  <tr>
    <th>Company</th>
    <th>Contact</th>
    <th>Country</th>
  </tr>
  <tr>
    <td>Alfreds Futterkiste</td>
    <td>Maria Anders</td>
    <td>Germany</td>
  </tr>
  <tr>
    <td>Centro comercial Moctezuma</td>
    <td>Francisco Chang</td>
    <td>Mexico</td>
  </tr>
</table>
```

Si queremos que una celda se expanda varias columnas, usaremos el atributo `colspan`

```html
<th colspan="2">Nombre</th>
```

Si queremos que una celda se expanda varias filas, usaremos el atributo `rowspan`

```html
<th rowspan="2">Nombre</th>
```

## Cuidado con las tablas

Las tablas HTML deben usarse para datos tabulares; para eso están diseñadas. Desafortunadamente, muchas personas solían usar tablas HTML para diseñar páginas web, p. una fila para contener el encabezado, una fila para contener las columnas de contenido, una fila para contener el pie de página, etc.

En resumen, usar tablas para el diseño en lugar de las técnicas de diseño CSS es una mala idea. Las razones principales son las siguientes:

- Las tablas de diseño reducen la accesibilidad para usuarios con discapacidad visual: los lectores de pantalla, utilizados por personas ciegas, interpretan las etiquetas que existen en una página HTML y leen el contenido al usuario. Debido a que las tablas no son la herramienta adecuada para el diseño, y el marcado es más complejo que con las técnicas de diseño CSS, la salida de los lectores de pantalla será confusa para sus usuarios.

- Las tablas producen sopa de etiquetas: como se mencionó anteriormente, los diseños de tablas generalmente involucran estructuras de marcado más complejas que las técnicas de diseño adecuadas. Esto puede hacer que el código sea más difícil de escribir, mantener y depurar.

- Las tablas no responden automáticamente: cuando utiliza contenedores de diseño adecuados (como `<header>`, `<section>`, `<article>` o `<div>`), su ancho se establece de manera predeterminada en el 100% de su elemento principal. Las tablas, por otro lado, se dimensionan de acuerdo con su contenido de forma predeterminada, por lo que se necesitan medidas adicionales para que el diseño de la tabla funcione de manera efectiva en una variedad de dispositivos.

## `<thead>` y `<tbody>`

A medida que las tablas se vuelven un poco más complejas en estructura, es útil darles una definición más estructural. Una forma clara de hacerlo es usando `<thead>`, `<tfoot>` y `<tbody>`, que permiten marcar un encabezado, pie de página y sección de cuerpo para la tabla.

Estos elementos no hacen que la tabla sea más accesible para los usuarios de lectores de pantalla, y no resultan en ninguna mejora visual por sí mismos. Sin embargo, son muy útiles para el estilo y el diseño, ya que actúan como ganchos útiles para agregar CSS a la tabla.

Para usarlos:

- El elemento `<thead>` marca la parte de la tabla que es el encabezado; esta será comúnmente la primera fila que contiene los encabezados de columna.

- El elemento `<tbody>` marca el contenido de la tabla que no están en el encabezado o pie de página de la tabla.

_Nota: <tbody> siempre se incluye en cada tabla, implícitamente si no es especificado_

```html
<table>
  <caption>
    Table of companies contact data
  </caption>
  <thead>
    <tr>
      <th scope="col">Company</th>
      <th scope="col">Contact</th>
      <th scope="col">Country</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>Alfreds Futterkiste</td>
      <td>Maria Anders</td>
      <td>Germany</td>
    </tr>
    <tr>
      <td>Centro comercial Moctezuma</td>
      <td>Francisco Chang</td>
      <td>Mexico</td>
    </tr>
  </tbody>
</table>
```

- `scope`: este atributo ayuda a los lectores de pantalla a identificar qué clase de contenido es el que maneja el `<th>`

- `<caption>`: permite establecer una descripción semántica del contenido de la tabla
