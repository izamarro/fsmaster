# Formateando una carta

## Resumen:

En este proyecto tu tarea será publicar una carta que debe estar alojada en la intranet de una universidad. La carta es la respuesta de un compañero investigador a un posible estudiante de postgrado en relación a su deseo de trabajar en la universidad.

### Semánticas de bloque/estructurales:

- Estructura el documento completo incluyendo los elementos (doctype), <html>, <head> y <body>.

- Incluye los elementos correspondientes de marcado en la carta tales como párrafos y títulos, a excepción de los siguientes. Hay un título principal (la línea que comienza por "Re:") y tres títulos secundarios.

- Las fechas de comienzo de los semestres, las materias y los bailes deben ser marcados con los correspondientes tipos de lista.

- Las dos direcciones pueden dejarse como simples párrafos. No es apropiado usar el elemento <address> sobre ellas — piensa porqué. Además, cada línea de las direcciones debe comenzar en una nueva línea, pero no deben pertenecer a diferentes párrafos.

### Semánticas intralínea:

- Los nombres de remitente y destinatario (también "Tel" e "Email") deben ser marcado con importancia (strong).

- Deberás usar los elementos apropiados en las cuatro fechas contenidas en el documento para que puedan ser leidas por los motores de lectura automática.

- La primera dirección y la primera fecha en la carta deben ser asignadas a un atributo de clase llamado "sender-column"; el código CSS lo añadirás posteriormente para que quede bien alineado, como debe ser en un formato de carta clásico.

- Deberás utilizar el elemento apropiado para los cinco acrónimos/abreviaciones contenidos en el texto principal, proporcionándoles las extensiones correspondientes.

- Marca apropiadamente los seis sub/superíndices.

- Los símbolos de los grados, los mayor que y los símbolos de multiplicar deben ser marcados usando las referencias correctas.

- Marca al menos dos palabras en el texto con fuerte importancia/énfasis.

- Hay dos lugares donde deberíamos añadir hyperlinks; añádelos con títulos. Como sitio donde apuntan simplemente usa: http://example.com.

- Marca con el elemento apropiado el lema de la universidad y la cita del autor.

### El encabezamiento del documento:

- El juego de caracteres del documento deberá ser utf-8 usando una etiqueta meta adecuada.
- El autor de la carta debe estar especificado con la etiqueta meta correspondiente.
- El CSS proporcionado deberá estar incluido dentro de la etiqueta adecuada.

El resultado debe ser parecido a esto:

![Resultado esperado](letter.png)
