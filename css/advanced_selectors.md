# Selectores avanzados

Utilizando solamente los selectores básicos de la sección anterior, es posible diseñar prácticamente cualquier página web. CSS define otros selectores más avanzados que permiten simplificar las hojas de estilos.

## Selector de hijos

Se trata de un selector similar al selector descendente, pero diferente en su funcionamiento. Se utiliza para seleccionar un elemento que es hijo directo de otro elemento y se indica mediante el parentesis angulado de cierre `>`:

Dado el siguiente HTML:

```html
<p><span>Texto1</span></p>
<p>
  <a href="#"><span>Texto2</span></a>
</p>
```

```css
p > span {
  color: blue;
}
```

El selector aplicado se interpreta como "cualquier elemento `<span>` que sea hijo directo de un elemento `<p>`", por lo que el primer elemento `<span>` cumple la condición del selector. Sin embargo, el segundo elemento `<span>` no la cumple porque es descendiente pero no es hijo directo de un elemento `<p>`.

Para ver la diferencia entre el selector de descendencia y el de hijos, veamos un ejemplo:

```html
<p><a href="#">Enlace1</a></p>
<p>
  <span><a href="#">Enlace2</a></span>
</p>
```

```css
p a {
  color: red;
}
p > a {
  color: red;
}
```

El primer selector es de tipo descendente y por tanto se aplicará a todos los elementos `<a>` que se encuentran dentro de elementos `<p>`. En este caso, los estilos de este selector se aplican a los dos enlaces.

Por otra parte, el selector de hijos obliga a que el elemento `<a>` sea hijo directo de un elemento `<p>`. Por lo tanto, los estilos del selector `p > a` no se aplican al segundo enlace.

## Selector adyacente

El selector adyacente se emplea para seleccionar elementos que en el código HTML de la página se encuentran justo a continuación de otros elementos. Su sintaxis emplea el signo `+` para separar los dos elementos:

```css
p + a {
  ...;
}
```

Considerando el siguiente HTML:

```html
<body>
  <h1>Titulo1</h1>
  <h2>Subtítulo</h2>
  <h2>Otro subtítulo</h2>
</body>
```

Observamos dos elementos `<h2>`, pero sólo uno de ellos se encuentra inmediatamente después del elemento `<h1>`. Si se quiere aplicar diferentes colores en función de esta circunstancia, el selector adyacente es el más adecuado:

```css
h2 {
  color: green;
}
h1 + h2 {
  color: red;
}
```

Estas reglas hacen que todos los `<h2>` de la página se vean de color verde, excepto los `<h2>` que se encuentran inmediatamente después de cualquier elemento `<h1>` y que se mostrarán de color rojo.

Los elementos que forman el selector adyacente deben cumplir las dos siguientes condiciones:

- elemento1 y elemento2 deben ser elementos hermanos, por lo que su elemento padre debe ser el mismo.

- elemento2 debe aparecer inmediatamente después de elemento1 en el código HTML de la página.

# Selector adyacente general

Existe otro selector de elementos adyacentes: el selector de elementos adyacentes general.

Se utiliza para seleccionar **todos** los elementos adyacentes a otro. Su sintaxis utiliza el signo `~` para separar los elementos.

Dado el siguiente HTML:

```html
<body>
  <p>Parrafo1</p>
  <div>
    <p>Parrafo2</p>
  </div>
  <p>Parrafo3</p>
  <p>Parrafo4</p>
</body>
```

Y el siguiente CSS:

```css
div ~ p {
  color: yellow;
}
```

La anterior regla, selecciona todos los elementos `<p>` adyacentes a cualquier elemento `<div>`. Con lo cual los parrafos: `Parrafo3` y `Parrafo4` se verán afectados por dicha regla.

## Selectores de atributo

Permiten seleccionar elementos HTML en función de sus atributos y/o valores de esos atributos.

Los cuatro tipos de selectores de atributos son:

- [nombre_atributo], selecciona los elementos que tienen establecido el atributo llamado nombre_atributo, independientemente de su valor.

- [nombre_atributo=valor], selecciona los elementos que tienen establecido un atributo llamado nombre_atributo con un valor igual a valor.

- [nombre_atributo~=valor], selecciona los elementos que tienen establecido un atributo llamado nombre_atributo y al menos uno de los valores del atributo es valor.

- [nombre_atributo|=valor], selecciona los elementos que tienen establecido un atributo llamado nombre_atributo y cuyo valor es una serie de palabras separadas con guiones, pero que comienza con valor. Este tipo de selector sólo es útil para los atributos de tipo lang que indican el idioma del contenido del elemento.

Ejemplo:

```css
/* Se muestran de color azul todos los enlaces que tengan
   un atributo "class", independientemente de su valor */
a[class] {
  color: blue;
}

/* Se muestran de color azul todos los enlaces que tengan
   un atributo "class" con el valor "externo" */
a[class="externo"] {
  color: blue;
}

/* Se muestran de color azul todos los enlaces que apunten
   al sitio "http://www.ejemplo.com" */
a[href="http://www.ejemplo.com"] {
  color: blue;
}

/* Se muestran de color azul todos los enlaces que tengan
   un atributo "class" en el que al menos uno de sus valores
   sea "externo" */
a[class~="externo"] {
  color: blue;
}

/* Selecciona todos los elementos de la página cuyo atributo
   "lang" sea igual a "en", es decir, todos los elementos en inglés */
*[lang="en"] {
  ...;
}

/* Selecciona todos los elementos de la página cuyo atributo
   "lang" empiece por "es", es decir, "es", "es-ES", "es-AR", etc. */
*[lang|="es"] {
  color: red;
}
```

## Selectores de pseudo clases

Los selectores de pseudo clases nos permiten aplicar reglas a estados determinados de los elementos.
Pueden ser usados para cosas como:

- Darle estilo a un elemento cuando nos posicionamos encima con el ratón.
- Darle estilo a un elemento de tipo input cuando gana el foco.

Su sintaxis es del tipo:

```css
selector:pseudo-clase {
  propiedad: valor;
}
```

Podemos notar que tras el selector, se hace uso del símbolo de doble punto `:`, para separar selector y pseudoclase.

Vamos a ver los más comunes:

### Pseudo clases de los links

Los links pueden ser mostrados de diferentes maneras, aprovechando las siguientes pseudoclases:

```css
/* unvisited link */
a:link {
  color: red;
}

/* visited link */
a:visited {
  color: green;
}

/* mouse over link */
a:hover {
  color: blue;
}

/* selected link */
a:active {
  color: teal;
}
```

_Importante: `:hover` debe situarse siempre tras `:link` y `:visited` si queremos que sus reglas se apliquen. Al igual que `:active` debe venir tras `:hover`_

Por supuesto las pseudo clases son combinables con otros selectores, por ejemplo:

```css
a.highlight:hover {
  color: #ff0000;
}
```

Al ponernos encima de un link con la clase highlight este cambia de color.

Incluso podemos realizar cosas más complicadas:

```css
p {
  display: none;
  background-color: yellow;
  padding: 20px;
}

div:hover p {
  display: block;
}
```

El código anterior permite mostrar un elemento `<p>` cuando nos posicionamos encima de un elemento `<div>`.

### :first-child

Otro ejemplo de pseudo clase es `:first-child`, que nos permite seleccionar el primer descendiente de cualquier elemento. Por ejemplo:

```css
p:first-child {
  color: blue;
}
```

### :nth-child(n)

El selector `:nth-child(n)` encuentra cada elemento que es el hijo en posicion n de un padre, independientemente de su tipo.

Ejemplo:

```css
p:nth-child(2) {
  background: red;
}
```

Encuentra cada elemento `<p>` que es el segundo hijo de su padre.

### :nth-of-type(n)

El selector `:nth-child(n)` encuentra cada elemento que es el hijo en posicion n de un padre, de un tipo concretp.

Ejemplo:

```css
p:nth-of-type(2) {
  background: red;
}
```

Encuentra cada elemento `<p>` que es el segundo `<p>` de su padre.

## Selectores de pseudo elementos

Un pseudo elemento CSS es usado para dar estilo a partes concretas de un elemento HTML.

Por ejemplo, puede servirnos para:

- Dar estilo a la primera letra de cada parrafo
- Insertar contenido antes y despues de un elemento

La sintáxis es parecida a la de los pseudo clases:

```css
selector::pseudo-elemento {
  propiedad: valor;
}
```

Usandose el doble colon, o doble doble punto (valga la redundancia) `::` como prefijo del pseudo elemento.

### ::first-letter ::first-line

El pseudo-elemento `::first-letter` se usa para agregar un estilo especial a la primera letra de un texto.

Por ejemplo:

```css
p::first-letter {
  color: #ff0000;
  font-size: xx-large;
}
```

Aquí estaremos dando estilo a la primera letra de todos los elementos `<p>` de un documento.

Aunque tiene ciertas reglas, solo se puede aplicar a elementos de bloque, y con las siguientes reglas:

- propiedades: font
- propiedades: color
- propiedades: background
- propiedades: margin
- propiedades: padding
- propiedades: border
- text-decoration
- vertical-align (only if "float" is "none")
- text-transform
- line-height
- float
- clear

Estas propiedades son combinables, con otros selectores y por supuesto se pueden aplicar de manera consecutiva, lo que produce efectos curiosos:

```css
p::first-letter {
  color: red;
  font-size: xx-large;
}

p::first-line {
  color: blue;
  font-variant: small-caps;
}
```

En este caso la primera regla aplicará a la primera letra estilo, y al resto del parrafo otro estilo.

### ::before ::after

Estos pseudo elementos nos permiten insertan antes y despues de un elemento html contenido.

```css
h1::before {
  content: url(smiley.gif);
}

h2::after {
  content: url(smiley.gif);
}
```

## Agrupación de las reglas

Fijate en el siguiente código CSS:

```css
h1 {
  color: red;
}
h1 {
  font-size: 2em;
}
h1 {
  font-family: Verdana;
}
```

Aunque es válido, es más eficiente si agrupamos las reglas a un elemento común, esto hará nuestra hoja de estilos más eficiente.

```css
h1 {
  color: red;
  font-size: 2em;
  font-family: Verdana;
}
```

## Herencia de estilos

Una de las características principales de CSS es la herencia de los estilos definidos para los elementos. Cuando se establece el valor de una propiedad CSS en un elemento, sus elementos descendientes heredan de forma automática el valor de esa propiedad.

Ejemplo:

```css
body {
  color: blue;
}
```

Si aplicamos al body, la regla de `color: blue;` todos los elementos de la página mostrarán el texto con dicho color.

Esto por supuesto se puede restablecer aplicando otras reglas específicas,pero ayuda a simplificar la creación de hojas de estilo.

Aunque la herencia automática de estilos puede parecer complicada, simplifica en gran medida la creación de hojas de estilos complejas. Como se ha visto en los ejemplos anteriores, si se quiere establecer por ejemplo la tipografía base de la página, simplemente se debe establecer en el elemento `<body>` de la página y el resto de elementos la heredarán de forma automática.

## Especificidad de las reglas y colisión de estilos

En las hojas de estilos complejas, es habitual que varias reglas CSS se apliquen a un mismo elemento HTML. El problema de estas reglas múltiples es que se pueden dar colisiones como la del siguiente ejemplo:

```css
p {
  color: red;
}
p {
  color: blue;
}
```

¿De qué color se muestra el párrafo? CSS tiene un mecanismo de resolución de colisiones muy complejo y que tiene en cuenta el tipo de hoja de estilo que se trate (de navegador, de usuario o de diseñador), la importancia de cada regla y lo específico que sea el selector.

- Cuanto más específico sea un selector, más importancia tiene su regla asociada.
- A igual especificidad, se considera la última regla indicada.

La jerarquía de la especificidad es la siguiente:

- Estilos en línea - Un estilo en linea es el usado mediante el atributo `style` de un elemento, por ejemplo:

  ```html
  <h1 style="color: #ffffff;"></h1>
  ```

- IDs - Identificadores únicos de un elemento, como por ejemplo `#navbar`

- Clases, atributos y pseudo-clases: Incluye `.clases`, `[atributos]` y pseudo-clases como `:hover`, `:focus`, etc

- Elementos y pseudo-elementos: Incluye nombres de elementos and pseudo-elementos, como `h1`, `div`, `:before` etc.

Para calcular la especificidad podemos asignar puntuación a las reglas en base a su jerarquía:

Memorize how to calculate specificity!

- 1000 para los estilos en línea.
- 100 para cada ID
- 10 para cada atributo
- 1 para cada elemento o pseudo-elemento

```text
A: h1
B: #content h1
C: <div id="content"><h1 style="color: #ffffff">Heading</h1></div>
```

- La especificidad de A es 1 (un element)
- La especificidad de B es 101 (un ID t y un elemento)
- La especificidad de C es 1000 (estilo en línea)

Ante igual especificidad, se aplica la última regla declarada.

```css
h1 {
  background-color: green;
}
h1 {
  background-color: red; /* esta es la aplicada */
}
```

Los selectores contextuales también tiene preferencia sobre reglas declaradas en un archivo CSS. Ejemplo:

```text
Archivo externo:
#content h1 {background-color: red;}

En el HTML:
<style>
#content h1 {
  background-color: green;
}
</style>
```

La regla declarada en el HTML, al estar más cerca del elemento es la que se aplicará.
