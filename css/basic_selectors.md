# Selectores básicos

Para poder implementar un diseño web de manera correcta, debemos hacer uso de los selectores CSS.

Si la regla CSS nos indicaba "qué debemos hacer", el selector CSS nos indica "a quién debemos hacérselo". Es por ello que los selectores CSS son imprescindibles para poder aplicar de manera correcta los estilos CSS a los elementos de una página.

A un mismo elemento HTML podemos aplicarles múltiples reglas y dichas reglas son reusables sobre un elemento ilimitados de elementos.

La mayoría de sitios web se pueden diseñar usando solamente 4 o 5 selectores básicos.

## Selector universal

Usado para seleccionar todos los elementos de la página

```css
* {
  margin: 0;
  padding: 0;
}
```

Se declara mediante el asterisco (\*). No es muy utilizado habitualmente, ya que raramente vamos a querer aplicar un mismo estilo a todos los elementos. Pero si es combinable con otros selectores.

## Selector de tipo o tag

Seleccionará todos los elementos del HTML cuya etiqueta coincide con el valor de nuestro selector. Por ejemplo, para seleccionar todos los elementos `<p>` de un HTML, usaremos:

```css
p {
  /* ... */
}
```

Este selector solamente requiere del nombre de la etiqueta HTML a la que vamos a aplicar el estilo, sin los parentesis angulados `<` y `>`.

## Cadena de selectores

Si se quisiera aplicar la misma regla a varios selectores, solo es necesario separar por comas los selectores.

```css
h1,
h2,
h3 {
  color: #8a8e27;
  font-weight: normal;
}
```

Esta técnica es muy habitual en hojas de estilo complejas, donde se agrupan las propiedades comunes de varios elementos y luego se aplican las específicas de los mismos. Si cojemos el ejemplo anterior, podríamos aplicar dicha especificidad como:

```css
h1,
h2,
h3 {
  color: #8a8e27;
  font-weight: normal;
}

h1 {
  font-size: 2em;
}

h2 {
  font-size: 1.5em;
}

h3 {
  font-size: 1.2em;
}
```

## Selector descendente

Selecciona los elementos que se encuentran **dentro** de otros elementos. Es decir es descendiente de otro, pero no tiene por qué ser un descendiente directo, se aplicará a todos los elementos que lo contengan.

Dado el siguiente HTML:

```html
<p>
  ...
  <span>ejemplo1</span>
  <a><span>ejemplo2</span></a>
</p>
```

y el siguiente CSS:

```css
p span {
  color: red;
}
```

El selector `p span` seleccionara tanto `ejemplo1` como `ejemplo2`. Esto es porque el selector por descendencia no requiere que un elemento sea descendiente directo de otro, **solamente debe estar dentro de del elemento**, dando igual el nivel de prrofundiad.

La sintaxis formal sigue la regla de:

```
selector1 selector 2 ... selectorN
```

Los selectores descendentes siempre están formados por dos o más selectores separados entre sí por espacios en blanco. El último selector indica el elemento sobre el que se aplican los estilos y todos los selectores anteriores indican el lugar en el que se debe encontrar ese elemento.

Por ejemplo, este selector descendente se compone de cuatro selectores individuales:

```css
p a span em {
  text-decoration: underline;
}
```

La regla anterior se aplica a los elementos de tipo `<em>` que se encuentra dentro de elementos de tipo `<span>`, que a su vez se encuentren dentro de elementos de tipo `<a>` que se encuentren dentro de elementos de tipo `<p>`.

No debe confundirse el selector descendente con la cadena de selectores:

```css
p,
a,
span,
em {
  text-decoration: underline;
}

/* Es diferente de */
p a span em {
  text-decoration: underline;
}
```

Podemos limitar el alcance de un selector descendente mediante el uso del selector universal. Por ejemplo, dado el siguiente HTML:

```html
<p><a href="#">Enlace</a></p>
<p>
  <span><a href="#">Enlace</a></span>
</p>
```

El siguiente selector muestra los dos enlaces de color rojo:

```css
p a {
  color: red;
}
```

Pero si aplicamos el selector universal, solo el segundo enlace se mostrará en rojo:

```css
p * a {
  color: red;
}
```

El selector `p * a` se interpreta como: todos los elementos de tipo `<a>` que se encuentren dentro de cualquier elemento que, a su vez, se encuentre dentro de un elemento de tipo `<p>`. Como el primer elemento `<a>` se encuentra directamente bajo un elemento `<p>`, no se cumple la condición del selector.

## Selectores de clase

Consideremos el siguiente HTML:

```html
<body>
  <p>Lorem ipsum dolor sit amet...</p>
  <p>Nunc sed lacus et est adipiscing accumsan...</p>
  <p>Class aptent taciti sociosqu ad litora...</p>
</body>
```

¿Cómo aplicamos estilos CSS sólo al primer párrafo? El selector universal `*` no se puede utilizar porque selecciona todos los elementos de la página. El selector de tipo o etiqueta `p` tampoco se puede utilizar porque seleccionaría todos los párrafos. Y el selector descendente `body p` tampoco se puede utilizar porque todos los párrafos se encuentran en el mismo sitio.

La solución más sencilla pasa por hacer uso del atributo HTML `class` para establecer un identificador con el que hacer referencia de manera directa la regla CSS que queremos aplicar

```html
<body>
  <p class="marked">Lorem ipsum dolor sit amet...</p>
  <p>Nunc sed lacus et est adipiscing accumsan...</p>
  <p>Class aptent taciti sociosqu ad litora...</p>
</body>
```

En el archivo CSS crearemos una regla llamada `marked` prefijada por un `.` para que el navegador no la confunda con otro tipo de selector.

```css
.marked {
  color: red;
}
```

El selector `.marked` será interpretado como "cualquier elemento de la página cuyo atributo class sea igual a `marked`", por lo que solamente el primer párrafo cumplirá esa condición.

Este tipo de selectores se llaman selectores de clase y son los más utilizados junto con los selectores de ID que se verán a continuación. La principal característica de este selector es que en una misma página HTML varios elementos diferentes pueden utilizar el mismo valor en el atributo `class`:

```html
<body>
  <p class="marked">Lorem ipsum dolor sit amet...</p>
  <p>
    Nunc sed lacus et <a href="#" class="marked">est adipiscing</a> accumsan...
  </p>
  <p>Class aptent taciti <em class="marked">sociosqu ad</em> litora...</p>
</body>
```

Este tipo de selectores son los más comunes en el diseño de páginas web y permiten una gran reutilización de estilos.

Es posible aplicar limitaciones al selector de clase, partiendo del ejemplo anterior:

```html
<body>
  <p class="marked">Lorem ipsum dolor sit amet...</p>
  <p>
    Nunc sed lacus et <a href="#" class="marked">est adipiscing</a> accumsan...
  </p>
  <p>Class aptent taciti <em class="marked">sociosqu ad</em> litora...</p>
</body>
```

¿Cómo aplicamos estilos solamente al párrafo cuyo atributo `class` sea igual a `marked`? Combinando el selector de tipo y el selector de clase:

```css
p.marked {
  color: red;
}
```

El selector `p.marked` se interpreta como "aquellos elementos de tipo `<p>` que dispongan de un atributo `class` con valor `marked`".

No debe confundirse el selector de clase con los selectores anteriores:

```css
p.marked {
  ...;
}
/* Es diferente de */
p .marked {
  ...;
}
/* Es diferente de */
p,
.marked {
  ...;
}
```

Es posible aplicar varios selectores de clase a un mismo elemento HTML, simplemente separando en el atributo `class` el nombre del selector con espacios:

```html
<p class="marked bold error">...</p>
```

Si un elemento dispone de un atributo class con más de un valor, es posible utilizar un selector más avanzado:

```css
.error {
  color: red;
}
.error.marked {
  color: blue;
}
.bold {
  font-weight: bold;
}
```

```html
<p class="error marked bold">Párrafo de texto...</p>
```

En el ejemplo anterior, el color de la letra del texto es azul y no rojo. El motivo es que se ha utilizado un selector de clase múltiple `.error.destacado`, que se interpretará como "aquellos elementos de la página que dispongan de un atributo `class` con al menos los valores `error` y `marked`".

## Selectores de ID

Hay veces que necesitamos aplicar estilos directamente sobre un único elemento HTML. Bien podríamos crear un selector de clase específico para dicho elemento, pero existe otro selector más adecuado para estos casos.

El selector de ID permite seleccionar un elemento de la página a través del valor de su atributo `id`. Este tipo de selectores sólo seleccionan un elemento de la página porque el valor del atributo `id` no se puede repetir en dos elementos diferentes de una misma página.

La sintaxis de los selectores de ID es muy parecida a la de los selectores de clase, salvo que se utiliza el símbolo de la almohadilla `#` en vez del punto `.` como prefijo del nombre de la regla CSS:

Por ejemplo, dado el siguiente HTML:

```html
<p>Primer párrafo</p>
<p id="destacado">Segundo párrafo</p>
<p>Tercer párrafo</p>
```

```css
#marked {
  color: red;
} /* El selector solamente selecciona el segundo párrafo. */
```

La diferencia entre estos dos selectores viene dada gracias a la característica de HTML de que dos elementos con un atributo `id` no pueden compartir el mismo valor. Esto no es el caso cuando usamos el atributo `class`, pudiendo en este último usar el selector múltiples veces.

La recomendación general es utilizar el selector de ID cuando se quiere aplicar un estilo a un solo elemento específico de la página y utilizar el selector de clase cuando se quiere aplicar un estilo a varios elementos diferentes de la página HTML.

Al igual que los selectores de clase, en este caso también se puede restringir el alcance del selector mediante la combinación con otros selectores.

Por ejemplo:

```css
p#info {
  color: blue;
}
```

Restringir el alcance de un selector de ID puede parecer absurdo. En realidad, un selector de tipo `p#info` sólo tiene sentido cuando el archivo CSS se aplica sobre muchas páginas HTML diferentes.

## Combinación de selectores básicos

CSS nos permite combinar varios selectores para limitar el alcance de la regla aplicada:

El siguiente selector solamente selecciona elementos con un `class="especial"` que se encuentren dentro de cualquier elemento con un `class="aviso"`.

```css
.aviso .especial {
  ...;
}
```

Si modificamos el anterior selector:

```css
div.aviso span.especial {
  ...;
}
```

Solamente seleccionará aquellos elementos de tipo `<span>` con un atributo `class="especial"` que estén dentro de cualquier elemento de tipo `<div>` que tenga un atributo `class="aviso"`.

Otro ejemplo, un poco más complejo:

```css
ul#menuPrincipal li.destacado a#inicio {
  ...;
}
```

Ahora el selector hace referencia al enlace con un atributo `id="inicio"` que se encuentra dentro de un elemento de tipo `<li>` con un atributo `class="destacado"`, que forma parte de una lista `<ul>` con un atributo `id="menuPrincipal"`.
