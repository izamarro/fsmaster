# Ejercicio 4

A partir de la página web que se te proporciona, debes escribir las reglas CSS necesarias para lograr una página web que tenga el mismo aspecto que la siguiente imagen:

![pagina resultado](assets/result.png)

Los bordes que debes definir son:

- Encabezado nivel 1: borde inferior con una anchura de 2px, sólido y de color azul (#00F).
- Encabezado nivel 2: borde completo con una anchura de 2px, sólido y de color gris (#AAA).
- Lista con los datos del libro: borde superior e inferior con una anchura de 4px, punteado y de color rojo (#F00)
- Lista con el contenido del libro: borde completo con una anchura de 2px, con guiones y de color verde (#0F0)

Puedes modificar el código HTML proporcionado para añadir los identificadores y clases que necesites.
