# Ejercicio 5

A partir de la página web que se te proporciona, debes escribir las reglas CSS necesarias para lograr una página web que tenga el mismo aspecto que la siguiente imagen:

![resultado](assets/result.png)

Debes utilizar los selectores avanzados para lograr que:

- La primera letra de un encabezado de nivel 2 se muestre con un tamaño más grandes.
- El primer párrafo después de cada encabezado de nivel 2 tenga un sangrado 2em en la primera línea.
- Al principio de un enlace se muestre la flecha "→".
- Después de una abreviatura se muestre el significado de la abreviatura.
