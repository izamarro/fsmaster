# Ejercicio 2

A partir de la página web que se te proporciona, debes escribir las reglas CSS necesarias para lograr una página web que tenga el mismo aspecto que la siguiente imagen:

![tabla a realizar](assets/table.png)

Puedes modificar el código HTML proporcionado para añadir los identificadores y clases que necesites.

En la tabla se indican los nombres de los colores que debes emplear.

Debes linkar el css de manera correcta al HTML.
