# El modelo de caja en CSS

El modelo de cajas o "box model" es seguramente la característica más importante del lenguaje de hojas de estilos CSS, ya que condiciona el diseño de todas las páginas web. El modelo de cajas es el comportamiento de CSS que hace que todos los elementos de las páginas se representen mediante cajas rectangulares.

Las cajas de una página se crean automáticamente. Cada vez que se inserta una etiqueta HTML, se crea una nueva caja rectangular que encierra los contenidos de ese elemento.

![html box model](assets/box_model_html.png)

Las cajas de las páginas no son visibles a simple vista porque inicialmente no muestran ningún color de fondo ni ningún borde.

Los navegadores crean y colocan las cajas de forma automática, pero CSS permite modificar todas sus características. Cada una de las cajas está formada por las siguientes partes:

![css box model](assets/box_model_css.png)

Las partes que componen cada caja y su orden de visualización desde el punto de vista del usuario son las siguientes:

- Contenido (content): se trata del contenido HTML del elemento (las palabras de un párrafo, una imagen, el texto de una lista de elementos, etc.)

- Relleno (padding): espacio libre opcional existente entre el contenido y el borde.

- Borde (border): línea que encierra completamente el contenido y su relleno.

- Margen (margin): separación opcional existente entre la caja y el resto de cajas adyacentes.

El relleno y el margen son transparentes, por lo que en el espacio ocupado por el relleno se muestra el color o imagen de fondo (si están definidos) y en el espacio ocupado por el margen se muestra el color o imagen de fondo de su elemento padre (si están definidos). Si ningún elemento padre tiene definido un color o imagen de fondo, se muestra el color o imagen de fondo de la propia página (si están definidos).

Si una caja define tanto un color como una imagen de fondo, la imagen tiene más prioridad y es la que se visualiza. No obstante, si la imagen de fondo no cubre totalmente la caja del elemento o si la imagen tiene zonas transparentes, también se visualiza el color de fondo.

## Anchura y altura

### width

La propiedad CSS que controla la anchura de la caja de los elementos se denomina `width`.

![css width](assets/css_width.png)

La propiedad width no admite valores negativos y los valores en porcentaje se calculan a partir de la anchura de su elemento padre. El valor inherit indica que la anchura del elemento se hereda de su elemento padre. El valor auto, que es el que se utiliza si no se establece de forma explícita un valor a esta propiedad, indica que el navegador debe calcular automáticamente la anchura del elemento, teniendo en cuenta sus contenidos y el sitio disponible en la página.

El siguiente ejemplo establece el valor de la anchura del elemento `<div>` lateral:

```css
#lateral {
  width: 200px;
}
```

```html
<div id="lateral">
  ...
</div>
```

CSS define otras dos propiedades relacionadas con la anchura de los elementos: `min-width` y `max-width`.

### height

La propiedad CSS que controla la altura de los elementos se denomina `height`.

![css height](assets/css_height.png)

Al igual que sucede con width, la propiedad height no admite valores negativos. Si se indica un porcentaje, se toma como referencia la altura del elemento padre. Si el elemento padre no tiene una altura definida explícitamente, se asigna el valor auto a la altura.

El valor inherit indica que la altura del elemento se hereda de su elemento padre. El valor auto, que es el que se utiliza si no se establece de forma explícita un valor a esta propiedad, indica que el navegador debe calcular automáticamente la altura del elemento, teniendo en cuenta sus contenidos y el sitio disponible en la página.

El siguiente ejemplo establece el valor de la altura del elemento <div> de cabecera:

```css
#cabecera {
  height: 60px;
}
```

```html
<div id="cabecera">
  ...
</div>
```

CSS define otras dos propiedades relacionadas con la altura de los elementos: `min-height` y `max-height`.

## Margen y relleno

### margin

CSS define cuatro propiedades para controlar cada uno de los márgenes horizontales y verticales de un elemento.

![css margin](assets/css_margin.png)

Cada una de las propiedades establece la separación entre el borde lateral de la caja y el resto de cajas adyacentes:

![css margin relaciones](assets/css_margin_relations.png)

Las unidades más utilizadas para indicar los márgenes de un elemento son los píxeles (cuando se requiere una precisión total), los em (para hacer diseños que mantengan las proporciones) y los porcentajes (para hacer diseños líquidos o fluidos).

El siguiente ejemplo añade un margen izquierdo al segundo párrafo:

```css
.destacado {
  margin-left: 2em;
}
```

```html
<p>
  Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Nam et elit. Vivamus
  placerat lorem. Maecenas sapien. Integer ut massa. Cras diam ipsum, laoreet
  non, tincidunt a, viverra sed, tortor.
</p>

<p class="destacado">
  Vestibulum lectus diam, luctus vel, venenatis ultrices, cursus vel, tellus.
  Etiam placerat erat non sem. Nulla molestie odio non nisl tincidunt faucibus.
</p>

<p>
  Aliquam euismod sapien eu libero. Ut tempor orci at nulla. Nam in eros egestas
  massa vehicula nonummy. Morbi posuere, nibh ultricies consectetuer tincidunt,
  risus turpis laoreet elit, ut tincidunt risus sem et nunc.
</p>
```

Algunos diseñadores web utilizan la etiqueta `<blockquote>` para tabular los contenidos de los párrafos. Se trata de un error grave porque HTML no debe utilizarse para controlar el aspecto de los elementos. CSS es el único responsable de establecer el estilo de los elementos, por lo que en vez de utilizar la etiqueta `<blockquote>` de HTML, debería utilizarse la propiedad `margin-left` de CSS.

Los márgenes verticales (`margin-top` y `margin-bottom`) sólo se pueden aplicar a los elementos de bloque y las imágenes, mientras que los márgenes laterales (`margin-left` y `margin-right`) se pueden aplicar a cualquier elemento.

En los elementos en línea los márgenes verticales no tienen ningún efecto. Sin embargo, los márgenes laterales funcionan sobre cualquier tipo de elemento.

Además de las cuatro propiedades que controlan cada uno de los márgenes del elemento, CSS define una propiedad especial que permite establecer los cuatro márgenes de forma simultánea. Estas propiedades especiales se denominan "propiedades shorthand".

La propiedad que permite definir de forma simultanea los cuatro márgenes se denomina margin.

![css margin shortland](assets/css_margin_shortland.png)

La propiedad margin admite entre uno y cuatro valores, con el siguiente significado:

- Si solo se indica un valor, todos los márgenes tienen ese valor.

- Si se indican dos valores, el primero se asigna al margen superior e inferior y el segundo se asigna a los márgenes izquierdo y derecho.

- Si se indican tres valores, el primero se asigna al margen superior, el tercero se asigna al margen inferior y el segundo valor se asigna los márgenes izquierdo y derecho.

- Si se indican los cuatro valores, el orden de asignación es: margen superior, margen derecho, margen inferior y margen izquierdo.

Ejemplo:

```css
div img {
  margin-top: 0.5em;
  margin-bottom: 0.5em;
  margin-left: 1em;
  margin-right: 0.5em;
}

/* es equivalente a: */

div img {
  margin: 0.5em 0.5em 0.5m 1em;
}
```

El comportamiento de los márgenes verticales es más complejo de lo que se puede imaginar. Cuando se juntan dos o más márgenes verticales, se fusionan de forma automática y la altura del nuevo margen será igual a la altura del margen más alto de los que se han fusionado.

![css margin fusion](assets/css_margin_fusion.png)

De la misma forma, si un elemento está contenido dentro de otro elemento, sus márgenes verticales se fusionan y resultan en un nuevo margen de la misma altura que el mayor margen de los que se han fusionado:

![css margin fusion de contenido](assets/css_margin_fusion_content.png)

Aunque en principio puede parecer un comportamiento extraño, la razón por la que se propuso este mecanismo de fusión automática de márgenes verticales es el de dar uniformidad a las páginas web habituales. En una página con varios párrafos, si no se diera este comportamiento y se estableciera un determinado margen a todos los párrafos, el primer párrafo no mostraría un aspecto homogéneo respecto de los demás.

En el caso de un elemento que se encuentra en el interior de otro y sus márgenes se fusionan de forma automática, se puede evitar este comportamiento añadiendo un pequeño relleno `(padding: 1px)` o un borde `(border: 1px solid transparent)` al elemento contenedor.

### padding

CSS define cuatro propiedades para controlar cada uno de los espacios de relleno horizontales y verticales de un elemento.

![css padding](assets/css_padding.png)

Cada una de estas propiedades establece la separación entre el contenido y los bordes laterales de la caja del elemento:

![css padding relacion](assets/css_padding_relations.png)

Como sucede con los márgenes, CSS también define una propiedad de tipo "shorthand" llamada padding para establecer los cuatro rellenos de un elemento de forma simultánea.

![css padding shortland](assets/css_padding_shortland.png)

La propiedad padding admite entre uno y cuatro valores, con el mismo significado que el de la propiedad margin. Ejemplo:

```css
body {
  padding: 2em;
} /* Todos los rellenos valen 2em */
body {
  padding: 1em 2em;
} /* Superior e inferior = 1em, Izquierdo y derecho = 2em */
body {
  padding: 1em 2em 3em;
} /* Superior = 1em, derecho = 2em, inferior = 3em, izquierdo = 2em */
body {
  padding: 1em 2em 3em 4em;
} /* Superior = 1em, derecho = 2em, inferior = 3em, izquierdo = 4em */
```

## Bordes

CSS permite modificar el aspecto de cada uno de los cuatro bordes de la caja de un elemento. Para cada borde se puede establecer su anchura o grosor, su color y su estilo.

### Anchura

La anchura de los bordes se controla con las cuatro propiedades siguientes:

![css borders](assets/css_borders.png)

La anchura de los bordes se indica mediante una medida (en cualquier unidad de medida absoluta o relativa) o mediante las palabras clave thin (borde delgado), medium (borde normal) y thick (borde ancho).

La unidad de medida más habitual para establecer el grosor de los bordes es el píxel, ya que es la que permite un control más preciso sobre el grosor. Las palabras clave apenas se utilizan, ya que el estándar CSS no indica explícitamente el grosor al que equivale cada palabra clave, por lo que pueden producirse diferencias visuales entre navegadores. Así por ejemplo, el grosor medium equivale a 4px en algunas versiones de Internet Explorer y a 3px en el resto de navegadores.

Ejemplo:

```css
div {
  border-top-width: 10px;
  border-right-width: 1em;
  border-bottom-width: thick;
  border-left-width: thin;
}
```

Si se quiere establecer de forma simultánea la anchura de todos los bordes de una caja, es necesario utilizar una propiedad "shorthand" llamada `border-width`:

![css borders width](assets/css_border_width.png)

La propiedad `border-width` permite indicar entre uno y cuatro valores.

```css
p {
  border-width: thin;
} /* thin thin thin thin */
p {
  border-width: thin thick;
} /* thin thick thin thick */
p {
  border-width: thin thick medium;
} /* thin thick medium thick */
p {
  border-width: thin thick medium thin;
} /* thin thick medium thin */
```

Si se indica un solo valor, se aplica a los cuatro bordes. Si se indican dos valores, el primero se aplica al borde superior e inferior y el segundo valor se aplica al borde izquierdo y derecho.

Si se indican tres valores, el primero se aplica al borde superior, el segundo se aplica al borde izquierdo y derecho y el tercer valor se aplica al borde inferior. Si se indican los cuatro valores, el orden de aplicación es superior, derecho, inferior e izquierdo.

### Color

El color de los bordes se controla con las cuatro propiedades siguientes:

![css borders color](assets/css_border_color.png)

Por ejemplo, para mostrar el `<div>` anterior con diferentes colores en cada borde, usaríamos:

```css
div {
  border-top-color: #cc0000;
  border-right-color: blue;
  border-bottom-color: #00ff00;
  border-left-color: #ccc;
}
```

CSS incluye una propiedad "shorthand" llamada `border-color` para establecer de forma simultánea el color de todos los bordes de una caja.

Al igual que sucede con la propiedad `border-width`, es posible indicar de uno a cuatro valores y las reglas de aplicación son idénticas a las de la propiedad `border-width`:

![css borders color shortland](assets/css_border_color_shortland.png)

### Estilo

CSS permite establecer el estilo de cada uno de los bordes mediante las siguientes propiedades:

![css borders style](assets/css_border_style.png)

El estilo de los bordes sólo se puede indicar mediante alguna de las palabras reservadas definidas por CSS. Como el valor por defecto de esta propiedad es none, los elementos no muestran ningún borde visible a menos que se establezca explícitamente un estilo de borde.

```css
div {
  border-top-style: dashed;
  border-right-style: double;
  border-bottom-style: dotted;
  border-left-style: solid;
}
```

Los bordes más utilizados son solid y dashed, seguidos de double y dotted. Los estilos none y hidden son idénticos visualmente, pero se diferencian en la forma que los navegadores resuelven los conflictos entre los bordes de las celdas adyacentes en las tablas.

Para establecer de forma simultánea los estilos de todos los bordes de una caja, es necesario utilizar la propiedad "shorthand" llamada border-style:

![css borders style shortland](assets/css_border_style_shortland.png)

### shortlands

Como sucede con los márgenes y los rellenos, CSS define una serie de propiedades de tipo "shorthand" que permiten establecer todos los atributos de los bordes de forma simultánea. CSS incluye una propiedad "shorthand" para cada uno de los cuatro bordes y una propiedad "shorthand" global.

![css borders shortland](assets/css_borders_shortland.png)

El significado de cada uno de los valores especiales es el siguiente:

- medida: una medida CSS o alguna de las siguientes palabras clave: thin, medium, thick.

- color: un color de CSS o la palabra clave transparent

- estilo: una de las siguientes palabras clave: none, hidden, dotted, dashed, solid, double, groove, ridge, inset, outset.

Las propiedades "shorthand" permiten establecer alguno o todos los atributos de cada borde. El siguiente ejemplo establece el color y el tipo del borde inferior, pero no su anchura, la anchura del borde será la correspondiente al valor por defecto (medium):

```css
h1 {
  border-bottom: solid red;
}

div {
  border-top: 1px solid #369;
  border-bottom: 3px double #369;
}
```

Por ultimo, CSS define una propiedad de tipo "shorthand" global para establecer el valor de todos los atributos de todos los bordes de forma directa:

![css border shortland](assets/css_border_shortland.png)

Las siguientes reglas CSS son equivalentes:

```css
div {
  border-top: 1px solid red;
  border-right: 1px solid red;
  border-bottom: 1px solid red;
  border-left: 1px solid red;
}

div {
  border: 1px solid red;
}
```

Como el valor por defecto de la propiedad border-style es none, si una propiedad shorthand no establece explícitamente el estilo de un borde, el elemento no muestra ese borde:

```css
/* Sólo se establece el color, por lo que el estilo es
    "none" y el borde no se muestra */
div {
  border: red;
}

/* Se establece el grosor y el color del borde, pero no
   su estilo, por lo que es "none" y el borde no se muestra */
div {
  border-bottom: 5px blue;
}
```

Cuando los cuatro bordes no son idénticos pero sí muy parecidos, se puede utilizar la propiedad border para establecer de forma directa los atributos comunes de todos los bordes y posteriormente especificar para cada uno de los cuatro bordes sus propiedades particulares:

```css
h1 {
  border: solid #000;
  border-top-width: 6px;
  border-left-width: 8px;
}
```

## Margen, relleno, bordes y modelo de cajas

La anchura y altura de un elemento no solamente se calculan teniendo en cuenta sus propiedades width y height. El margen, el relleno y los bordes establecidos a un elemento determinan la anchura y altura final del elemento. En el siguiente ejemplo se muestran los estilos CSS de un elemento:

```css
div {
  width: 300px;
  padding-left: 50px;
  padding-right: 50px;
  margin-left: 30px;
  margin-right: 30px;
  border: 10px solid black;
}
```

La anchura total con la que se muestra el elemento no son los 300 píxel indicados en la propiedad width, sino que también se añaden todos sus márgenes, rellenos y bordes:

![css width total](assets/css_width_total.png)

De esta forma, la anchura del elemento en pantalla sería igual a la suma de la anchura original, los márgenes, los bordes y los rellenos:

```text
30px + 10px + 50px + 300px + 50px + 10px + 30px = 480 píxel
```

Así, la anchura/altura establecida con CSS siempre hace referencia a la anchura/altura del contenido. La anchura/altura total del elemento debe tener en cuenta además los valores del resto de partes que componen la caja del box model.

## fondos

El último elemento que forma el box model es el fondo de la caja del elemento. El fondo puede ser un color simple o una imagen. El fondo solamente se visualiza en el área ocupada por el contenido y su relleno, ya que el color de los bordes se controla directamente desde los bordes y las zonas de los márgenes siempre son transparentes.

Para establecer un color o imagen de fondo en la página entera, se debe establecer un fondo al elemento `<body>`. Si se establece un fondo a la página, como el valor inicial del fondo de los elementos es transparente, todos los elementos de la página se visualizan con el mismo fondo a menos que algún elemento especifique su propio fondo.

CSS define propiedades para establecer el fondo de cada elemento (`background-color`, `background-image`, `background-repeat`, `background-attachment`, `background-position`) y otra propiedad de tipo "shorthand" (`background`).

La propiedad `background-color` permite mostrar un color de fondo sólido en la caja de un elemento. Esta propiedad no permite crear degradados ni ningún otro efecto avanzado.

![css background-color](assets/css_background_color.png)

```css
body {
  background-color: #f5f5f5;
}
```

Para crear efectos gráficos avanzados, es necesario utilizar la propiedad `background-image`, que permite mostrar una imagen como fondo de la caja de cualquier elemento:

![css background-image](assets/css_background_image.png)

CSS permite establecer de forma simultánea un color y una imagen de fondo. En este caso, la imagen se muestra delante del color, por lo que solamente si la imagen contiene zonas transparentes es posible ver el color de fondo.

```css
body {
  background-image: url("imagenes/fondo.png");
}
```

Las imágenes de fondo se indican a través de su URL, que puede ser absoluta o relativa. Suele ser recomendable crear una carpeta de imágenes que se encuentre en el mismo directorio que los archivos CSS y que almacene todas las imágenes utilizadas en el diseño de las páginas.

Así, las imágenes correspondientes al diseño de la página se mantienen separadas del resto de imágenes del sitio y el código CSS es más sencillo (por utilizar URL relativas) y más fácil de mantener (por no tener que actualizar URL absolutas en caso de que se cambie la estructura del sitio web).

Por otra parte, suele ser habitual indicar un color de fondo siempre que se muestra una imagen de fondo. En caso de que la imagen no se pueda mostrar o contenga errores, el navegador mostrará el color indicado (que debería ser, en lo posible, similar a la imagen) y la página no parecerá que contiene errores.

Si la imagen que se quiere mostrar es demasiado grande para el fondo del elemento, solamente se muestra la parte de imagen comprendida en el tamaño del elemento. Si la imagen es más pequeña que el elemento, CSS la repite horizontal y verticalmente hasta llenar el fondo del elemento.

Este comportamiento es útil para establecer un fondo complejo a una página web entera.

Con una imagen muy pequeña (y que por tanto, se puede descargar en muy poco tiempo) se consigue cubrir completamente el fondo de la página, con lo que se consigue un gran ahorro de ancho de banda.

En ocasiones, no es conveniente que la imagen de fondo se repita horizontal y verticalmente. Para ello, CSS introduce la propiedad `background-repeat` que permite controlar la forma de repetición de las imágenes de fondo.

![css background-repeat](assets/css_background_repeat.png)

El valor repeat indica que la imagen se debe repetir en todas direcciones y por tanto, es el comportamiento por defecto. El valor no-repeat muestra una sola vez la imagen y no se repite en ninguna dirección. El valor `repeat-x` repite la imagen sólo horizontalmente y el valor repeat-y repite la imagen solamente de forma vertical.

Por ejemplo:

```css
#hdr {
  background: url("/images/ds.gif") repeat-x;
  width: 100%;
  text-align: center;
}
```

Además de seleccionar el tipo de repetición de las imágenes de fondo, CSS permite controlar la posición de la imagen dentro del fondo del elemento mediante la propiedad `background-position`.

![css background-position](assets/css_background_position.png)

La propiedad `background-position` permite indicar la distancia que se desplaza la imagen de fondo respecto de su posición original situada en la esquina superior izquierda.

Si se indican dos porcentajes o dos medidas, el primero indica el desplazamiento horizontal y el segundo el desplazamiento vertical respecto del origen (situado en la esquina superior izquierda). Si solamente se indica un porcentaje o una medida, se considera que es el desplazamiento horizontal y al desplazamiento vertical se le asigna automáticamente el valor de 50%.

Cuando se utilizan porcentajes, su interpretación no es intuitiva. Si el valor de la propiedad `background-position` se indica mediante dos porcentajes x% y%, el navegador coloca el punto (x%, y%) de la imagen de fondo en el punto (x%, y%) del elemento.

Las palabras clave permitidas son equivalentes a algunos porcentajes significativos: top = 0%, left = 0%, center = 50%, bottom = 100%, right = 100%.

CSS permite mezclar porcentajes y palabras clave, como por ejemplo 50% 2cm, center 2cm, center 10%.

Si se utilizan solamente palabras clave, el orden es indiferente y por tanto, es equivalente indicar top left y left top.

Ejemplo:

Las reglas CSS del ejemplo anterior se muestran a continuación:

![css background-position example](assets/css_background_position_example.png)

```css
#caja1 {
  background-image: url("images/help.png");
  background-repeat: no-repeat;
  background-position: bottom left;
}
#caja2 {
  background-image: url("images/help.png");
  background-repeat: no-repeat;
  background-position: right top;
}
#caja3 {
  background-image: url("images/help.png");
  background-repeat: no-repeat;
  background-position: 50% 50%;
}
```

```html
<div id="caja1"><h1>bottom left</h1></div>
<div id="caja2"><h1>right top</h1></div>
<div id="caja3"><h1>50% 50%</h1></div>
```

CSS define una propiedad de tipo "shorthand" para indicar todas las propiedades de los colores e imágenes de fondo de forma directa. La propiedad se denomina background y es la que generalmente se utiliza para establecer las propiedades del fondo de los elementos.

![css background shortland](assets/css_background_shortland.png)

El orden en el que se indican las propiedades es indiferente, aunque en general se sigue el formato indicado de color, url de imagen, repetición y posición.

Por ejemplo:

```css
/* Color e imagen de fondo de la página mediante una propiedad shorthand */
body {
  background: #222d2d url(./graphics/colorstrip.gif) repeat-x 0 0;
}

/* La propiedad shorthand anterior es equivalente a las siguientes propiedades */
body {
  background-color: #222d2d;
  background-image: url("./graphics/colorstrip.gif");
  background-repeat: repeat-x;
  background-position: 0 0;
}
```

Puede ser refactorizado a:

```css
body {
  background: #293838 url("./graphics/colorstrip.gif") repeat-x 0 0;
}
```
